﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string uname = "";
            string upwd = "";
            if (args.Length>=1)
            {
                uname = args[0];
                upwd = args[1];                
                //以下重置密码
                if (uname!="" && upwd !="")
                {
                    Console.WriteLine("用户："+uname+"新密码："+upwd);
                    string sqle1 = "UPDATE  urp_user  SET  f0003=@f3,f0004=@f4 WHERE f0002=@f2";
                    Dictionary<string, dynamic> pse1 = new Dictionary<string, dynamic>();
                    string upwd1 = Models.common.EncryptByMD5(upwd.Trim());
                    string upwd2 = Models.StringSecurity.DESEncrypt(upwd.Trim());
                    pse1.Add("@f3",upwd1);
                    pse1.Add("@f4",upwd2);
                    pse1.Add("@f2", uname.Trim());
                    int fe1 = Models.DataBase_SQLite.InsertDelEdit(sqle1, pse1);
                    if (fe1 > 0)
                    {
                        Console.WriteLine("修改成功！"+fe1);
                    }
                    else
                    {
                        Console.WriteLine("修改失败！" + fe1);
                    }

                }
                else
                {
                    Console.WriteLine("参数不正确!\r\n 正确格式：dotnet fmcmd.dll 用户名 密码");
                }
            }else
            {
                Console.WriteLine("缺少参数!\r\n 正确格式：dotnet fmcmd.dll 用户名 密码");

            }
            
        }
    }
}
