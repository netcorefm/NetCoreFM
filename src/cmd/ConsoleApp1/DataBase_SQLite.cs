﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
namespace ConsoleApp1.Models
{
   public class DataBase_SQLite
    {
        //事务增删除改示例
        //空间System.Data.SQLite;
        //string[] cmdsqls2 = new string[2] {
        //    "INSERT INTO dm_design_lists(f0001, f0002, f0003, f0004, f0005, f0006)VALUES (?,?,?,?,?,?);",
        //    "INSERT INTO dm_design_labelbase(f0001, f0002, f0003, f0004, f0005, f0006, f0007, f0008)VALUES(?,?,?,?,?,?,?,?)"

        //      };
        //List<SQLiteParameter[]> lps2 = new List<SQLiteParameter[]>();
        ////sql1参
        //SQLiteParameter[] ps = new SQLiteParameter[6];
        //ps[0] = new SQLiteParameter("@f1", int.Parse(f1));//与sql对应的字段类型要一值
        //ps[1] = new SQLiteParameter("@f2", f2);
        //ps[2] = new SQLiteParameter("@f3", f3);
        //ps[3] = new SQLiteParameter("@f4", f4);
        //ps[4] = new SQLiteParameter("@f5", f5);
        //ps[5] = new SQLiteParameter("@f6", f6);
        //lps2.Add(ps);
        //SQLiteParameter[] ps1 = new SQLiteParameter[8];
        //ps1[0] = new SQLiteParameter("@f1", int.Parse(f1));//与sql对应的字段类型要一值
        //ps1[1] = new SQLiteParameter("@f2", f2);
        //ps1[2] = new SQLiteParameter("@f3", f3);
        //ps1[3] = new SQLiteParameter("@f4", "---");
        //ps1[4] = new SQLiteParameter("@f5", "---");
        //ps1[5] = new SQLiteParameter("@f6", "false");
        //ps1[6] = new SQLiteParameter("@f7", "false");
        //ps1[7] = new SQLiteParameter("@f8", "11100101000000000000000");
        //lps2.Add(ps1);
        //fl1 = DataBase_SQLite.TranInsertDelEdit(cmdsqls2, lps2);
        //带参单条增删查改示例
        //string sql2 = "SELECT *  FROM dm_food_subclass where f0001=? order by f0002";
        //SQLiteParameter[] ps = new SQLiteParameter[]
        //{
        //        new SQLiteParameter("@zlbm",this.tp3v1.CurrentRow.Cells[0].Value.ToString())

        //};
        //DataTable dt2 = DataBase_SQLite.GetDataTable(sql2, ps);

         /*    
            参数引用1    
            Dictionary <string,dynamic> ps1=new Dictionary<string,dynamic>();
            ps1.Add("@f1",5);
            ps1.Add("@f2","ping");       

            参数引用2
             
         */


        //---------------------------------------------------------------
        //                   SQL查询第一行第一列的值   
        //  xypzf lastdate 2018.6.22
        //---------------------------------------------------------------
        /// <summary>
        /// SQL查询第一行第一列的值,用于单一值如聚合函数count(*)的值
        /// </summary>
        /// <param name="cmdstr">SQL查询字符串</param>     
        /// <returns>返回值为字符串,不存在为空串</returns>  
        public static string GetOneValue(string cmdstr)
        {
            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                SQLiteCommand  cmd = new SQLiteCommand(cmdstr, conn);
                string rc1 = cmd.ExecuteScalar().ToString();
                return rc1;
            }
        }
        /// <summary>
        /// 预处理方式SQL查询第一行第一列的值,用于单一值如聚合函数count(*)的值
        /// </summary>
        /// <param name="cmdstr">SQL查询字符串</param>     
        /// <param name="vars">查询SQL字符串中带的n个变量及对应值</param>    
        /// <returns>返回值为字符串,不存在为空串</returns>  
        public static string GetOneValue(string cmdstr,SQLiteParameter[] vars)
        {
            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
                cmd.Parameters.AddRange(vars);
                string rc1 = cmd.ExecuteScalar().ToString();
                return rc1;
            }
        }
        /*        
            Dictionary <string,dynamic> ps1=new Dictionary<string,dynamic>();
            ps1.Add("@f1",5);
            ps1.Add("@f2","ping");        
         */
         public static string GetOneValue(string cmdstr,Dictionary<string,dynamic> cmdps)
        {  
            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);                
                List<SQLiteParameter> lps=new List<SQLiteParameter>();
                foreach(KeyValuePair<string,dynamic> kvp in cmdps)
                {
                     SQLiteParameter pst=new SQLiteParameter(kvp.Key,kvp.Value);
                     lps.Add(pst);
                } 
                cmd.Parameters.AddRange(lps.ToArray());
                string rc1 = cmd.ExecuteScalar().ToString();
                return rc1;
            }
        }
       
        //---------------------------------------------------------------
        //                  获取NpgsqlDataReader
        //  xypzf lastdate 2018.2.25
        //---------------------------------------------------------------
        /// <summary>
        /// 获取NpgsqlDataReader,以便流式处理表行数据
        /// </summary>
        /// <param name="cmdstr">SQL查询字符串</param>     
        /// <returns>返回值NpgsqlDataReader</returns>  
        public static SQLiteDataReader GetDataReader(string cmdstr)
        {
            string StrCon = Models.common.strcon;
            SQLiteConnection conn = new SQLiteConnection(StrCon);
            conn.Open();//连接会在生成rd时，通过参数设置自动关闭
            SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
            SQLiteDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);                    
            return rd;

        }
        /// <summary>
        /// 预处理方式获取NpgsqlDataReader,以便流式处理表行数据
        /// </summary>
        /// <param name="cmdstr">SQL查询字符串</param>   
        /// <param name="vars">查询SQL字符串中带的n个变量及对应值</param>   
        /// <returns>返回值NpgsqlDataReader</returns>  
        public static SQLiteDataReader GetDataReader(string cmdstr, SQLiteParameter[] vars)
        {
            string StrCon = Models.common.strcon;
            SQLiteConnection conn = new SQLiteConnection(StrCon);
            conn.Open();
            SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
            cmd.Parameters.AddRange(vars);
            SQLiteDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);//带参自动关闭连接      
            cmd.Parameters.Clear();
            return rd;

        }
        public static SQLiteDataReader GetDataReader(string cmdstr,Dictionary<string,dynamic> cmdps)
        {
            string StrCon = Models.common.strcon;
            SQLiteConnection conn = new SQLiteConnection(StrCon);
            conn.Open();
            SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
            List<SQLiteParameter> lps=new List<SQLiteParameter>();
            foreach(KeyValuePair<string,dynamic> kvp in cmdps)
                {
                     SQLiteParameter pst=new SQLiteParameter(kvp.Key,kvp.Value);
                     lps.Add(pst);
                } 
            cmd.Parameters.AddRange(lps.ToArray());
            SQLiteDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);//带参自动关闭连接      
            cmd.Parameters.Clear();
            return rd;

        }
        //---------------------------------------------------------------
        //                  SQL查询获取DataTable  
        //  xypzf lastdate 2018.2.2
        //---------------------------------------------------------------
        /// <summary>
        /// SQL查询，获取DataTable
        /// </summary>
        /// <param name="cmdstr">SQL查询字符串</param>     
        /// <returns>返回值DataTable</returns>  
        public static DataTable GetDataTable(string cmdstr)
        {
            DataTable dt = new DataTable();
            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                // conn.Open();
                SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
                SQLiteDataAdapter NpgDa = new SQLiteDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                NpgDa.Fill(dt1);
                dt = dt1.Copy();
                dt1.Dispose();
                NpgDa.Dispose();

            }
            return dt;
        }
        /// <summary>
        /// 预处理SQL查询，获取DataTable
        /// </summary>
        /// <param name="cmdstr">SQL查询字符串</param> 
        /// <param name="vars">查询SQL字符串中带的n个变量及对应值</param>   
        /// <returns>返回值DataTable</returns>  
        public static DataTable GetDataTable(string cmdstr,  SQLiteParameter[] vars)
        {
            DataTable dt = new DataTable();
            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                // conn.Open();
                SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
                cmd.Parameters.AddRange(vars);
                SQLiteDataAdapter NpgDa = new SQLiteDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                NpgDa.Fill(dt1);
                dt = dt1.Copy();
                dt1.Dispose();
                NpgDa.Dispose();
            }
            return dt;
        }

     public static DataTable GetDataTable(string cmdstr, Dictionary<string,dynamic> cmdps)
        {
            DataTable dt = new DataTable();
            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                // conn.Open();
                SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
                 List<SQLiteParameter> lps=new List<SQLiteParameter>();
                foreach(KeyValuePair<string,dynamic> kvp in cmdps)
                {
                     SQLiteParameter pst=new SQLiteParameter(kvp.Key,kvp.Value);
                     lps.Add(pst);
                } 
                cmd.Parameters.AddRange(lps.ToArray());
                SQLiteDataAdapter NpgDa = new SQLiteDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                NpgDa.Fill(dt1);
                dt = dt1.Copy();
                dt1.Dispose();
                NpgDa.Dispose();
            }
            return dt;
        }
        //---------------------------------------------------------------
        //                  SQL查询内容填充到DataSet
        //  xypzf lastdate 2018.2.2
        //---------------------------------------------------------------
        /// <summary>
        ///  SQL查询内容填充到DataSet
        /// </summary>
        /// <param name="cmdstr">SQL查询字符串</param> 
        /// <param name="ds">查填充的DataSet</param>   
        /// <returns>无返回</returns>  
        public static void FillDataSet(string cmdstr, DataSet ds)
        {
            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
               SQLiteDataAdapter NpgDa = new SQLiteDataAdapter(cmd);
                NpgDa.Fill(ds);
            }
        }
        /// <summary>
        ///  SQL查询内容填充到DataSet
        /// </summary>
        /// <param name="cmdstr">SQL查询字符串</param> 
        /// <param name="ds">查填充的DataSet</param>  
        /// <param name="dn">自定义表名</param> 
        /// <returns>无返回</returns>  
        public static void FillDataSet(string cmdstr, DataSet ds, string dn)
        {
            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
               SQLiteDataAdapter NpgDa = new SQLiteDataAdapter(cmd);
                NpgDa.Fill(ds, dn);
            }
        }
        /// <summary>
        ///  预处理方式SQL查询内容填充到DataSet
        /// </summary>
        /// <param name="cmdstr">SQL查询字符串</param> 
        /// <param name="ds">查填充的DataSet</param>        
        ///  <param name="vars">查询SQL字符串中带的n个变量及对应值</param> 
        /// <returns>无返回</returns>  
        public static void FillDataSet(string cmdstr, DataSet ds,  SQLiteParameter[] vars)
        {
            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
                cmd.Parameters.AddRange(vars);
               SQLiteDataAdapter NpgDa = new SQLiteDataAdapter(cmd);
                NpgDa.Fill(ds);
            }
        }
         public static void FillDataSet(string cmdstr, DataSet ds,Dictionary<string,dynamic> cmdps)
        {
            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
                 List<SQLiteParameter> lps=new List<SQLiteParameter>();
                foreach(KeyValuePair<string,dynamic> kvp in cmdps)
                {
                     SQLiteParameter pst=new SQLiteParameter(kvp.Key,kvp.Value);
                     lps.Add(pst);
                } 
                cmd.Parameters.AddRange(lps.ToArray());
               SQLiteDataAdapter NpgDa = new SQLiteDataAdapter(cmd);
                NpgDa.Fill(ds);
            }
        }
        /// <summary>
        ///  预处理方式SQL查询内容填充到DataSet
        /// </summary>
        /// <param name="cmdstr">SQL查询字符串</param> 
        /// <param name="ds">查填充的DataSet</param> 
        /// <param name="dn">自定义表名</param>
        ///  <param name="vars">查询SQL字符串中带的n个变量及对应值</param> 
        /// <returns>无返回</returns>  
        public static void FillDataSet(string cmdstr, DataSet ds, string dn,  SQLiteParameter[] vars)
        {
            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
                cmd.Parameters.AddRange(vars);
               SQLiteDataAdapter NpgDa = new SQLiteDataAdapter(cmd);
                NpgDa.Fill(ds, dn);
            }
        }
       public static void FillDataSet(string cmdstr, DataSet ds, string dn, Dictionary<string,dynamic> cmdps)
        {
            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
                 List<SQLiteParameter> lps=new List<SQLiteParameter>();
                foreach(KeyValuePair<string,dynamic> kvp in cmdps)
                {
                     SQLiteParameter pst=new SQLiteParameter(kvp.Key,kvp.Value);
                     lps.Add(pst);
                } 
                cmd.Parameters.AddRange(lps.ToArray());
               SQLiteDataAdapter NpgDa = new SQLiteDataAdapter(cmd);
                NpgDa.Fill(ds, dn);
            }
        }


        //---------------------------------------------------------------
        //                   单条  增  删  改  函数   
        //  xypzf lastdate 2018.2.2
        //---------------------------------------------------------------
        /// <summary>
        ///执行一条增、删、改SQL命令
        /// </summary>
        /// <param name="cmdstr">一条增删改SQL字符串</param>     
        /// <returns>返回1批处理成功，0失败</returns>  
        public static int InsertDelEdit(string cmdstr)//插，删，改 //一条sql处理
        {
            try
            {
                string StrCon = Models.common.strcon;
                using (SQLiteConnection conn = new SQLiteConnection(StrCon))
                {
                    conn.Open();
                    SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
                    int fl = cmd.ExecuteNonQuery();
                    return fl;
                }
            }
            catch
            {
                return 0;
            }
        }
        /// <summary>
        ///预处理方式执行一条增、删、改SQL命令
        /// </summary>
        /// <param name="cmdstr">一条增删改SQL字符串</param>
        /// <param name="vars">一条增删改SQL字符串带的n个变量及对应值</param>
        /// <returns>返回1批处理成功，0失败</returns>        
        public static int InsertDelEdit(string cmdstr,  SQLiteParameter[] vars)//插，删，改 //一条sql预处理方式处理 
        {
            try
            {
                string StrCon = Models.common.strcon;
                using (SQLiteConnection conn = new SQLiteConnection(StrCon))
                {
                    conn.Open();
                    SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
                    cmd.Parameters.AddRange(vars);
                    int fl = cmd.ExecuteNonQuery();
                    return fl;
                }
            }
            catch 
            {
                // MessageBox.Show(e.Message);
                return 0;
            }
        }
       public static int InsertDelEdit(string cmdstr,Dictionary<string,dynamic> cmdps)//插，删，改 //一条sql预处理方式处理 
        {
            try
            {
                string StrCon = Models.common.strcon;
                using (SQLiteConnection conn = new SQLiteConnection(StrCon))
                {
                    conn.Open();
                    SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
                     List<SQLiteParameter> lps=new List<SQLiteParameter>();
                    foreach(KeyValuePair<string,dynamic> kvp in cmdps)
                    {
                        SQLiteParameter pst=new SQLiteParameter(kvp.Key,kvp.Value);
                        lps.Add(pst);
                    } 
                     cmd.Parameters.AddRange(lps.ToArray());
                    int fl = cmd.ExecuteNonQuery();
                    return fl;
                }
            }
            catch 
            {
               // MessageBox.Show(e.Message);
                return 0;
            }
        }
        //---------------------------------------------------------------
        //                  批处理  增  删  改  函数   
        //  xypzf lastdate 2018.2.2
        //---------------------------------------------------------------

        /// <summary>
        ///执行一批增、删、改SQL命令  
        /// </summary>
        /// <param name="cmdstrs">多条SQL命令字符串数组例： string[] cmdsqls =new string[2];</param>
        /// <returns>返回1批处理成功，0失败</returns> 
        public static int TranInsertDelEdit(string[] cmdstrs)
        {

            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                 SQLiteTransaction tran = conn.BeginTransaction();//开始事务处理
                try
                {
                    SQLiteCommand cmd = new SQLiteCommand();
                    int f1 = 0;
                    for (int i = 0; i < cmdstrs.Length; i++)
                    {
                        cmd = new SQLiteCommand(cmdstrs[i], conn,tran);
                        f1 = cmd.ExecuteNonQuery();
                    }
                    tran.Commit();//事务提交
                    return 1;
                }
                catch
                {
                    tran.Rollback();//回滚事务
                    return 0;
                }

            }
        }
        /// <summary>
        ///预处理方式执行一批增、删、改SQL命令 (调用时注意：SQL字段类型与参数传值类型要一值）
        /// </summary>
        /// <param name="cmdstrs">多条SQL命令字符串数组例： string[] cmdsqls =new string[2];</param>
        /// <param name="listvars">对应多条SQL命令字符串包含预处理变量的预处理参数集,如果其中一条SQL无预处理变量，请对应参数设为Null;</param>
        /// <returns>返回1批处理成功，0失败</returns> 
        public static int TranInsertDelEdit(string[] cmdstrs, List< SQLiteParameter[]> listvars)//插，删，改//一批预处理sql处理
        {

            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                SQLiteTransaction tran = conn.BeginTransaction();//开始事务处理
                try
                {
                    SQLiteCommand cmd = new SQLiteCommand();
                    int f1 = 0;
                    for (int i = 0; i < cmdstrs.Length; i++)
                    {
                        //string a1 = lps[i][0].Value.ToString() + ":" + lps[i][1].Value.ToString() + ":" + lps[i][2].Value.ToString();
                        //MessageBox.Show( cmdstr[i].ToString());
                        cmd = new SQLiteCommand(cmdstrs[i], conn, tran);
                        if (listvars[i] != null)
                        {
                            cmd.Parameters.AddRange(listvars[i]);
                        }
                        f1 = cmd.ExecuteNonQuery();
                    }
                    tran.Commit();//事务提交                    
                    return 1;
                } catch 
                {
                    tran.Rollback();
                   // MessageBox.Show(ex.ToString());
                    return 0;
                }
                   
                
            }
           
        }
        public static int TranInsertDelEdit(string[] cmdstrs, List<Dictionary <string,dynamic>> listcmdps)//插，删，改//一批预处理sql处理
        {

            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                SQLiteTransaction tran = conn.BeginTransaction();//开始事务处理
                try
                {
                    SQLiteCommand cmd = new SQLiteCommand();
                    int f1 = 0;
                    for (int i = 0; i < cmdstrs.Length; i++)
                    {
                        //string a1 = lps[i][0].Value.ToString() + ":" + lps[i][1].Value.ToString() + ":" + lps[i][2].Value.ToString();
                        //MessageBox.Show( cmdstr[i].ToString());
                        cmd = new SQLiteCommand(cmdstrs[i], conn, tran);
                        if (listcmdps[i] != null)
                        {
                                List<SQLiteParameter> lps=new List<SQLiteParameter>();
                                foreach(KeyValuePair<string,dynamic> kvp in listcmdps[i] )
                                {
                                    SQLiteParameter pst=new SQLiteParameter(kvp.Key,kvp.Value);
                                    lps.Add(pst);
                                } 
                                cmd.Parameters.AddRange(lps.ToArray());                            
                        }
                        f1 = cmd.ExecuteNonQuery();
                    }
                    tran.Commit();//事务提交                    
                    return 1;
                } catch 
                {
                    tran.Rollback();
                   // MessageBox.Show(ex.ToString());
                    return 0;
                }
                   
                
            }
           
        }
        //---------------------------------------------------------------
        //                  获取DataAdapter 
        //  xypzf lastdate 2018.2.2
        //---------------------------------------------------------------
        /// <summary>
        /// 获取SQLiteDataAdapter ,以便操纵 dataset增删改,在填充前先建立关连  SQLiteCommandBuilder cb = new SQLiteCommandBuilder(MyDa);
        /// </summary>
        /// <param name="cmdstr">SQL查询字符串</param>     
        /// <returns>返回值NpgsqlDataAdapter</returns>  
        public static SQLiteDataAdapter GetDataAdapter(string cmdstr)
        {
            string StrCon = Models.common.strcon;
            SQLiteConnection conn = new SQLiteConnection(StrCon);
            //conn.Open();     
            SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
            SQLiteDataAdapter Da = new SQLiteDataAdapter(cmd);           
            return Da;

        }
        /// <summary>
        /// 预处理方式获取SQLiteDataAdapter ,以便操纵 dataset增删改,在填充前先建立关连  SQLiteCommandBuilder cb = new SQLiteCommandBuilder(MyDa);
        /// </summary>
        /// <param name="cmdstr">SQL查询字符串</param>   
        /// <param name="vars">查询SQL字符串中带的n个变量及对应值</param>   
        /// <returns>返回值NpgsqlDataAdapter</returns>  
        public static SQLiteDataAdapter GetDataAdapter(string cmdstr,  SQLiteParameter[] vars)
        {
            string StrCon = Models.common.strcon;
            SQLiteConnection conn = new SQLiteConnection(StrCon);
            // conn.Open();
            SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
            cmd.Parameters.AddRange(vars);
           SQLiteDataAdapter Da = new SQLiteDataAdapter(cmd);
            return Da;

        }
         public static SQLiteDataAdapter GetDataAdapter(string cmdstr, Dictionary<string,dynamic> cmdps)
        {
            string StrCon = Models.common.strcon;
            SQLiteConnection conn = new SQLiteConnection(StrCon);
            // conn.Open();
            SQLiteCommand cmd = new SQLiteCommand(cmdstr, conn);
            List<SQLiteParameter> lps=new List<SQLiteParameter>();
                                foreach(KeyValuePair<string,dynamic> kvp in cmdps )
                                {
                                    SQLiteParameter pst=new SQLiteParameter(kvp.Key,kvp.Value);
                                    lps.Add(pst);
                                } 
                                cmd.Parameters.AddRange(lps.ToArray());   
           SQLiteDataAdapter Da = new SQLiteDataAdapter(cmd);
            return Da;

        }
        /// <summary>
        /// 只适合单表操作，有主键，没外键
        /// </summary>
        /// <param name="cmdstrs">查询语言集合，增改删sql</param>
        /// <param name="cmdstrstype">两个值"sql"表示是增改删除语句，"dtsql"表示是做datatable更新的查询sql</param>
        /// <param name="listdt">查询sql对应的更新datatable,listdt的数据结构要与查询sql的数据结库一致</param>
        /// <returns></returns>
        public static int TranIUD_and_DT(string[] cmdstrs,string[] cmdstrstype,List<DataTable> listdt)//2 sql,dtsql
        {

            string StrCon = Models.common.strcon;
            using (SQLiteConnection conn = new SQLiteConnection(StrCon))
            {
                conn.Open();
                SQLiteTransaction tran = conn.BeginTransaction();//开始事务处理
                try{
                    //start                    
                    int f1 = 0;
                    int dtflag = 0;
                    SQLiteCommand cmd = new SQLiteCommand();
                    for (int i = 0; i < cmdstrs.Length; i++)
                    {
                       cmd = new SQLiteCommand(cmdstrs[i], conn, tran);

                        if (cmdstrstype[i] == "sql")
                        {

                            f1 = cmd.ExecuteNonQuery();

                        }
                        else
                        {  //查询da到dt

                            SQLiteDataAdapter Da = new SQLiteDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            SQLiteCommandBuilder builder = new SQLiteCommandBuilder(Da);
                            Da.Fill(dt);
                            dt.Rows.Clear();
                            for (int j = 0; j < listdt[dtflag].Rows.Count; j++)//行赋值
                            {
                                dt.ImportRow(listdt[dtflag].Rows[j]);
                            }
                            f1 = Da.Update(dt);
                            dtflag++;                             
                        }
                    }
                    //end
                    tran.Commit();//事务提交
                    return 1;
                }catch
                {
                    tran.Rollback();                  
                    return 0;
                }
            }
           
        }
        //****************





    }
}
