﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace ConsoleApp1.Models
{
    class common
    {
        public static string strcon = "Data Source=./NetCoreFM.db3";//定义连接字符串，在startup.cs已做覆盖
        public static string EncryptByMD5(string input)//md5加密，和php的md5(str,false)结果一样
        {
            MD5 md5o = MD5.Create();
            byte[] data = md5o.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2")); //将每个字节转为两位格式16进制,如01,1F不足两位以0占位
            }
            return sBuilder.ToString();
        }
    }
}
