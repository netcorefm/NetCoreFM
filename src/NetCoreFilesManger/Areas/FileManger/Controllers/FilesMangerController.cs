﻿using System;
using System.IO;
using System.IO.Compression;
using System.Data.SQLite;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using NetCoreFilesManger.Models;
using NetCoreFilesManger.Models.PEntity;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.FileProviders.Physical;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using NPOI.SS.Formula.Functions;

namespace NetCoreFilesManger.Controllers

{
    public class  psdata
    {  //Trashfiles使用
         public  ComputerDiskinfo   f1{ get; set; }
         public JArray f2 { get; set; }
         
    }
    public class computerdisk_viewpsdata
    {  //computerdisk使用
         public  List<ComputerDiskinfo> diskpaths { get; set; }
         public ComputerDoucmentinfo dirpath { get; set; }
         
    }
    public class  ComputerDiskinfo
    {
         public string xpath { get; set; }
         public double usesum { get; set; }
         public double totalsum { get; set; }
         public string label { get; set; }
    }
    public class  ComputerDoucmentinfo
    {
         public string docpath { get; set; }
         public string docfalg { get; set; }
         public string imgpath { get; set; }
         public string imgfalg { get; set; }
         public string videopath { get; set; }
         public string videofalg { get; set; }
        
    }
     [Area("FileManger")]
    public class FilesMangerController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;   
        private readonly  WebSets _websiteset;  
      
        public FilesMangerController(IHostingEnvironment hostingEnvironment,IOptionsSnapshot<WebSets> websets)
        {
           
            _hostingEnvironment = hostingEnvironment;//注入IHostingEnvironment获取路径用
             _websiteset=websets.Value;            
        }  
        /// <summary>
       /// 加载框布局页面
       /// </summary>
       /// <returns></returns>
        [TypeFilter(typeof(CheckUserLoginFilter))]
        [CheckUserPermissionFilter("我的电脑")]
        public  IActionResult MyComputerDisk()
        {   
            
            //定义页面导航
            //   --/home--
            Models.PEntity.BreadCrumbNode[] bcnodes = new BreadCrumbNode[3];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");    
            bcnodes[1] = new BreadCrumbNode("文件资源管理器", "#");   
            bcnodes[2] = new BreadCrumbNode("我的电脑", "/FileManger/FilesManger/MyComputerDisk");           
            ViewData["BreadCrumbNode_Data"] = bcnodes;
            ViewData["UserID"] = HttpContext.Session.GetString("xsdUserID");
            ViewData["UserName"] = HttpContext.Session.GetString("xsdUserName");
                     
             //*********************************************
                //以下处理页面数据------------------------
                  
                   string uid=HttpContext.Session.GetString("xsdUserID");
                   string sqltui= "Select f0003,f0005 From urp_userinfo where f0001=@uid";   
                    SQLiteParameter[] ps= new  SQLiteParameter[]
                     {
                       new SQLiteParameter("@uid",int.Parse(uid))
                       }; 
                  DataTable dt1= Models.DataBase_SQLite.GetDataTable(sqltui,ps);
                  string pathps =dt1.Rows[0][0].ToString(); //Models.DataBase_SQLite.GetOneValue(sqltui,ps);
                  string[] paths = pathps.Split(";");
                  List<ComputerDiskinfo> lc=new List<ComputerDiskinfo> ();
                  for(int i=0 ;i<paths.Length;i++)
                  {   //检查路径文件夹是否存在
                    if (System.IO.Directory.Exists(paths[i].Split(",")[0].Trim ()))
                    {
                        ComputerDiskinfo p1=new ComputerDiskinfo ();                        
                        p1.xpath=paths[i].Split(",")[0]; //路径
                        p1.usesum = double.Parse(paths[i].Split(",")[3]);//使用额
                        p1.totalsum = double.Parse(paths[i].Split(",")[1]);//限额
                        p1.label=paths[i].Split(",")[2];//磁盘标签
                        p1.xpath=Models.StringSecurity.DESEncrypt(p1.xpath);//对路径加密
                        lc.Add(p1);
                     }
                  }
                  string pathps1  =dt1.Rows[0][1].ToString();
                  string[] paths1 = pathps1.Split(";");
                  ComputerDoucmentinfo cdi=new ComputerDoucmentinfo ();                   
                  cdi.docpath=Models.StringSecurity.DESEncrypt(paths1[0].Split(",")[0].Trim());
                  cdi.docfalg="display:none";
                  if(paths1[0].Split(",")[1].Trim()=="true")  cdi.docfalg="";  

                  cdi.imgpath=Models.StringSecurity.DESEncrypt(paths1[1].Split(",")[0].Trim ());
                  cdi.imgfalg="display:none";
                  if(paths1[1].Split(",")[1].Trim()=="true")  cdi.imgfalg="";    

                  cdi.videopath=Models.StringSecurity.DESEncrypt(paths1[2].Split(",")[0].Trim());
                  cdi.videofalg="display:none";
                  if(paths1[2].Split(",")[1].Trim()=="true")  cdi.videofalg="";  

                  computerdisk_viewpsdata vps=new computerdisk_viewpsdata ();
                  vps.diskpaths=lc;
                  vps.dirpath=cdi;
                  return this.View(vps);
 
        } 
         [HttpPost]
          public string DiskSpaceUseCount([FromBody] dynamic data )
          {  
             string uid=data.uid;
             string msg= UpdateUseSpace( uid);
             return msg;
          }

        Func<string,string> computerdiskspace = (uid) => {             
           
            return UpdateUseSpace(uid); 
        
        };

        public static string UpdateUseSpace(string uid)
    {   

        string msg="操作失败！";
            try{
             
              string f3="";
              string sqltui= "Select f0003 From urp_userinfo where f0001=@uid";   
              SQLiteParameter[] ps= new  SQLiteParameter[]
               {
                       new SQLiteParameter("@uid",int.Parse(uid))
               };                
               string pathps = Models.DataBase_SQLite.GetOneValue(sqltui,ps);
               string[] paths = pathps.Split(";");
               for(int i=0 ;i<paths.Length;i++)
               {
                   if(paths[i].Trim()!="")
                   {
                         f3=f3+paths[i].Split(",")[0]+","+paths[i].Split(",")[1]+","+paths[i].Split(",")[2]+","+getdirlength(paths[i].Split(",")[0]).ToString()+";";
                   }                 
                          
               }
                f3 = f3.Substring(0,f3.Length - 1);//去除最后一个字符 ;
                string sqld = "update urp_userinfo set f0003=@f3  WHERE f0001=@f1";
                Dictionary <string,dynamic> psd1=new Dictionary<string,dynamic>();
                psd1.Add("@f3", f3);                           
                psd1.Add("@f1",int.Parse(uid));                            
                int fd1 = Models.DataBase_SQLite.InsertDelEdit(sqld, psd1);
                if (fd1 == 1)
                {
                    msg = "磁盘使用大小更新成功！";
                }
                else
                {
                    msg = "磁盘使用大小更新失败！";

                }   
               
            }catch(Exception ex)
            {
               msg=msg+"路径配置不正确"+ex.Message;
            }
             return msg;

    }
    /// <summary>
    /// 得到目录的总大小
    /// </summary>
    /// <returns></returns>
    
    public  static long  getdirlength(string xpath)
    {  
         long sum=0;
         var provider = new PhysicalFileProvider(xpath ,ExclusionFilters.None );   
         try{     
                var contents = provider.GetDirectoryContents(string.Empty);          
                
                      
                foreach (var item in contents)
                {
                    
                    if (item.IsDirectory)
                    {   
                        if(!System.IO.File.Exists(item.PhysicalPath+Path.DirectorySeparatorChar+item.Name+".txt") )
                        {  
                            long temp1= Task.Run<long>(() => { return getdirlength(item.PhysicalPath); }).Result;
                            sum = sum + temp1;
                        }
                        else
                        {
                            //排除删除标志的文件夹
                        }

                    }else
                    {  
                        sum=sum+item.Length;
                    }
                }
         }catch
         {
             sum=0;
         }
         return sum;
    }
       /// <summary>
       /// 加载框布局页面
       /// </summary>
       /// <returns></returns>
       [TypeFilter(typeof(CheckUserLoginFilter))]
       [CheckUserPermissionFilter("我的电脑")]
        public  IActionResult MyComputer([FromBody] dynamic data)
        {   

            //定义页面导航
            //   --/home--
            Models.PEntity.BreadCrumbNode[] bcnodes = new BreadCrumbNode[3];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");    
            bcnodes[1] = new BreadCrumbNode("文件资源管理器", "#");   
            bcnodes[2] = new BreadCrumbNode("我的电脑", "/FileManger/FilesManger/MyComputerDisk");           
            ViewData["BreadCrumbNode_Data"] = bcnodes;           
           
             //*********************************************
            //以下处理页面数据------------------------
            string desxpath=data.xpathroot;
            string xpathusesize = data.xpathusesize;
            string xpathtotal = data.xpathtotal;
            ViewData["xpathroot"]="Disk:/";            
            ViewData["UserID"] = HttpContext.Session.GetString("xsdUserID");
            ViewData["UserName"] = HttpContext.Session.GetString("xsdUserName");
             
                
                string optpath=Models.StringSecurity.DESDecrypt(desxpath);             
                HttpContext.Session.SetString("FileOptCPath",optpath);//当前操作路径
            HttpContext.Session.SetString("FileOptCPathTotalSize",xpathtotal);//当前操作路径总空间
            HttpContext.Session.SetString("FileOptCPathUseSize", xpathusesize);//当前操作路径总使用空间
            HttpContext.Session.SetString("FileUploadMode", "OverOFF");//设置文件上传默认方式,为不覆盖

            HttpContext.Session.SetString("FileOptRPath",optpath);//工作根目录
                   string uid=HttpContext.Session.GetString("xsdUserID");                 
                   string sqltui= "Select f0004 From urp_userinfo where f0001=@uid";   
                    SQLiteParameter[] ps= new  SQLiteParameter[]
                     {
                       new SQLiteParameter("@uid",int.Parse(uid))
                       }; 
                  string pathps = Models.DataBase_SQLite.GetOneValue(sqltui,ps);
                  string[] paths =pathps.Split(",");
                 HttpContext.Session.SetString("trashPath",paths[0].Trim());//回收站路径                           
                return this.View(); 
        }  
        /// <summary>
        /// 设置上传模式是否为覆盖
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string SetUpLoadFileModel([FromBody]dynamic data)
        {
            
            string fm = data.filemode;
            string msg = "设置为完成";
            HttpContext.Session.SetString("FileUploadMode",fm);//设置文件上传默认方式,为不覆盖
            return msg;

        }
          /// <summary>
       /// 上传显示
       /// </summary>
       /// <returns></returns>
        public  IActionResult MyComputer_upload()
        { 
            return this.View();
        }     
       /// <summary>
       /// 下载显示页面弹窗，列出所有要下载的文件
       /// </summary>
       /// <returns></returns>
          public  IActionResult MyComputer_download([FromBody] dynamic datas)
        {
            //传参过来是文件名或文件夹名的选择集  datas由 data = { name:n,type:t}组成
            List<string> ls = new List<string>();
            string xpath=HttpContext.Session.GetString("FileOptCPath");//得到当前目录
            string rpath= HttpContext.Session.GetString("FileOptRPath");//得到根目录       
            @ViewData["cpath"]= xpath.Replace(rpath.ToString().Trim(),"Disk:/");//显示当前目录(去根显示)
            foreach (var data in datas)
            {
                if (data.type == "文件夹")
                {
                    //如果是文件夹列出所有文件然后下载
                    string workpath = xpath + data.name;
                    var provider = new PhysicalFileProvider(workpath);
                    var DContents = provider.GetDirectoryContents(string.Empty);
                    ls.AddRange(listdirfile(DContents).ToArray());
                }
                else
                {  //如果是文件直接下载               
                    var path = xpath + data.name; //实际路径
                    ls.Add(path.Replace(xpath.ToString().Trim(),""));//显示路径,去除当前路径

                }
            }
            return this.View(ls);

        }   
         
        public List<string>  listdirfile( IDirectoryContents dirs)
        {

           // string workrpath= HttpContext.Session.GetString("FileOptRPath"); //根路径
            string xpath= HttpContext.Session.GetString("FileOptCPath"); //当前路径
            List<string> ds = new List<string>();
            foreach(var item in dirs)
            {
                if (item.IsDirectory)
                {  //如果是目录，继续遍历文件
                    var provider = new PhysicalFileProvider(item.PhysicalPath);
                    var DContents = provider.GetDirectoryContents(string.Empty);                    
                    ds.AddRange(listdirfile(DContents).ToArray());
                }
                else
                {   //如果是文件，添架显示，去当前路径
                    ds.Add(item.PhysicalPath.ToString().Replace(Path.DirectorySeparatorChar,'/').Replace(xpath.ToString().Trim(),""));//去当前路径
                }

            }
           
            return ds;
        }
         [TypeFilter(typeof(CheckUserLoginFilter))]
        public IActionResult downloadfile(string filename)
        {           //下载页面里点击下载单个文件
                    string xpath= HttpContext.Session.GetString("FileOptCPath");
                    var addrUrl =xpath.Trim()+filename;
                    var stream = System.IO.File.OpenRead(addrUrl);
                    string fileExt = Path.GetExtension(filename);               
                    return File(stream, "application/octet-stream", Path.GetFileName(addrUrl));
            
        } 
        //预览图片，以height为参考高宽缩略图
         public string preimg(string filename,int changkuan)
        {            
            string xpath=HttpContext.Session.GetString("FileOptCPath");   
            var addrUrl =xpath+filename;
            return NetCoreFilesManger.Areas.FileManger.Models.thumbnail.MakeImgPreBase64_JZ(addrUrl,changkuan);
                    
        }     
        public List<string>  listdir_and_file( IDirectoryContents dirs)
        {

            //string workrpath= HttpContext.Session.GetString("FileOptRPath"); //根路径
            string xpath= HttpContext.Session.GetString("FileOptCPath"); //当前路径
            List<string> ds = new List<string>();
            foreach(var item in dirs)
            {
                if (item.IsDirectory)
                {  
                     //去当前路径,添加目录
                    ds.Add(item.PhysicalPath.ToString().Replace(Path.DirectorySeparatorChar,'/').Replace(xpath.ToString().Trim(),"")+"/");
                    //如果是目录，继续遍历文件
                    var provider = new PhysicalFileProvider(item.PhysicalPath);
                    var DContents = provider.GetDirectoryContents(string.Empty); 
                    ds.AddRange(listdir_and_file(DContents).ToArray());
                   
                    
                }
                else
                {   //如果是文件，去当前路径
                    ds.Add(item.PhysicalPath.ToString().Replace(Path.DirectorySeparatorChar,'/').Replace(xpath.ToString().Trim(),""));
                }

            }
           
            return ds;
        }
        //打包下载
        public IActionResult downloadzipfile([FromBody] dynamic datas)
        {       
            //传参过来是文件名或文件夹名的选择集  datas由 data = { name:n,type:t}组成        
            List<string> ls=new List<string>(); //压缩文件    全路径及名
            List<string> ls1=new List<string>();//压缩文件名，无当前路径
            string xpath=HttpContext.Session.GetString("FileOptCPath"); //默认是“/"分割             
            foreach (var data in datas)
            {
                
               
                 //如果是目录递归列出 子目录与文件
                 if (data.type == "文件夹")
                 {
                    string sdname=data.name;//得到当前遍历目录名称
                    string workpath = xpath +sdname;  //得到当前遍历目录路径 
                    //添加目录
                    ls.Add(xpath.Trim()+sdname+"/");
                    ls1.Add(sdname+"/");
                    //递归遍历目录         
                    var provider = new PhysicalFileProvider(workpath);
                    var DContents = provider.GetDirectoryContents(string.Empty);
                    List<string> cls=listdir_and_file(DContents);//得到目录下所以文件及文件夹
                    foreach( string lj in cls)
                    {       
                            ls.Add(xpath.Trim()+lj.Trim());//
                            ls1.Add(lj.Trim());
                    }
                    
                     
                }else
                {
                      //如果是文件或文件夹直接下载              
                        string sname=data.name;
                        ls.Add(xpath.Trim()+sname);
                        ls1.Add(sname);
                }
                 
            }       
       
        byte[] compressedBytes;
        using (var outStream = new MemoryStream())
        {
            using (var archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
            {  
                              
                for(int i=0;i<ls.Count;i++)
                {
                   var fileInArchive = archive.CreateEntry(ls1[i].ToString(), CompressionLevel.Optimal);
                   if(System.IO.File.Exists(ls[i].ToString()))//如果是文件路径添加数据到压缩包，不是说明是目录实体
                   {
                        byte[] fileBytes = System.IO.File.ReadAllBytes(ls[i].ToString());
                        using (var entryStream = fileInArchive.Open())
                        using (var fileToCompressStream = new MemoryStream(fileBytes))
                        {
                            fileToCompressStream.CopyTo(entryStream);
                        }
                   }
                }
            }
            compressedBytes = outStream.ToArray();
        }          
        return     File(compressedBytes, "application/octet-stream", "ptemp.zip");
            
        } 
        [HttpPost]
          public string DeleteDirAllFile([FromBody] dynamic datas )
        {   
        string userid6 = HttpContext.Session.GetString("xsdUserID");
        string zxpwd6=HttpContext.Session.GetString("xsd_zxPwd");                       
        if(Models.common.testzxpwd(zxpwd6,userid6))
        {
             string xpath=HttpContext.Session.GetString("FileOptCPath");
             string tpath=HttpContext.Session.GetString("trashPath")+DateTime.Now.ToString("yyyyMMdd_HHmmss");     
             try{

                System.IO.Directory.CreateDirectory(tpath);
                foreach (var data in datas)
                {
                    var pathf = xpath + data.name;
                    if (data.type == "文件夹")
                    {    
                        System.IO.Directory.Move(pathf,tpath+@"/"+data.name); 
                
                    }else
                    {     
                          System.IO.File.Move(pathf,tpath+@"/"+data.name);
                         // System.IO.File.Delete(pathf );
                    }                   
                }
                string uid=  HttpContext.Session.GetString("xsdUserID");
                string msg=UpdateUseSpace( uid);
                  return "删除成功!<br>"+msg;
            }catch
            {
              return "删除失败!";
            }
        }else
        {
            return "未开启管理模式!";
        }
           
        }
        [HttpPost]
          public string CopySelectFile([FromBody] dynamic datas )
        {  
            string xpath=HttpContext.Session.GetString("FileOptCPath");
            var ds=datas;
            string pp=JsonConvert.SerializeObject(ds);           
            HttpContext.Session.SetString("PastePath",xpath);//剪贴板路径
            HttpContext.Session.SetString("Pasteflag","1");//剪贴板标志
            HttpContext.Session.SetString("PasteDirFiles",pp);//剪贴板内容           
            return "已复制！";
           
        }
         [HttpPost]
          public string pasteSelectFile( )
        {    
            string opath=HttpContext.Session.GetString("FileOptCPath");      
            string spath=HttpContext.Session.GetString("PastePath");
            dynamic pdfs=HttpContext.Session.GetString("PasteDirFiles");
            string pasteflag=HttpContext.Session.GetString("Pasteflag");
            if(pasteflag=="1")
            {   
                bool v1=true;
                HttpContext.Session.SetString("Pasteflag","0");//剪贴板标志
                if(pdfs!=null && pdfs!="")
                {
                    JArray ja=JArray.Parse(pdfs);
                    for(int i=0;i<ja.Count;i++)
                    {
                        JObject jo = JObject.Parse(ja[i].ToString());
                        if(jo["type"].ToString()=="文件夹")
                        {
                              v1= CopyDirectory(spath+jo["name"].ToString(),opath+jo["name"].ToString());
                        }else
                        {  
                              try{
                                  System.IO.File.Copy(spath+jo["name"].ToString(),opath+jo["name"].ToString(),false);
                              }catch
                              {
                                  v1=false;
                              }
                        }
                    }
                    if(v1==true)
                    {
                        string uid=  HttpContext.Session.GetString("xsdUserID");
                        string msg=UpdateUseSpace( uid);
                        return "非覆盖方式_粘贴成功！<br>"+msg;
                    }else
                    {
                        return "粘贴中断_粘贴失败,部分拷贝错误如文件已存在！";
                    }
                }else
                {
                    return "粘贴失败！";
                }
            }else
            {
                 return "未复制,制复一次只能粘贴一次！";
            }
           
        }

         public Boolean CopyDirectory(string sourceDirPath, string saveDirPath)
        {
            try
            {
                bool f1=true;
                sourceDirPath=   sourceDirPath.Replace('/',Path.DirectorySeparatorChar);
                saveDirPath=   saveDirPath.Replace('/',Path.DirectorySeparatorChar);
                if (!Directory.Exists(saveDirPath))
                {
                    Directory.CreateDirectory(saveDirPath);//如果不存在，新建目录
                } 
                string[] files = Directory.GetFiles(sourceDirPath);//得到当前目录下的文件
                foreach (string file in files)
                {
                    string pFilePath = saveDirPath +Path.DirectorySeparatorChar + Path.GetFileName(file);
                   
                    System.IO.File.Copy(file, pFilePath,false);//非覆盖方式拷贝
                    
                }//把目录下的把有的文件拷贝到指定目录
                string[] dirs = Directory.GetDirectories(sourceDirPath);
                foreach (string dir in dirs)
                {

                   f1= CopyDirectory(dir, saveDirPath + Path.DirectorySeparatorChar + Path.GetFileName(dir));
                }
                if(f1==true)
                {
                    return true;
                }else
                {
                    return false;
                }
               
            }
            catch  
            {
                 return false;
            }
        }   
 
  [HttpPost]
          public string createdir([FromBody] dynamic data )
        {    string xpath=HttpContext.Session.GetString("FileOptCPath");
             try{
                
                    var pathf = xpath + data.name;                       
                    System.IO.Directory.CreateDirectory(pathf);
                    return "新建文件夹成功！";
            }catch
            {
                 return "新建文件夹失败！";
            }
           
        }

        [HttpPost]
          public string dirfilesrename([FromBody] dynamic data )
        {   
             string opath=HttpContext.Session.GetString("FileOptCPath")+data.oldname;
             string npath=HttpContext.Session.GetString("FileOptCPath")+data.newname;
             try{                  
                    if (data.type == "文件夹")
                    {    
                        System.IO.Directory.Move(opath,npath);
                       
                    }else
                    {     
                          System.IO.File.Move(opath,npath);               
                    } 
                  return "修改名字成功！";
            }catch 
            {
              return "修改名字失败！";
            }
           
        }

        [HttpGet]
        public string GetCpathShengYuSpace()
        {
            try
            {
                string totalsize = HttpContext.Session.GetString("FileOptCPathTotalSize"); //当前操作路径总空间
                string uselsize = HttpContext.Session.GetString("FileOptCPathUseSize"); //当前操作路径总使用空间   
                long sysize = long.Parse(totalsize)*1024*1024 - long.Parse(uselsize);
                return sysize.ToString();
            }catch
            {
                return "0";
            }
        }

        /// <summary>
        /// AJAX请求上传，通过Request.Form.Files获取上传文件信息
        /// </summary>
        /// <returns></returns>
        [HttpPost] 
        [DisableFormValueModelBinding]          
        [DisableRequestSizeLimit,RequestFormLimits(ValueCountLimit = 50000,MultipartBodyLengthLimit =107374182400 )]// 大小不限，数量最大5万,多body大小10G 4294967296  解决Form value count limit 1024 exceeded. Multipart body length limit 134217728 exceeded. 
        public string  UpLoadFiles()
        {
            List<string> cfiles = new List<string>();
             try
            {
                string xpath=HttpContext.Session.GetString("FileOptCPath");
                string userid6 = HttpContext.Session.GetString("xsdUserID");
                string zxpwd6 = HttpContext.Session.GetString("xsd_zxPwd");
                bool MangerModel = Models.common.testzxpwd(zxpwd6, userid6);//得到管理员模式
                string UMode = HttpContext.Session.GetString("FileUploadMode");//得到文件上传默认方式,为不覆盖
                var files = Request.Form.Files;

                    string rootpath = xpath;

                    foreach (var file in files)
                    {

                        string[] arrpath = file.FileName.Split(@"/");
                        string dirpath = "";//该文件的所在目录（包括一、二级目录）
                        string filename = arrpath[arrpath.Length - 1].ToString();//该文件名
                        for (int i = 0; i < arrpath.Length; i++)
                        {
                            if (i == arrpath.Length - 1)
                            {
                                break;
                            }
                            dirpath += arrpath[i] + @"/";
                        }

                        DicCreate(Path.Combine(rootpath, dirpath));//不存在则创建该目录                    
                        string filepath = Path.Combine(rootpath, file.FileName);
                    
                        if (System.IO.File.Exists(filepath)  )//重名文件处理
                        {
                            if (!MangerModel)//如果有相同文件并且非管理员模式，则原文件加时间戳
                            {
                                string extension = System.IO.Path.GetExtension(filepath);//扩展名 “.aspx”
                                string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(filepath);// 没有扩展名的文件名 “Default”
                                string efilename = dirpath + fileNameWithoutExtension + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                                string objfilepath = rootpath + efilename;
                                System.IO.File.Move(filepath, objfilepath);
                                cfiles.Add(efilename);
                            }else
                            {
                               if(UMode=="OverOFF")//不覆盖，保存旧的
                                {
                                    string extension = System.IO.Path.GetExtension(filepath);//扩展名 “.aspx”
                                    string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(filepath);// 没有扩展名的文件名 “Default”
                                    string efilename = dirpath + fileNameWithoutExtension + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                                    string objfilepath = rootpath + efilename;
                                    System.IO.File.Move(filepath, objfilepath);
                                    cfiles.Add(efilename);
                                }                                  
                            }                        
                        } 
                        using (var addFile = new FileStream(filepath, FileMode.OpenOrCreate))
                        {
                            if (file != null)
                            {
                                file.CopyTo(addFile);
                            }
                            addFile.Close();
                        }

                    }
                    string uid = HttpContext.Session.GetString("xsdUserID");

                    TaskFactory taskfactory1 = new TaskFactory();
                    Task<string> t1 = taskfactory1.StartNew(() => computerdiskspace(uid));
                    string html = "<div style='height:250px;overflow-y:scroll;'>";
                    for (int i = 0; i < cfiles.Count; i++)
                    {
                        html = html + "<br>" + cfiles[i].Trim();
                    }
                    html = html + "</div>";
                    if (cfiles.Count > 0)
                    {
                        return "上传成功！重名文件已更名，列表如下：<br>" + html;
                    }
                    else
                    {
                        return "上传成功！";
                    }               
                 
            }
            catch (Exception ex)
            {
                 return "上传失败！"+ex.Message.ToString();
            }
           
        }
 private void DicCreate(string path)
        {
            if (!Directory.Exists(path)) //不存在创建
            {
                Directory.CreateDirectory(path);
            }
            //else　//存在改名，加时间戳
            //{               
            //   Directory.Move(path,path + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"));                
            //}
        }

        /// <summary>
        /// 错误
        /// </summary>
        /// <returns></returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


            /// <summary>
       /// 操作list
       /// </summary>
       /// <returns></returns>
        public  IActionResult displist([FromBody] dynamic data)
        {    
            var workcpath= HttpContext.Session.GetString("FileOptCPath"); 
            var workrpath= HttpContext.Session.GetString("FileOptRPath");
            string workpath=workcpath;//data.cdir.ToString().Trim();
            string opt=data.opt.ToString().Trim();
            switch(opt)
             {
                 case  "current":  
                 workpath=workcpath;//data.cdir.ToString().Trim();
                 ViewData["workpath"] =workpath.Replace(workrpath.ToString().Trim(),"Disk:/");              
                 break;
                 case  "uplevel":                   
                   workpath=workcpath.ToString();
                   workpath= workpath.Replace(workrpath.ToString().Trim(),"");
                   string[] upstr=workpath.ToString().Split("/");
                   string upath="";
                   for(int i=0;i<upstr.Length-2;i++)
                   {
                       upath=upath+upstr[i]+"/";
                   }                 
                   workpath= workrpath.ToString().Trim()+upath;
                   ViewData["workpath"] ="Disk:/"+upath;                   
                   HttpContext.Session.SetString("FileOptCPath",workpath);
                 break;
                 case  "downlevel":
                    workpath=workcpath.ToString()+data.cdir+"/"; 
                    ViewData["workpath"] =workpath.Replace(workrpath.ToString().Trim(),"Disk:/");     
                    //ViewData["workpath"] =workpath; 
                    HttpContext.Session.SetString("FileOptCPath",workpath);
                 break;
             } 
            // var  physicalProvider = _hostingEnvironment.WebRootFileProvider;   
             var provider = new PhysicalFileProvider(workpath.Trim ());        
             var contents = provider.GetDirectoryContents(string.Empty);
             JArray jd=JArray.Parse(JsonConvert.SerializeObject(contents));            
             return this.View(jd); 
        } 
         public  IActionResult TrashDblist([FromBody] dynamic data)
        {    
            var workcpath= HttpContext.Session.GetString("tFileOptCPath"); 
            var workrpath= HttpContext.Session.GetString("tFileOptRPath");
            string workpath=workcpath;//data.cdir.ToString().Trim();
            string opt=data.opt.ToString().Trim();
            switch(opt)
             {
                 case  "current":  
                 workpath=workcpath;//data.cdir.ToString().Trim();
                // ViewData["workpath"] =workcpath;   
                  ViewData["workpath"] =workpath.Replace(workrpath.ToString().Trim(),"Trash:/");              
                 break;
                 case  "uplevel":                   
                   workpath=workcpath.ToString();
                   workpath= workpath.Replace(workrpath.ToString().Trim(),"");
                   string[] upstr=workpath.ToString().Split("/");
                   string upath="";
                   for(int i=0;i<upstr.Length-2;i++)
                   {
                       upath=upath+upstr[i]+"/";
                   }
                   workpath= workrpath.ToString().Trim()+upath;
                  // ViewData["workpath"] =workpath;
                    ViewData["workpath"] ="Trash:/"+upath;
                   HttpContext.Session.SetString("tFileOptCPath",workpath);
                 break;
                 case  "downlevel":
                    workpath=workcpath.ToString()+data.cdir+"/"; 
                     ViewData["workpath"] =workpath.Replace(workrpath.ToString().Trim(),"Trash:/");  
                    // ViewData["workpath"] =workpath;
                    HttpContext.Session.SetString("tFileOptCPath",workpath);
                 break;
             } 
            // var  physicalProvider = _hostingEnvironment.WebRootFileProvider;   
             var provider = new PhysicalFileProvider(workpath.Trim ());
             var contents = provider.GetDirectoryContents(string.Empty);
             //ViewData["dir1"] =JsonConvert.SerializeObject(contents);   
             List<Object> dirlist=new List<object> ();
             foreach(var dire in contents)
             {
                 if (!System.IO.File.Exists(dire.PhysicalPath+Path.DirectorySeparatorChar+dire.Name+".txt") )
                 {
                     dirlist.Add(dire);
                 }
             }
             JArray jd=JArray.Parse(JsonConvert.SerializeObject(dirlist));
            
             return this.View(jd); 
        } 
            /// <summary>
       /// 操作icon
       /// </summary>
       /// <returns></returns>
        public  IActionResult dispicon([FromBody] dynamic data)
        {   
            var workcpath= HttpContext.Session.GetString("FileOptCPath"); 
            var workrpath= HttpContext.Session.GetString("FileOptRPath");
            string workpath=workcpath;//data.cdir.ToString().Trim();
            string opt=data.opt.ToString().Trim();
            switch(opt)
             {
                 case  "current":  
                 workpath=workcpath;//data.cdir.ToString().Trim();
                 ViewData["workpath"] =workpath.Replace(workrpath.ToString().Trim(),"Disk:/"); //workpath;               
                 break;
                 case  "uplevel":                   
                   workpath=workcpath.ToString();
                   workpath= workpath.Replace(workrpath.ToString().Trim(),"");
                   string[] upstr=workpath.ToString().Split("/");
                   string upath="";
                   for(int i=0;i<upstr.Length-2;i++)
                   {
                       upath=upath+upstr[i]+"/";
                   }
                   workpath= workrpath.ToString().Trim()+upath;
                   ViewData["workpath"] ="Disk:/"+upath;//workpath;
                   HttpContext.Session.SetString("FileOptCPath",workpath);
                 break;
                 case  "downlevel":
                    workpath=workcpath.ToString()+data.cdir+"/"; 
                   ViewData["workpath"] =workpath.Replace(workrpath.ToString().Trim(),"Disk:/");//workpath;
                    HttpContext.Session.SetString("FileOptCPath",workpath);
                 break;
             } 
            // var  physicalProvider = _hostingEnvironment.WebRootFileProvider;   
             var provider = new PhysicalFileProvider(workpath.Trim ());        
             var contents = provider.GetDirectoryContents(string.Empty);
             JArray jd=JArray.Parse(JsonConvert.SerializeObject(contents));
            
             return this.View(jd); 
        } //

         /// <summary>
       /// 加载框布局页面
       /// </summary>
       /// <returns></returns>
       [TypeFilter(typeof(CheckUserLoginFilter))]
        [CheckUserPermissionFilter("回收站")]
        public  IActionResult TrashFiles()
        {   

            //定义页面导航
            //   --/home--
            Models.PEntity.BreadCrumbNode[] bcnodes = new BreadCrumbNode[3];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");    
            bcnodes[1] = new BreadCrumbNode("文件资源管理器", "#");   
            bcnodes[2] = new BreadCrumbNode("回收站", "/FileManger/FilesManger/TrashFiles");           
            ViewData["BreadCrumbNode_Data"] = bcnodes;
            
             //*********************************************
                //以下处理页面数据------------------------
            
                   string uid=HttpContext.Session.GetString("xsdUserID");                 
                   string sqltui= "Select f0004 From urp_userinfo where f0001=@uid";   
                    SQLiteParameter[] ps= new  SQLiteParameter[]
                     {
                       new SQLiteParameter("@uid",int.Parse(uid))
                       }; 
                  string pathps = Models.DataBase_SQLite.GetOneValue(sqltui,ps);
                  string[] paths =pathps.Split(",");
                psdata ps1 = new psdata();
                //检查路径文件夹是否存在
                if (System.IO.Directory.Exists(paths[0].Trim()))
                {
                    string optpath=paths[0].Trim();
                    ViewData["xpathroot"]="Trash:/";
                    HttpContext.Session.SetString("tFileOptCPath",optpath);//当前操作路径
                    HttpContext.Session.SetString("tFileOptRPath",optpath);//工作根目录
                    ComputerDiskinfo p1 = new ComputerDiskinfo();
                    p1.xpath = paths[0].Trim(); //路径                  
                    double t1 =0;// getdirlength(p1.xpath);
                    p1.usesum = t1;
                    p1.totalsum = double.Parse(paths[1]);//配额

                    // var physicalProvider = _hostingEnvironment.WebRootFileProvider;   
                    var provider = new PhysicalFileProvider(p1.xpath); //得到当前目录的内容
                    var contents = provider.GetDirectoryContents(string.Empty);
                     List<Object> dirlist=new List<object> ();
                    foreach(var dire in contents)
                    {
                        if (!System.IO.File.Exists(dire.PhysicalPath+Path.DirectorySeparatorChar+dire.Name+".txt") )
                        {
                            dirlist.Add(dire);
                        }
                    }
                    JArray jd = JArray.Parse(JsonConvert.SerializeObject(dirlist));
                   
                    ps1.f1 = p1;//当前目录信息
                    ps1.f2 = jd;//当前目录内容
                }else
                {
                    ps1.f1 =null;//当前目录信息
                    ps1.f2 =null;//当前目录内容
                }
                
                  return this.View(ps1);            
        } //
         [HttpGet]
          public string TrashFilesSpaceUseCount()
          {  
                   string uid=HttpContext.Session.GetString("xsdUserID");   
                   string msg="0 byte";              
                   string sqltui= "Select f0004 From urp_userinfo where f0001=@uid";   
                    SQLiteParameter[] ps= new  SQLiteParameter[]
                     {
                       new SQLiteParameter("@uid",int.Parse(uid))
                       }; 
                  string pathps = Models.DataBase_SQLite.GetOneValue(sqltui,ps);
                  string[] paths =pathps.Split(",");
                psdata ps1 = new psdata();
                //检查路径文件夹是否存在
                if (System.IO.Directory.Exists(paths[0].Trim()))
                {                                     
                    double t1 =getdirlength(paths[0].Trim());
                    double totalsum = double.Parse(paths[1]);//配额
                    string flength =t1.ToString() + " byte:0%";
                    int fint = t1.ToString().Length;
                    if (fint <= 3)
                    { //byte

                    }
                    else
                    {
                        if (fint <= 6)
                        {  //kb
                            flength = (Math.Ceiling(t1 / 1024)).ToString() + " Kb";
                        }
                        else
                        {  //Mb
                            flength = (Math.Ceiling(t1 / 1024 / 1024 * 100) / 100).ToString() + " Mb";
                        }
                    }
                    int zb = (int)(t1 * 100 / (totalsum * 1024 * 1024));//字节运算
                    msg=flength+":"+zb.ToString()+"%";
                }
             return msg;
          }
      
        /// <summary>
       /// 下载显示
       /// </summary>
       /// <returns></returns>
          public  IActionResult TrashFiles_download([FromBody] dynamic datas)
        {
            //传参过来是文件名或文件夹名的选择集  datas由 data = { name:n,type:t}组成
            List<string> ls = new List<string>();
            string xpath=HttpContext.Session.GetString("tFileOptCPath");//得到当前目录
            string rpath= HttpContext.Session.GetString("tFileOptRPath");//得到根目录       
            @ViewData["cpath"]= xpath.Replace(rpath.ToString().Trim(),"Trash:/");//显示当前目录(去根显示)
            foreach (var data in datas)
            {
                if (data.type == "文件夹")
                {
                    //如果是文件夹列出所有文件然后下载
                    string workpath = xpath + data.name;
                    var provider = new PhysicalFileProvider(workpath);
                    var DContents = provider.GetDirectoryContents(string.Empty);
                    ls.AddRange(tlistdirfile(DContents).ToArray());
                }
                else
                {  //如果是文件直接下载               
                    var path = xpath + data.name; //实际路径
                    ls.Add(path.Replace(xpath.ToString().Trim(),""));//显示路径,去除当前路径

                }
            }
            return this.View(ls);

        }  
         public List<string>  tlistdirfile( IDirectoryContents dirs)
        {            
            string xpath= HttpContext.Session.GetString("tFileOptCPath"); //当前路径
            List<string> ds = new List<string>();
            foreach(var item in dirs)
            {
                if (item.IsDirectory)
                {  //如果是目录，继续遍历文件
                    var provider = new PhysicalFileProvider(item.PhysicalPath);
                    var DContents = provider.GetDirectoryContents(string.Empty);                    
                    ds.AddRange(tlistdirfile(DContents).ToArray());
                }
                else
                {   //如果是文件，添架显示，去当前路径
                    ds.Add(item.PhysicalPath.ToString().Replace(Path.DirectorySeparatorChar,'/').Replace(xpath.ToString().Trim(),""));//去当前路径
                }

            }
           
            return ds;
        }
         [TypeFilter(typeof(CheckUserLoginFilter))]
         public IActionResult trashdownloadfile(string filename)
        {  
             //下载页面里点击下载单个文件
            string xpath= HttpContext.Session.GetString("tFileOptCPath");
            var addrUrl =xpath.Trim()+filename;
            var stream = System.IO.File.OpenRead(addrUrl);
            string fileExt = Path.GetExtension(filename);               
            return File(stream, "application/octet-stream", Path.GetFileName(addrUrl));
             
        }     
         
          public List<string>  tlistdir_and_file( IDirectoryContents dirs)
        {

            //string workrpath= HttpContext.Session.GetString("FileOptRPath"); //根路径
            string xpath= HttpContext.Session.GetString("tFileOptCPath"); //当前路径
            List<string> ds = new List<string>();
            foreach(var item in dirs)
            {
                if (item.IsDirectory)
                {  
                     //去当前路径,添加目录
                    ds.Add(item.PhysicalPath.ToString().Replace(Path.DirectorySeparatorChar,'/').Replace(xpath.ToString().Trim(),"")+"/");
                    //如果是目录，继续遍历文件
                    var provider = new PhysicalFileProvider(item.PhysicalPath);
                    var DContents = provider.GetDirectoryContents(string.Empty); 
                    ds.AddRange(tlistdir_and_file(DContents).ToArray());
                   
                    
                }
                else
                {   //如果是文件，去当前路径
                    ds.Add(item.PhysicalPath.ToString().Replace(Path.DirectorySeparatorChar,'/').Replace(xpath.ToString().Trim(),""));
                }

            }
           
            return ds;
        }
          public IActionResult trashdownloadzipfile([FromBody] dynamic datas)
        {       
            //传参过来是文件名或文件夹名的选择集  datas由 data = { name:n,type:t}组成        
            List<string> ls=new List<string>(); //压缩文件    全路径及名
            List<string> ls1=new List<string>();//压缩文件名，无当前路径
            string xpath=HttpContext.Session.GetString("tFileOptCPath"); //默认是“/"分割             
            foreach (var data in datas)
            {
                
               
                 //如果是目录递归列出 子目录与文件
                 if (data.type == "文件夹")
                 {
                    string sdname=data.name;//得到当前遍历目录名称
                    string workpath = xpath +sdname;  //得到当前遍历目录路径 
                    //添加目录
                    ls.Add(xpath.Trim()+sdname+"/");
                    ls1.Add(sdname+"/");
                    //递归遍历目录         
                    var provider = new PhysicalFileProvider(workpath);
                    var DContents = provider.GetDirectoryContents(string.Empty);
                    List<string> cls=tlistdir_and_file(DContents);//得到目录下所以文件及文件夹
                    foreach( string lj in cls)
                    {       
                            ls.Add(xpath.Trim()+lj.Trim());//
                            ls1.Add(lj.Trim());
                    }
                    
                     
                }else
                {
                      //如果是文件或文件夹直接下载              
                        string sname=data.name;
                        ls.Add(xpath.Trim()+sname);
                        ls1.Add(sname);
                }
                 
            }       
       
        byte[] compressedBytes;
        using (var outStream = new MemoryStream())
        {
            using (var archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
            {  
                              
                for(int i=0;i<ls.Count;i++)
                {
                   var fileInArchive = archive.CreateEntry(ls1[i].ToString(), CompressionLevel.Optimal);
                   if(System.IO.File.Exists(ls[i].ToString()))//如果是文件路径添加数据到压缩包，不是说明是目录实体
                   {
                        byte[] fileBytes = System.IO.File.ReadAllBytes(ls[i].ToString());
                        using (var entryStream = fileInArchive.Open())
                        using (var fileToCompressStream = new MemoryStream(fileBytes))
                        {
                            fileToCompressStream.CopyTo(entryStream);
                        }
                   }
                }
            }
            compressedBytes = outStream.ToArray();
        }          
        return     File(compressedBytes, "application/octet-stream", "ptemp.zip");
            
        } 
 [HttpPost]
          public string DeleteTrashDirAllFile([FromBody] dynamic datas )
        {   
             string userid6 = HttpContext.Session.GetString("xsdUserID");
        string zxpwd6=HttpContext.Session.GetString("xsd_zxPwd");                       
        if(Models.common.testzxpwd(zxpwd6,userid6))
        {
             string xpath=HttpContext.Session.GetString("tFileOptCPath");
             try{

                if(_websiteset.Trashswitch=="on")
                { 
                    //自动删除
                     foreach (var data in datas)
                    {
                        var pathf = xpath + data.name;
                        if (data.type == "文件夹")
                        {    
                        
                                System.IO.Directory.Delete(pathf,true);        
                                    
                    
                        }else
                        {     
                            
                             System.IO.File.Delete(pathf );
                        }                   
                    }
                     return "清除成功！";

                }else 
                {
                    //做手动删除标志
                    //在目录里做删除标志
                     int f1=0;
                    foreach (var data in datas)
                    {
                         if (data.type == "文件夹")
                        { 
                            f1=0;
                        }else
                        {
                            f1=1;
                        }
                    }
                    if (f1==1)
                    { 
                        return "只能清除整个文件夹内容！";

                    }else
                    {
                             foreach (var data in datas)
                            {
                                var pathf = xpath + data.name;                                
                                pathf=pathf+Path.DirectorySeparatorChar+data.name+".txt";
                                FileStream myFs = new FileStream(pathf,System.IO.FileMode.Create);
                                StreamWriter mySw = new StreamWriter(myFs);
                                mySw.Write("delete");
                                mySw.Close();
                                myFs.Close();  
                                               
                            }
                             return "清除成功！";
                    }
                   
                   
                    
                   
                }
                
            }catch
            {
              return "清除回收站内容失败！";
            }
        }else
        {
            return "未开启管理模式！";
        }
           
        }//
        [HttpPost]
        public string Setfastpath([FromBody] dynamic data )
        {  
            
            string dirtype=data.dirtype;
            string dirname=data.dirname;
            string msg=UpdateFastPath(dirtype,dirname);
            return msg;
        }
    public string UpdateFastPath(string dirtype,string dirname)
    {   
        string uid=HttpContext.Session.GetString("xsdUserID");
        string msg="操作失败！";
            try{
               
              string f5="";
              string sqltui= "Select f0005 From urp_userinfo where f0001=@uid";   
              SQLiteParameter[] ps= new  SQLiteParameter[]
               {
                       new SQLiteParameter("@uid",int.Parse(uid))
               };                
                string pathps = Models.DataBase_SQLite.GetOneValue(sqltui,ps);
                string[] paths = pathps.Split(";");
                string cpath=HttpContext.Session.GetString("FileOptCPath")+dirname+"/" ;
                switch(dirtype)
                {
                    case "doc":
                     f5=f5+cpath+",true;"+paths[1]+";"+paths[2];
                    break;
                    case "img":
                     f5=f5+paths[0]+";"+cpath+",true;"+paths[2];
                    break;
                    case  "video":
                     f5=f5+paths[0]+";"+paths[1]+";"+cpath+",true";
                    break;
                }
                string sqld = "update urp_userinfo set f0005=@f5  WHERE f0001=@f1";
                Dictionary <string,dynamic> psd1=new Dictionary<string,dynamic>();
                psd1.Add("@f5", f5);                           
                psd1.Add("@f1",int.Parse(uid));                            
                int fd1 = Models.DataBase_SQLite.InsertDelEdit(sqld, psd1);
                if (fd1 == 1)
                {
                    msg = "设置成功！";
                }
                else
                {
                    msg = "设置失败！";

                }   
               
            }catch(Exception ex)
            {
               msg=msg+"路径配置不正确"+ex.Message;
            }
             return msg;

        }//
        

    }
}
