using System;
using System.IO;
using System.Drawing;
namespace NetCoreFilesManger.Areas.FileManger.Models
{
    public class thumbnail
    {
        //生成预览图Base64,以高为标准进行等比例
        public static string MakeImgPreBase64_JZ(string pathfilename,int heightc)
        {
             if (heightc>600) heightc=600;//规定缩放最大高度
             string sBasecode="";
             Image image = Image.FromFile(pathfilename);
             double width = image.Width;
             double height = image.Height;  
             if(width>heightc || height>heightc)   
             {       
                double sz = height /heightc;//以高为标准进行等比例
                width =width / sz;
                height =height / sz;    
                if(width>800)  //规定缩放最大宽度
                {
                double sz1 = width /800;//限制宽的为标准进行等比例
                width =width / sz1;
                height =height / sz1;  
                }     
               // Image img1=image.GetThumbnailImage(Convert.ToInt16(width),Convert.ToInt16(height), new Image.GetThumbnailImageAbort(delegate { return false; }), IntPtr.Zero);
                Bitmap destBitmap =new Bitmap(Convert.ToInt16(width),Convert.ToInt16(height));  
                Graphics g = Graphics.FromImage(destBitmap);  
                g.Clear(Color.Transparent);  
                //设置画布的描绘质量           
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;  
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;  
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;  
                g.DrawImage(image, new Rectangle(0,0,Convert.ToInt16(width),Convert.ToInt16(height)), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);  
                g.Dispose();  
              
                byte[] byteArray = ImageToBytes(destBitmap, System.Drawing.Imaging.ImageFormat.Jpeg);
                sBasecode = "data:image/jpeg;base64," + Convert.ToBase64String(byteArray);
                image.Dispose();
                destBitmap.Dispose();
                return sBasecode;
             }else
             {  
                 byte[] byteArray1 = ImageToBytes(image, System.Drawing.Imaging.ImageFormat.Jpeg);
                  sBasecode = "data:image/jpeg;base64," + Convert.ToBase64String(byteArray1);
                return sBasecode;
             }
            
        }//
        //生成预览图Base64,200内大小
        public static string MakeImgPreBase64(string pathfilename)
        {
             string sBasecode="";
             Image image = Image.FromFile(pathfilename);
             int width = image.Width;
             int height = image.Height;
             int sz = height /150;//以高为标准进行等比例
             width = width / sz;
             height = height / sz;        
             Image img1=image.GetThumbnailImage(width,height, new Image.GetThumbnailImageAbort(delegate { return false; }), IntPtr.Zero);
             byte[] byteArray = ImageToBytes(img1, System.Drawing.Imaging.ImageFormat.Jpeg);
             sBasecode = "data:image/jpeg;base64," + Convert.ToBase64String(byteArray);
            image.Dispose();
            img1.Dispose();
            return sBasecode;
        }//
        public static byte[] ImageToBytes(Image Image, System.Drawing.Imaging.ImageFormat imageFormat)
        {

            if (Image == null) { return null; }

            byte[] data =null;

    using (MemoryStream ms = new MemoryStream())
            {

                using (Bitmap Bitmap = new Bitmap(Image))
                {

                    Bitmap.Save(ms, imageFormat);

                    ms.Position = 0;

                    data =new byte[ms.Length];

            ms.Read(data, 0, Convert.ToInt32(ms.Length));

                    ms.Flush();

                }

            }

            return data;

        }
        //---
    }
}