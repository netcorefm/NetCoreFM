﻿using System;
using System.ComponentModel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace NetCoreFilesManger
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;//这里要改为false，默认是true，true的时候session无效
                options.MinimumSameSitePolicy = SameSiteMode.None;
               
                
            });
            //注入配置session服务
            services.AddDistributedMemoryCache();
            services.AddSession(Options =>
            {
                Options.IdleTimeout = TimeSpan.FromSeconds(3600);//60分钟
                Options.Cookie.HttpOnly = true;
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            NetCoreFilesManger.Models.common.strcon =Configuration.GetConnectionString("sqlitedb3");
            //将自定义数据注入到指定类，由指定类在控制器注入使用
            services.Configure<Models.PEntity.WebSets>(Configuration.GetSection("WebSets"));
           
            
            //NetCoreFilesManger.Models.common.strcon =Configuration.GetConnectionString("pgsql9514")+"Database=xxx;Username=xxx;Password=xxx";           
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
               // app.UseHsts();
            }

           // app.UseHttpsRedirection();
            app.UseStaticFiles();   //wwwroot                 
            app.UseCookiePolicy();
            app.UseSession();//使用session中间件
            app.UseMvc(routes =>
            {
                 routes.MapRoute(
                    name: "area",
                    template: "{area:exists}/{controller=FilesManger}/{action=MyComputerDisk}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
    
}
