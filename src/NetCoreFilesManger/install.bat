@echo off
echo 安装说明
echo .
echo 注意事项1：请以管理员身份运行,路径不能为中文,否则初始化会失败
echo .  
echo 注意事项2：host.json文件  用来设置服务端口
echo . 
echo 注意事项3：appsettings.production.json文件  做网站信息与功能配置
echo .
echo .  
pause
set FMpath=%~dp0
color 2f
echo ******************************************
:begin
cls
color 2f
echo 1  Dos命令行  

echo 2  安装     

echo 3  卸载       

echo 4  启动        

echo 5  停止        
 
echo 0  退出      
echo ******************************************
Set /P Choice= 请输入要选择的操作数字 ，然后按回车运行：
If not "%Choice%"=="" ( 
  If "%Choice%"=="1" goto :m1
  If "%Choice%"=="2" goto :m2
  If "%Choice%"=="3" goto :m3
  If "%Choice%"=="4" goto :m4
  If "%Choice%"=="5" goto :m5 
  If "%Choice%"=="0" exit
)

pause>nul
goto :begin

:m1
echo 进入Dos命令行 
color 0f
cls
cmd
goto :begin

:m2
echo  现在执行  注册服务  命令
sc create "NetCoreFilesManger_V1.0" binPath= %FMpath%NetCoreFilesManger.exe
sc description "NetCoreFilesManger_V1.0" "家庭云（Nas文件管理服务器）"
sc config "NetCoreFilesManger_V1.0" start= AUTO
pause
goto :begin

:m3
echo 现在执行  卸载服务  命令
sc delete "NetCoreFilesManger_V1.0" binPath= %FMpath%NetCoreFilesManger.exe
pause
goto :begin

:m4
echo 现在执行  启动  命令 
net start "NetCoreFilesManger_V1.0"
pause
goto :begin

:m5
echo 现在执行  停止  命令 
net stop "NetCoreFilesManger_V1.0"
pause
goto :begin
 


