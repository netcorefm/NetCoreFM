﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SQLite;
using NetCoreFilesManger.Models;
namespace NetCoreFilesManger.Controllers
{
  
    [Route("api/[controller]")]
    [ApiController]
    public class GetLoginStatusController : ControllerBase
    {
        public static IServiceProvider ServiceProvider;
        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]   
        public async Task Get()
        {
            var response =HttpContext.Response;
             response.Headers.Add("Content-Type", "text/event-stream");
            var uname=HttpContext.Session.GetString("xsdUserName");
            string Udate = HttpContext.Session.GetString("xsd_logindate");// 登录时间   
            string UIP = HttpContext.Session.GetString("xsd_loginip");// 登录IP    
            string loginf = DateTime.Now.ToLongTimeString();//在线池是否存在标志            
            while(true)
            {
                 var obj1 = Models.common.UserStatus.Where(ojb => ojb.username == uname).FirstOrDefault();
                    if (obj1 != null)
                    {

                        if (Udate.Trim() == obj1.logindate.Trim() && UIP.Trim() == obj1.loginip.Trim())
                        {
                            loginf = "loginok";// 相等登录成功
                        }
                        else
                        {
                           loginf = "loginno1";//用户名在，其它不同说明异地登录了
                        }
                    }
                    else
                    {
                        loginf = "loginno";//用户名在，其它不同说明异地登录了

                    } 
                await response.WriteAsync($"data:{loginf},{uname}\n\n");
                response.Body.Flush();
                await Task.Delay(5 * 1000);
                
            }
        }
    }
    //----------------------
    [Route("api/[controller]")]
    [ApiController]
    public class CheckUserController : ControllerBase
    {
      
        [HttpPost]
        public string Post([FromBody] dynamic data)
        {
            string msg="flase";
            string uname = data.uname;
            string upwd = data.upwd;
            if(checklogin(upwd,uname))
            {
              msg="true";
            }
            return msg;
            
        }
         public   bool checklogin(string upwd,string username)
        {
            try
            {
                string upwdm=Models.common.EncryptByMD5(upwd);                
                string sqluser = "Select * From  urp_user where f0002=@un and f0003=@pwd "; 
                Dictionary <string,dynamic> ps=new Dictionary<string,dynamic>();
                ps.Add("@un",username);
                ps.Add("@pwd",upwdm);                                      
                DataTable dt_user= Models.DataBase_SQLite.GetDataTable(sqluser,ps);
                if ( dt_user.Rows.Count> 0)
                {

                   return true;
                }else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        //
    }
    //-----------------------------
    [Route("api/[controller]")]
    [ApiController]
    public class GetDiskController : ControllerBase
    {
      
        [HttpPost]
        public string Post([FromBody] dynamic data)
        {
            string uname=data.uname;
            string upwd=data.upwd;
            upwd=common.EncryptByMD5(upwd);
            //对象转json字符串          
            string sqluser = "Select urp_userinfo.f0003 From  urp_user left join urp_userinfo on  urp_user.f0001=urp_userinfo.f0001 where urp_user.f0002=@uname and urp_user.f0003=@upwd "; 
            Dictionary <string,dynamic> ps=new Dictionary<string,dynamic>();
            ps.Add("@uname",uname);
            ps.Add("@upwd",upwd);                   
            string diskpath= Models.DataBase_SQLite.GetOneValue(sqluser,ps);  
            return diskpath;
        }
    }
    //-------------------------------
}