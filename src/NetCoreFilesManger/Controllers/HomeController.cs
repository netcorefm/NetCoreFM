﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using NetCoreFilesManger.Models;
using NetCoreFilesManger.Models.PEntity;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Options;
using System.Runtime.InteropServices;
using System.Net;
using System.Net.Sockets;

namespace NetCoreFilesManger.Controllers
{
    public class HomeController : Controller
    {      
        private readonly  WebSets _websiteset;  
      
        public HomeController(IOptionsSnapshot<WebSets> websets )
        {
           _websiteset=websets.Value;  
        } 
        public IActionResult About()
        {    //定义页面导航             
            BreadCrumbNode[] bcnodes = new BreadCrumbNode[2];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");                     
            bcnodes[1] = new BreadCrumbNode("关于软件", "/Home/About");
            ViewData["BreadCrumbNode_Data"] = bcnodes;
            ViewData["Version"] =_websiteset.Version;//应用版本
            ViewData["SystemFame"] =RuntimeInformation.OSArchitecture.ToString();//系统架构
            ViewData["SystemName"] =RuntimeInformation.OSDescription.ToString();//系统架构
            string ip = HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault();
            string sipv4 = GetLocalIPv4();
            string sipv6 = GetLocalIPv6();
            string siplinklocalv6 = GetlinkLocalIPv6();
            if (string.IsNullOrEmpty(ip))
            {  
                ip = HttpContext.Connection.RemoteIpAddress.ToString();//转换为 IPv4 点分四位或 IPv6 冒号十六进制表示法
                
            }
            ViewData["Client"]=ip;
            ViewData["sIpv4"]=sipv4;
            ViewData["sIpv6"]=sipv6;
            ViewData["siplinklocalv6"]=siplinklocalv6;
            ViewData["ClientBrower"]=HttpContext.Request.Headers["User-Agent"].ToString();            
            return this.View();
        }//
        public static string GetLocalIPv6()
        {
            try
            {
                string HostName = Dns.GetHostName(); //得到主机名
                IPHostEntry IpEntry = Dns.GetHostEntry(HostName);
                string s1 = "";
                for (int i = 0; i < IpEntry.AddressList.Length; i++)
                {
                    //从IP地址列表中筛选出IPv4类型的IP地址
                    //AddressFamily.InterNetwork表示此IP为IPv4,
                    //AddressFamily.InterNetworkV6表示此地址为IPv6类型
                    if (IpEntry.AddressList[i].AddressFamily == AddressFamily.InterNetworkV6 && !IpEntry.AddressList[i].IsIPv6LinkLocal )
                    {
                       s1=s1+ "["+IpEntry.AddressList[i].ToString() +"]";
                     }
                }
                return  s1;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
         public static string GetlinkLocalIPv6()
        {
            try
            {
                string HostName = Dns.GetHostName(); //得到主机名
                IPHostEntry IpEntry = Dns.GetHostEntry(HostName);
                string s1 = "";
                for (int i = 0; i < IpEntry.AddressList.Length; i++)
                {
                    //从IP地址列表中筛选出IPv4类型的IP地址
                    //AddressFamily.InterNetwork表示此IP为IPv4,
                    //AddressFamily.InterNetworkV6表示此地址为IPv6类型
                    if (IpEntry.AddressList[i].AddressFamily == AddressFamily.InterNetworkV6 && IpEntry.AddressList[i].IsIPv6LinkLocal )
                    {
                       s1=s1+ "["+IpEntry.AddressList[i].ToString() +"]";
                     }
                }
                return  s1;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
         public static string GetLocalIPv4()
        {
            try
            {
                string HostName = Dns.GetHostName(); //得到主机名
                IPHostEntry IpEntry = Dns.GetHostEntry(HostName);
                string s1 = "";
                for (int i = 0; i < IpEntry.AddressList.Length; i++)
                {
                    //从IP地址列表中筛选出IPv4类型的IP地址
                    //AddressFamily.InterNetwork表示此IP为IPv4,
                    //AddressFamily.InterNetworkV6表示此地址为IPv6类型
                    if (IpEntry.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                    {
                       s1=s1+ "["+IpEntry.AddressList[i].ToString() +"]";
                     }
                }
                return  s1;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        public string preimg()
        {            
            string yzm=Models.ImageOpt.MakeCode(4);
            HttpContext.Session.SetString("xsdyzm",yzm);//记录验证码
            return Models.ImageOpt.TextToBase64Img(yzm);            
        }  
      
        List<NavTreeNodeEntity> Nds = new List<NavTreeNodeEntity>(); //通过treeviewload（）赋值
        /// <summary>
        /// 得到权限权，侧边栏数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]       
        public ActionResult GetTreeData()
        {            
            //得到左侧全部结点  显示结点名称与权根名相同
            string sqltree = "SELECT no, name, classtype, parentno,  img, simg  FROM  urp_treeview order by no";            
            DataTable dt_trees = Models.DataBase_SQLite.GetDataTable(sqltree);
            //得到当前用户的权限 名称集
            int userid=int.Parse (HttpContext.Session.GetString("xsdUserID"));
            string sqlqx = "Select  distinct urp_permission.f0002 as name  From urp_user_role LEFT  JOIN urp_role on urp_user_role.f0003=urp_role.f0001 left join urp_role_permission on urp_role.f0001=urp_role_permission.f0002 left join urp_permission on urp_role_permission.f0003=urp_permission.f0001 where urp_user_role.f0002=@uid";            
            Dictionary <string,dynamic> ps=new Dictionary<string,dynamic>();
            ps.Add("@uid",userid);            
           // SQLiteParameter[] ps= new  SQLiteParameter[]{new SQLiteParameter("@uid",userid)};      
            DataTable dt_qxs= Models.DataBase_SQLite.GetDataTable(sqlqx,ps);
            //生成树对像Nds          
            LoadTreeView(Nds,"0",dt_trees,dt_qxs);
            JsonResult treenodesjson= Json(Nds);//菜单            
            return   treenodesjson;//返回菜单json数据
        }   

/// <summary>
/// 添加指定权限的菜单结点到 List<NavTreeNodeEntity>实例数据的递归算法，List<NavTreeNodeEntity>实例生成的json符合jquery treeview插件要求
/// </summary>
/// <param name="tn">NavTreeNodeEntity实例</param>
/// <param name="no">结点编码，根结点在数据库UrpTreeview记录为"0"</param>
/// <param name="dt">数据库UrpTreeview记录集</param>
/// <param name="qx">当前用户根权名称集,在qx中的结点将加入tn用做显示</param>
        private void LoadTreeView(List<NavTreeNodeEntity> tn, string no, DataTable dt, DataTable qx)
        {
            foreach (DataRow dr in dt.Select("parentno='"+no+"'"))//如果结点没有子结点侧改分枝结束
            {  
                
                NavTreeNodeEntity znode = new NavTreeNodeEntity();
                znode.text = dr["name"].ToString();//结点名称
                znode.icon = dr["img"].ToString();// "fa fa-file-text-o";               
                if (dr["classtype"].ToString().Trim() == "#")
                {  
                    //如果是目录    
                    znode.icon=""; //不能删除，如果是目录，不要目录结点图标               
                    znode.tags = new string[1];
                    znode.tags[0] =dr["classtype"].ToString();//url                           
                    tn.Add(znode);                 
                    DataRow[] utr1 = dt.Select("parentno='"+dr["no"].ToString()+"'");
                    if(utr1.Count()>0)
                    {
                        znode.nodes = new List<NavTreeNodeEntity>();
                        LoadTreeView(znode.nodes, dr["No"].ToString(),dt,qx);
                    }
                    if(znode.nodes.Count==0)
                    {
                        tn.Remove(znode);//如果该目录无子结点清除该目录
                    }
                }else
                {
                    znode.tags = new string[1];
                    znode.tags[0] = dr["classtype"].ToString();//url 
                    if(qx.Select("name='"+znode.text.Trim ()+"'").Count()>0) //如果末节点在权限表则添加展示
                    {
                        tn.Add(znode);          
                    }
                }
                 
            }
        }
        /// <summary>
        /// 退出登录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult LoginOut()
        {  
            HttpContext.Session.Clear();           
            return RedirectToAction("Index");//跳转到登录
        }
         [HttpGet]
        public IActionResult QLogOut()
        {
            HttpContext.Session.Clear();
            ViewData["webtitle"]=_websiteset.WebTitle; 
            ViewData["websubtitle"]=_websiteset.WebSubTitle;
            this.ViewBag.LoginEr = "该用户在其它地方已登录!";
            return this.View("Index");//跳转到登录
        }
         [HttpGet]
        public IActionResult LogOut()
        {
           
            ViewData["webtitle"]=_websiteset.WebTitle; 
            ViewData["websubtitle"]=_websiteset.WebSubTitle;
            this.ViewBag.LoginEr = "一个浏览器只登录一个帐号";
            return this.View("Index");//跳转到登录
        }
        /// <summary>
        /// 登录检查与初始化
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult LoginCheck()
        { 
            //用户检查
            string username = Request.Form["username"];
            string userpwd=Models.common.EncryptByMD5(Request.Form["password"]);
            string yzm = Request.Form["yzm"]; 
            string yzmnet = HttpContext.Session.GetString("xsdyzm");
            yzm=yzm.ToUpper();//不区分大小写 
            yzmnet.ToUpper();
            if (yzmnet!=null && yzm.Trim()==yzmnet.Trim () )
            {
                string sqluser = "Select * From  urp_user where f0002=@uname and f0003=@upwd "; 
                Dictionary <string,dynamic> ps=new Dictionary<string,dynamic>();
                ps.Add("@uname",username);
                ps.Add("@upwd",userpwd);                   
                DataTable dt_user= Models.DataBase_SQLite.GetDataTable(sqluser,ps);
                if ( dt_user.Rows.Count> 0)
                {
                    //判断同一浏览器是否有用户登录,如果有，则不能登录
                    
                        string Uname = HttpContext.Session.GetString("xsdUserName");
                        if (Uname!=null )
                        {
                            HttpContext.Session.Clear(); //如果已登录，重新配置前清除                       
                           
                        }
                        string ip = HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault();
                        if (string.IsNullOrEmpty(ip))
                        {                            
                            ip = HttpContext.Connection.RemoteIpAddress.ToString();//转换为 IPv4 点分四位或 IPv6 冒号十六进制表示法
                        }   
                        string ldate= DateTime.Now.ToString();               
                        var obj1=Models.common.UserStatus.Where(ojb=>ojb.username==username).FirstOrDefault();
                        if(obj1!=null)
                        {
                            obj1.logindate=ldate; //在线池只做更新登录日期与IP
                            obj1.loginip = ip;
                        }else
                        {
                            Models.common.UserStatus.Add(new OnLineUser(username,ldate,ip));
                        }                         
                        //无其它用户可以登录                   
                        HttpContext.Session.SetString("xsdUserID", dt_user.Rows[0]["f0001"].ToString());//记录用户ID
                        HttpContext.Session.SetString("xsdUserName",username);//记录用户名
                        HttpContext.Response.Cookies.Append("ukey",username, new CookieOptions
                        {
                            Expires = DateTime.Now.AddMinutes(30)//30分钟,改 cookie用于同名在同浏览器，退出
                        });                       
                        HttpContext.Session.SetString("xsd_logindate",ldate);// 登录时间   
                        HttpContext.Session.SetString("xsd_loginip",ip);// 登录IP                      
                        HttpContext.Session.SetString("xsd_zxPwd", "*");//初始化执行密码      
                        Models.common.WriterSysLog(ldate+"     :"+username+"  :"+ip+":    "+HttpContext.Request.Headers["User-Agent"].ToString());     
                        return RedirectToAction("Home");//跳转到首页框架
                        
                    
                }
                else
                {
                    ViewData["webtitle"]=_websiteset.WebTitle; 
                    ViewData["websubtitle"]=_websiteset.WebSubTitle;    
                    this.ViewBag.LoginEr = "帐号或密码错误!";
                    return this.View("Index");//跳转到登录
                }    
            }else
            {
                 ViewData["webtitle"]=_websiteset.WebTitle; 
                 ViewData["websubtitle"]=_websiteset.WebSubTitle;    
                this.ViewBag.LoginEr = "验证码错误!";
                return this.View("Index");//跳转到登录
            }    
        }

        /// <summary>
        /// 显示登录界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            //登录界面
            this.ViewBag.LoginEr = "请登录";
            ViewData["webtitle"]=_websiteset.WebTitle; 
            ViewData["websubtitle"]=_websiteset.WebSubTitle;                  
            return View();
        }
       /// <summary>
       /// 加载框布局页面
       /// </summary>
       /// <returns></returns>
        
        [TypeFilter(typeof(CheckUserLoginFilter))]
        public  IActionResult Home()
        {   

            //定义页面导航
            //   --/home--
            Models.PEntity.BreadCrumbNode[] bcnodes = new BreadCrumbNode[1];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");           
            ViewData["BreadCrumbNode_Data"] = bcnodes;           
            ViewData["UserID"] = HttpContext.Session.GetString("xsdUserID");
            string Uname= HttpContext.Session.GetString("xsdUserName");           
            string Udate = HttpContext.Session.GetString("xsd_logindate");// 登录时间                
            
            
            //以下处理页面数据------------------------
            ViewData["UserName"] = Uname;
            ViewData["webtitle"] = _websiteset.WebTitle;
            HttpContext.Session.SetString("PastePath", "");//剪贴板路径
            HttpContext.Session.SetString("PasteDirFiles", "");//剪贴板内容                  
            string username = HttpContext.Session.GetString("xsdUserName");
            string sqlmp = "Select mid as f1,mdate as f2,aname as f3, contents as f4 ,status as f5 From  message_pool where bname=@uname and status=@ustatus ";
            Dictionary<string, dynamic> ps = new Dictionary<string, dynamic>();
            ps.Add("@uname", username);
            ps.Add("@ustatus", "未阅读");
            DataTable dt_user = Models.DataBase_SQLite.GetDataTable(sqlmp, ps);
            JArray jos = JArray.Parse(JsonConvert.SerializeObject(dt_user));
            return this.View(jos);
        }        
        /// <summary>
        /// 错误
        /// </summary>
        /// <returns></returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult LogoutRefush()
        {
            
            HttpContext.Session.Clear();
            return View();
        }
    }
}
