﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using NetCoreFilesManger.Models.PEntity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting; 
using System.IO;
namespace NetCoreFilesManger.Controllers
{
    public class UrpController : Controller
    {
        
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly  WebSets _websiteset;  
        IApplicationLifetime applicationLifetime;
         public UrpController(IHostingEnvironment hostingEnvironment, IApplicationLifetime appLifetime ,IOptionsSnapshot<WebSets> websets)
        {
           
            _hostingEnvironment = hostingEnvironment;//注入IHostingEnvironment获取路径用
            applicationLifetime=appLifetime;
            _websiteset=websets.Value;
        }  
         
        /// <summary>
        /// API 按钮事件调用后台数据 用户编辑和角色编辑
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public string AddEditDelOpt([FromBody] dynamic data)
        {
            string optname =data.optname;
           
            string msg = "操作失败!";
            switch (optname.Split(',')[0])
            {
               
                case "user"://编辑用户
                        long uid = data.userid;
                        string uname = data.username;
                        string upwd = data.userpwd;
                         long rid =1;
                     string userid1 = HttpContext.Session.GetString("xsdUserID");
                     string zxpwd1=HttpContext.Session.GetString("xsd_zxPwd");                       
                     if(Models.common.testzxpwd(zxpwd1,userid1))
                     {
                        
                        switch (optname.Split(',')[1])
                      {
                        case "add":
                            //添加用户表和用户角色表
                            string [] sqls ={
                                  "INSERT INTO  urp_user(f0001,f0002,f0003,f0004) VALUES(@f1,@f2,@f3,@f4)",
                                  "INSERT INTO  urp_user_role(f0002, f0003) VALUES(@f2,@f3)",
                                  "INSERT INTO  urp_userinfo(f0001,f0002,f0003,f0004,f0005) VALUES (@f1,@f2,'d:/public1/,1024,disk1,0;','D:/MyDep/doctest/trush/,1000','d:/doc1/,false;d:/img1/,false;d:/vedio1/,false')"};
                            List<Dictionary <string,dynamic>> lps=new List<Dictionary<string, dynamic>>();
                            Dictionary <string,dynamic> ps1=new Dictionary<string,dynamic>();
                            ps1.Add("@f1",uid);
                            ps1.Add("@f2",uname);
                            ps1.Add("@f3",Models.common.EncryptByMD5(upwd));  
                            ps1.Add("@f4",Models.StringSecurity.DESEncrypt(upwd));                          
                            lps.Add(ps1); 
                            Dictionary <string,dynamic> ps2=new Dictionary<string,dynamic>();
                            ps2.Add("@f2",uid);
                            ps2.Add("@f3",rid);
                            lps.Add(ps2);
                            Dictionary <string,dynamic> ps3=new Dictionary<string,dynamic>();
                            ps3.Add("@f1",uid);
                            ps3.Add("@f2",uname);
                            lps.Add(ps3);                            
                             int f1=Models.DataBase_SQLite.TranInsertDelEdit(sqls,lps);
                             if(f1==1)
                             {
                                  msg="添加成功！";
                             }else
                             {
                                  msg="添加失败！";
                             }
                          
                            break;
                        case "edit":
                            if(upwd.Trim()!="" )
                            {
                                string sqle1 ="UPDATE  urp_user  SET  f0002=@f2,f0003=@f3,f0004=@f4 WHERE f0001=@f1";                            
                                Dictionary <string,dynamic> pse1=new Dictionary<string,dynamic>();
                                pse1.Add("@f2",uname);
                                pse1.Add("@f3",Models.common.EncryptByMD5(upwd));
                                pse1.Add("@f4",Models.StringSecurity.DESEncrypt(upwd)); 
                                pse1.Add("@f1",uid); 
                                int fe1=Models.DataBase_SQLite.InsertDelEdit(sqle1,pse1);
                                if(fe1>0)
                                {
                                    msg="修改成功！";
                                }else
                                {
                                    msg="修改失败！";
                                }
                            }else
                            { 
                                string sqle2 ="UPDATE  urp_user  SET  f0002=@f2 WHERE f0001=@f1";                            
                                Dictionary <string,dynamic> pse2=new Dictionary<string,dynamic>();
                                pse2.Add("@f2",uname);                                
                                pse2.Add("@f1",uid); 
                                int fe2=Models.DataBase_SQLite.InsertDelEdit(sqle2,pse2);
                                if(fe2>0)
                                {
                                    msg="修改成功！";
                                }else
                                {
                                    msg="修改失败！";
                                }
                            } 
                        break;
                        case "del":

                         string [] sqlsd ={
                                  "DELETE FROM  urp_user WHERE f0001=@f1",
                                  "DELETE FROM  urp_user_role WHERE f0002=@f1",
                                  "DELETE FROM  urp_userinfo WHERE f0001=@f1"
                        };
                        List<Dictionary <string,dynamic>> lpsd=new List<Dictionary<string, dynamic>>();
                        Dictionary <string,dynamic> psd1=new Dictionary<string,dynamic>();
                        psd1.Add("@f1",uid); 
                        lpsd.Add(psd1);  
                        Dictionary <string,dynamic> psd2=new Dictionary<string,dynamic>();
                        psd2.Add("@f1",uid); 
                        lpsd.Add(psd2);  
                        Dictionary <string,dynamic> psd3=new Dictionary<string,dynamic>();
                        psd3.Add("@f1",uid); 
                        lpsd.Add(psd3);
                        int fd1=Models.DataBase_SQLite.TranInsertDelEdit(sqlsd,lpsd);
                        if(fd1==1)
                        {
                             msg="册除成功！";
                        }else
                        {
                            msg="删除失败！";
                        }
                        break;
                        case "edituserrole":
                           JArray rolerows = data.rolerows;
                           DataTable dt = new DataTable();
                           dt = JsonConvert.DeserializeObject<DataTable>(rolerows.ToString());//JArray to datatable
                            List<string> sqlurs = new List<string>();
                            List<Dictionary<string, dynamic>> urlps = new List<Dictionary<string, dynamic>>();
                            //删除
                            sqlurs.Add("DELETE FROM urp_user_role WHERE f0002=@userid");//删除指定用户角色集
                            Dictionary<string, dynamic> urps1 = new Dictionary<string, dynamic>();
                            urps1.Add("@userid", uid);
                            urlps.Add(urps1);
                            //添加
                            foreach(DataRow dr in dt.Rows)
                            {
                                sqlurs.Add("INSERT INTO urp_user_role(f0002, f0003)VALUES(@f2, @f3)");
                                Dictionary<string, dynamic> urpst = new Dictionary<string, dynamic>();
                                urpst.Add("@f2", uid);
                                urpst.Add("@f3",int.Parse(dr[0].ToString()));
                                urlps.Add(urpst);
                            }
                            int fur1 = Models.DataBase_SQLite.TranInsertDelEdit(sqlurs.ToArray(), urlps);
                            if (fur1 > 0)
                            {
                                msg = "修改成功！";
                            }else
                            {
                                msg = "修改失败！";
                            }
                            break;

                    }
                     }else
                     {
                         msg="未开启管理模式！";
                     }
                    break;
                case "role":   //编辑角色  
                     int roleid = data.rid;
                     string rolename = data.rname;
                     string rolememo = data.rmemo;
                     string userid2 = HttpContext.Session.GetString("xsdUserID");
                     string zxpwd2=HttpContext.Session.GetString("xsd_zxPwd");                       
                     if(Models.common.testzxpwd(zxpwd2,userid2))
                     {
                        switch (optname.Split(',')[1])
                    {
                        case "add":
                            string sqladd="INSERT INTO  urp_role(f0001, f0002, f0003) VALUES (@f1,@f2,@f3)";
                            Dictionary <string,dynamic> ps1=new Dictionary<string,dynamic>();
                            ps1.Add("@f1",roleid);
                            ps1.Add("@f2",rolename);       
                            ps1.Add("@f3",rolememo); 
                            int fa1=Models.DataBase_SQLite.InsertDelEdit(sqladd,ps1);      
                            if(fa1>0)  
                            {
                                msg = "添加成功！";
                            }  else
                            {
                                msg = "添加失败！";
                            } 
                            break;
                        case "edit":
                           string sqledit="UPDATE urp_role  SET  f0002=@f2, f0003=@f3 WHERE f0001=@f1";
                            Dictionary <string,dynamic> ps2=new Dictionary<string,dynamic>();                           
                            ps2.Add("@f2",rolename);       
                            ps2.Add("@f3",rolememo); 
                            ps2.Add("@f1",roleid);
                            int fa2=Models.DataBase_SQLite.InsertDelEdit(sqledit,ps2);      
                            if(fa2>0)  
                            {
                                msg = "修改成功！";
                            }  else
                            {
                                msg = "修改失败！";
                            } 
                            break;
                        case "del":
                           string[] sqldels = new string[2]
                             {
                                "DELETE FROM  urp_role_permission WHERE f0002=@rid",
                                "DELETE FROM  urp_role  WHERE f0001=@rid"
                             };
                            List<Dictionary <string,dynamic>> lpds=new List<Dictionary<string, dynamic>>();
                            Dictionary<string,dynamic> psd1=new Dictionary<string, dynamic> ();
                            psd1.Add("@rid",roleid);
                            lpds.Add(psd1);
                            Dictionary<string,dynamic> psd2=new Dictionary<string, dynamic> ();
                            psd2.Add("@rid",roleid);
                            lpds.Add(psd2);
                            //执行
                            int fd1 = Models.DataBase_SQLite.TranInsertDelEdit(sqldels,lpds);
                            if (fd1>0)
                            {
                                msg = "删除成功！";
                            }
                            else
                            {
                                msg = "删除失败！";
                            }
                            break;
                        case "editrolepermission":
                            string permissionids = data.permissionids;
                            string[] pids = permissionids.Split(",");//权限集
                            List<string> sqlrs = new List<string>(); 
                            List<Dictionary<string, dynamic>> rlps = new List<Dictionary<string, dynamic>>();
                            //删除
                            sqlrs.Add("DELETE FROM  urp_role_permission WHERE f0002=@rid");//删除指定用户角色集
                            Dictionary<string, dynamic> rps1 = new Dictionary<string, dynamic>();
                            rps1.Add("@rid", roleid);
                            rlps.Add(rps1);
                            //添加
                            foreach (string i1 in pids)
                            {
                                 if (i1.Trim() != "00")
                                 {
                                    sqlrs.Add("INSERT INTO urp_role_permission(f0002, f0003)VALUES(@f2, @f3)");
                                    Dictionary<string, dynamic> rpst = new Dictionary<string, dynamic>();
                                    rpst.Add("@f2",roleid);
                                    rpst.Add("@f3",int.Parse(i1));
                                    rlps.Add(rpst);
                                 }
                            }
                            int fur1 = Models.DataBase_SQLite.TranInsertDelEdit(sqlrs.ToArray(), rlps);
                            if (fur1 > 0)
                            {
                                msg = "修改成功！";
                            }else
                            {
                                msg = "修改失败！";
                            }                          
                            break;
                    }
                     }else
                     {
                        msg="未开启管理模式！";
                     }
                    break;
                case "EditMsgPool":  //删除消息池                
                     string userid3 = HttpContext.Session.GetString("xsdUserID");
                     string zxpwd3=HttpContext.Session.GetString("xsd_zxPwd");                       
                     if(Models.common.testzxpwd(zxpwd3,userid3))
                     {
                         switch (optname.Split(',')[1])
                    {
                        case "del":
                            string read_status = data.read_status;
                            string range_s = data.range_s;
                            int rangeA =data.rangeA;
                            int rangeB =data.rangeB;
                            int fem1 =0;
                            string ramsqlstr="DELETE FROM message_pool ";
                            switch(range_s)
                            {
                                case "AtoB":
                                   ramsqlstr=ramsqlstr+ " where  mid>=@range1 and mid<=@range2";
                                     switch(read_status)
                                     {   
                                          case "已阅读":
                                          ramsqlstr=ramsqlstr+" and status='已阅读'";     
                                          break;
                                          case "未阅读": 
                                          ramsqlstr=ramsqlstr+" and status='未阅读'";                                         
                                          break;
                                          case "ALL":
                                          break;
                                     }
                                    Dictionary<string,dynamic> psd=new Dictionary<string, dynamic> ();
                                    psd.Add("@range1", rangeA);
                                    psd.Add("@range2", rangeB); 
                                    fem1 = Models.DataBase_SQLite.InsertDelEdit(ramsqlstr,psd);
                                break;
                                case "ALL":
                                      switch(read_status)
                                     {   
                                          case "已阅读":
                                          ramsqlstr=ramsqlstr+ " where status='已阅读'"; 
                                          break;
                                          case "未阅读":
                                          ramsqlstr=ramsqlstr+ " where status='未阅读'"; 
                                          break;
                                          case "ALL":
                                          break;
                                     }
                                     fem1 = Models.DataBase_SQLite.InsertDelEdit(ramsqlstr);
                                break;

                            }
                           
                            if (fem1>0)
                            {
                                msg = "册除成功！";
                            }
                            else
                            {
                                msg = "删除失败！";
                            }
                            break;
                    }
                     }else
                     {
                                 msg="未开启管理模式！";
                     }
                break;
                case "UserInfo":  //我的信息      

                    switch (optname.Split(',')[1])
                    {
                        case "EditPwd":
                            string od = data.oldpwd;
                            string oldpwd =Models.common.EncryptByMD5(od);
                            string nd = data.newpwd1;   
                            string newpwd= Models.common.EncryptByMD5(nd);
                            string userid = HttpContext.Session.GetString("xsdUserID");
                            //修改密码
                                string sqlep = "UPDATE  urp_user  SET f0003=@f3 WHERE f0001=@f1 and f0003=@f3o"; 
                                Dictionary <string,dynamic> psep=new Dictionary<string,dynamic>();
                                psep.Add("@f3",newpwd);
                                psep.Add("@f1",int.Parse(userid));
                                psep.Add("@f3o",oldpwd);                            
                                int fep = Models.DataBase_SQLite.InsertDelEdit(sqlep, psep);                            
                                if (fep==1)
                                {
                                    
                                
                                    msg = "密码修改成功！";

                                }else
                                {
                                    msg = "密码修改失败!请输入正确的验证密码！";
                                }                             
                            break;
                            case "EditzxPwd":
                            string zxod = data.zxoldpwd;
                            string zxoldpwd =Models.StringSecurity.DESEncrypt(zxod);
                            string zxnd = data.zxnewpwd1;   
                            string zxnewpwd= Models.StringSecurity.DESEncrypt(zxnd);
                            string zxuserid = HttpContext.Session.GetString("xsdUserID");
                            //修改密码
                            string zxsqlep = "UPDATE  urp_user  SET f0004=@f4 WHERE f0001=@f1 and f0004=@f4o"; 
                             Dictionary <string,dynamic> zxpsep=new Dictionary<string,dynamic>();
                             zxpsep.Add("@f4",zxnewpwd);
                             zxpsep.Add("@f1",int.Parse(zxuserid));
                             zxpsep.Add("@f4o",zxoldpwd);                            
                            int zxfep = Models.DataBase_SQLite.InsertDelEdit(zxsqlep, zxpsep);                            
                            if (zxfep==1)
                            {
                                
                              
                                msg = "执行密码修改成功！";

                            }else
                            {
                                msg = "执行密码修改失败!请输入正确的验证密码！";
                            }
                            break;
                        case "SendMsg":

                            ////得到记录编号select max(mid)+1 FROM message_pool
                            //string sqlno = "select max(mid)+1 FROM message_pool";
                            //string rno = Models.DataBase_SQLite.GetOneValue(sqlno);
                            //int mid =1;                            
                            //if (rno!=null) mid =int.Parse(rno);  //得到编号 
                            string mdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm").Trim(); 
                            string Aname = HttpContext.Session.GetString("xsdUserName");
                            string tomsg = data.tomsg;
                            string Bname = data.toaccount;
                            //查找用户查看用户名是否合法，存在则发送，不存在提示错误
                            string sqluc = "select count(*) from urp_user where f0002=@f2";
                            Dictionary <string,dynamic> psu=new Dictionary<string,dynamic>();
                            psu.Add("@f2",Bname);
                            // SQLiteParameter[] psu = new SQLiteParameter[]
                            // {
                            //        new SQLiteParameter("@f2",Bname) 
                                   
                            // };
                            string rcount = Models.DataBase_SQLite.GetOneValue(sqluc,psu);                           
                            if (int.Parse(rcount)>0)
                            {
                                if (Models.MsgPool.SendMessagePool(Aname, Bname, tomsg))
                                {
                                    msg = "发送成功！";
                                }
                                else
                                {
                                    msg = "发送失败！";
                                }                               
                            }else
                            {
                                msg = "发送帐号名有误！";
                            }
                            break;
                        case "FlagMsg":
                            string fmid = data.mid;
                            string sql = "UPDATE  message_pool   SET  status=@status  WHERE mid=@mid";
                            Dictionary <string,dynamic> ps1=new Dictionary<string,dynamic>();
                            ps1.Add("@status", "已阅读");
                            ps1.Add("@mid", int.Parse(fmid));
                            // SQLiteParameter[] ps1 = new SQLiteParameter[]
                            // {    new SQLiteParameter("@status", "已阅读"),
                            //      new SQLiteParameter("@mid", int.Parse(fmid))
                            // };
                            int f1 = Models.DataBase_SQLite.InsertDelEdit(sql, ps1);
                            if (f1 == 1)
                            {
                                msg = "标记成功！";
                            }
                            else
                            {
                                msg="标记失败！";

                            }

                            break;
                        case "DelMsg":
                         string userid5 = HttpContext.Session.GetString("xsdUserID");
                        string zxpwd5=HttpContext.Session.GetString("xsd_zxPwd");                       
                        if(Models.common.testzxpwd(zxpwd5,userid5))
                        {
                            string Mid = data.mid;                         
                            string sqld = "DELETE FROM message_pool   WHERE mid=@mid and status=@status ";
                            Dictionary <string,dynamic> psd1=new Dictionary<string,dynamic>();                           
                            psd1.Add("@mid", int.Parse(Mid));
                            psd1.Add("@status", "已阅读");
                            // SQLiteParameter[] psd1 = new SQLiteParameter[]
                            // {    new SQLiteParameter("@status", "已阅读"),
                            //      new SQLiteParameter("@mid", int.Parse(Mid))
                            // };
                            int fd1 = Models.DataBase_SQLite.InsertDelEdit(sqld, psd1);
                            if (fd1 == 1)
                            {
                                msg = "删除成功！";
                            }
                            else
                            {
                                msg = "删除失败！";

                            }    
                     }else
                     {
                          msg="未开启管理模式！";
                     }                     
                            break;
                    }
                    break;
                case "setpath":  //设置路径
                     string userid6 = HttpContext.Session.GetString("xsdUserID");
                     string zxpwd6=HttpContext.Session.GetString("xsd_zxPwd");                       
                     if(Models.common.testzxpwd(zxpwd6,userid6))
                     {
                    switch (optname.Split(',')[1])
                    {
                        case "edit":
                            uid= data.userid;  
                            string f3= data.userpath; //用户操作目录
                            string f4 = data.trashpath;//用户回收站路径
                            string f5 = data.dirpath;//文件夹快捷路径
                            string sqld = "update urp_userinfo set f0003=@f3,f0004=@f4 ,f0005=@f5  WHERE f0001=@f1";
                            Dictionary <string,dynamic> psd1=new Dictionary<string,dynamic>();
                            psd1.Add("@f3", f3);
                            psd1.Add("@f4",f4);
                            psd1.Add("@f5",f5);
                            psd1.Add("@f1", uid);                           
                            int fd1 = Models.DataBase_SQLite.InsertDelEdit(sqld, psd1);
                            if (fd1 == 1)
                            {
                                msg = "设置成功！";
                            }
                            else
                            {
                                msg = "设置失败！";

                            }                        
                        break;
                    }
                     }else
                     {
                          msg="未开启管理模式！";
                     }
                    break;
                case "EditMode":  //设置模式
                    switch (optname.Split(',')[1])
                    {
                        case "change":
                            string mode1= data.mode;  
                            string zxpwd= data.zxpwd;
                            string zxpwdm=Models.StringSecurity.DESEncrypt(zxpwd);
                            string userid = HttpContext.Session.GetString("xsdUserID");
                            if(mode1.Trim()=="putong")
                            {
                               HttpContext.Session.SetString("xsd_zxPwd","*"); 
                               msg = "普通模式设置成功！"; 
                            }else 
                            {
                                //管理方式
                                //验证执行密码后标记密码
                                if(Models.common.testzxpwd(zxpwdm,userid))
                                {
                                       HttpContext.Session.SetString("xsd_zxPwd",zxpwdm);   
                                       msg = "管理模式设置成功！"; 
                                }else
                                {
                                       msg = "管理模式设置失败,密码错误！"; 
                                }  
                            }                         
                                                                             
                        break;                           
                    }   
                              
                    break;
                  default:
                    msg = "操作对象未找到！";
                    break;
            }//

            return msg;
        }

        
        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <returns></returns>
        // GET: /<controller>/
        [TypeFilter(typeof(CheckUserLoginFilter))]
        [CheckUserPermissionFilter("用户管理")]
        public IActionResult EditUser()
        {   
            //定义页面导航
            //  
            BreadCrumbNode[] bcnodes = new BreadCrumbNode[4];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");
            bcnodes[1] = new BreadCrumbNode("控制面板", "#");
            bcnodes[2] = new BreadCrumbNode("用户帐户", "#");
            bcnodes[3] = new BreadCrumbNode("用户管理", "/Urp/EditUser");
            ViewData["BreadCrumbNode_Data"] = bcnodes;           
             //*********************************************
            //以下是业务逻辑     
                   
                string uid=  HttpContext.Session.GetString("xsdUserID");
                //以下处理页面数据------------------------               
                string sql="select  urp_user.f0001 as f1,  urp_user.f0002 as f2, urp_user.f0003 as f3,group_concat(urp_user_role.f0003) as f4,group_concat(urp_role.f0002) as f5  from urp_user left join urp_user_role on urp_user.f0001 = urp_user_role.f0002 left join urp_role on urp_user_role.f0003 = urp_role.f0001 GROUP BY urp_user.f0001";
                DataTable dt = Models.DataBase_SQLite.GetDataTable(sql);                     
                JArray jd = JArray.Parse(JsonConvert.SerializeObject(dt));
                return this.View(jd);
            //  return RedirectToAction("LogoutRefush", "Home");//跳转到登录
             
        }
         /// <summary>
        /// 添加用户
        /// </summary>
        /// <returns></returns>
        // GET: /<controller>/
        public IActionResult EditUser_Add( )
        {   
             string sql1="select max(f0001)+1 as f1 FROM urp_user";
             string bm=Models.DataBase_SQLite.GetOneValue(sql1);
             if(bm=="") bm="1";
             ViewData["ubm"]=bm;           
             return this.View();
        }
         public IActionResult EditUser_Edit([FromBody] dynamic data)
        {   
             
             ViewData["ubm"]=data.ubm;
             ViewData["uname"]=data.uname;            
             return this.View();
        }
         public IActionResult EditUser_EditRole([FromBody] dynamic data)
        {  
            int  uid=data.ubm;
            //得到所有角色
            string sql1="select '0' as f1 ,f0001 as f2,f0002 as f3,f0003 as f4  FROM urp_role";
            DataTable dt=Models.DataBase_SQLite.GetDataTable(sql1);
           //得到当前用户角色
            string sqls="SELECT f0001, f0002, f0003  FROM urp_user_role where f0002=@uid";
            Dictionary<string,dynamic> ps=new Dictionary<string, dynamic> ();
            ps.Add("@uid",uid);
            DataTable dts=Models.DataBase_SQLite.GetDataTable(sqls,ps);
            foreach( DataRow dr in dt.Rows)
            {

                DataRow[] drs = dts.Select("f0003=" + dr[1].ToString().Trim());
                if (drs.Length > 0)
                {
                    dr[0] = "1";
                }
                 
            }           
            JArray jd = JArray.Parse(JsonConvert.SerializeObject(dt));
            return this.View(jd);
        }  
        
        /// <summary>
        /// 显示角色对话框
        /// </summary>
        /// <returns></returns>
        public IActionResult EditUser_GetRoleDialog()
        {
            string sql = "select f0001 as f1,f0002 as f2,f0003 as f3 from urp_role order by f0001";           
            DataTable dt = Models.DataBase_SQLite.GetDataTable(sql);                   
            JArray jd = JArray.Parse(JsonConvert.SerializeObject(dt));
            return this.View(jd);
        }
        /// <summary>
        /// 编辑角色
        /// </summary>
        /// <returns></returns>
        [TypeFilter(typeof(CheckUserLoginFilter))]
        [CheckUserPermissionFilter("角色管理")]
        public IActionResult EditRole()
        {
            //定义页面导航
            //  
            BreadCrumbNode[] bcnodes = new BreadCrumbNode[4];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");
            bcnodes[1] = new BreadCrumbNode("控制面板", "#");
            bcnodes[2] = new BreadCrumbNode("用户帐户", "#");
            bcnodes[3] = new BreadCrumbNode("角色管理", "/Urp/EditRole");
            ViewData["BreadCrumbNode_Data"] = bcnodes;
           
          
             //*********************************************
            //以下处理页面数据------------------
                //显示角色表 
                string sql1="select  f0001 as f1,f0002 as f2,f0003 as f3  FROM urp_role";
                DataTable dt=Models.DataBase_SQLite.GetDataTable(sql1);                 
                JArray jd = JArray.Parse(JsonConvert.SerializeObject(dt));
                return this.View(jd);            
        }       

         public IActionResult EditRole_Add()
        {
             string sql1="select max(f0001)+1 as f1 FROM  urp_role";
             string rbm=Models.DataBase_SQLite.GetOneValue(sql1);
             if(rbm=="") rbm="1";
             ViewData["rid"]=rbm;    
            return this.View();
        }    
          public IActionResult EditRole_Edit([FromBody] dynamic data)
        {
             ViewData["rid"]=data.rid;
             ViewData["rname"]=data.rname;
             ViewData["rmemo"]=data.rmemo;
            return this.View();
        } 
        public IActionResult EditRole_EditPermission([FromBody] dynamic data)
        {            
                ViewData["rid"]=data.rid;
                ViewData["rname"]=data.rname;
                ViewData["rmemo"]=data.rmemo; 
                //显示权限树
                string sql1="select  f0001 as f1,f0002 as f2,f0003 as f3  FROM urp_permission";
                DataTable dt=Models.DataBase_SQLite.GetDataTable(sql1);                 
                JArray jd = JArray.Parse(JsonConvert.SerializeObject(dt));
                return this.View(jd);
           
        }  
        /// <summary>
        /// 得到角色权限json字符串 角色管理用于显示
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public string GetRolePermission([FromBody] dynamic data)
        {  
             int roleid=data.roleid;
             string sql="select * from  urp_role_permission where F0002=@f2";
             Dictionary<string,dynamic> ps1=new Dictionary<string, dynamic> ();
             ps1.Add("@f2",roleid);
            DataTable dt=Models.DataBase_SQLite.GetDataTable(sql,ps1);
            return JsonConvert.SerializeObject(dt);             
        }
/// <summary>
/// 消息池管理
/// </summary>
/// <returns></returns>
 [TypeFilter(typeof(CheckUserLoginFilter))]
 [CheckUserPermissionFilter("消息池管理")]
 public IActionResult EditMsgPool()
        {   
            //定义页面导航
            //   
            BreadCrumbNode[] bcnodes = new BreadCrumbNode[4];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");
            bcnodes[1] = new BreadCrumbNode("控制面板", "#");
            bcnodes[2] = new BreadCrumbNode("设备管理器", "#");
            bcnodes[3] = new BreadCrumbNode("消息池管理", "/Urp/EditMsgPool");
            ViewData["BreadCrumbNode_Data"] = bcnodes;
           
           
             //*********************************************
            //--------------------
            //以下是业务逻辑        
                //以下处理页面数据------------------------  
                //查询消息池  
                string sql = "select mid as f1,mdate as f2,aname as f3 ,bname as f4,contents as f5,status as f6 from message_pool order by mid";
                DataTable dt = Models.DataBase_SQLite.GetDataTable(sql);              
                JArray jd = JArray.Parse(JsonConvert.SerializeObject(dt));
                return this.View(jd);
               
        }
        //
        /// <summary>
        /// 用户信息
        /// </summary>
        /// <returns></returns>
 [TypeFilter(typeof(CheckUserLoginFilter))]
 [CheckUserPermissionFilter("我的消息")]
      public IActionResult UserInfo()
        {  
            //定义页面导航
            //   
            BreadCrumbNode[] bcnodes = new BreadCrumbNode[4];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");
            bcnodes[1] = new BreadCrumbNode("控制面板", "#");
            bcnodes[2] = new BreadCrumbNode("个人设置", "#");
            bcnodes[3] = new BreadCrumbNode("我的消息", "/Urp/UserInfo");
            ViewData["BreadCrumbNode_Data"] = bcnodes;
           
           
             //*********************************************
            //以下是业务逻辑          
            //以下处理页面数据------------------------ 
            return this.View();
           
        }
         /// <summary>
        /// 用户信息
        /// </summary>
        /// <returns></returns>
      [TypeFilter(typeof(CheckUserLoginFilter))]
      [CheckUserPermissionFilter("我的设置")]
      public IActionResult UserSet()
        {  
            //定义页面导航
            //   
            BreadCrumbNode[] bcnodes = new BreadCrumbNode[4];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");
            bcnodes[1] = new BreadCrumbNode("控制面板", "#");
            bcnodes[2] = new BreadCrumbNode("个人设置", "#");
            bcnodes[3] = new BreadCrumbNode("我的设置", "/Urp/UserSet");
            ViewData["BreadCrumbNode_Data"] = bcnodes;
          
           
             //*********************************************
            //以下是业务逻辑          
            //以下处理页面数据------------------------ 
            return this.View();
           
        }

        [TypeFilter(typeof(CheckUserLoginFilter))]
        [CheckUserPermissionFilter("用户模式")]
        public IActionResult UserMode()
        {  
            //定义页面导航
            //   
            BreadCrumbNode[] bcnodes = new BreadCrumbNode[4];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");
            bcnodes[1] = new BreadCrumbNode("控制面板", "#");
            bcnodes[2] = new BreadCrumbNode("个人设置", "#");
            bcnodes[3] = new BreadCrumbNode("用户模式", "/Urp/UserMode");
            ViewData["BreadCrumbNode_Data"] = bcnodes;
          
         
             //*********************************************
            //以下是业务逻辑          
                         //以下处理页面数据------------------------ 
                string zxmm=HttpContext.Session.GetString("xsd_zxPwd");
                string umode="普通模式";
                if(zxmm.Trim()!="*")
                {
                   umode="管理模式";
                }
                ViewData["UserMode"] =umode;
                return this.View();          
        }
       public  string CheckUserMode()
        {
                string zxmm=HttpContext.Session.GetString("xsd_zxPwd");
                string umode="UserN";
                if(zxmm.Trim()!="*")
                {
                   umode="UserM";
                }    
            return umode;
        }
        //
        /// <summary>
        /// 用户信息管理 page1 得到发给我的消息
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UserInfoAPI_GetMyMsg()
        {
            string username= HttpContext.Session.GetString("xsdUserName");
            string sql = "select mid as f1,mdate as f2,aname as f3 ,contents as f4,status as f5 from message_pool where bname=@bname order by mid";
            Dictionary <string,dynamic> ps1 =new Dictionary<string,dynamic>();
               ps1.Add("@bname",username);  
            // SQLiteParameter[] ps1 = new SQLiteParameter[]
            // {    new SQLiteParameter("@bname",username)

            // };
            DataTable dt = Models.DataBase_SQLite.GetDataTable(sql,ps1);                                  
            return Json(new { data = dt});        
        }
        /// <summary>
        /// 用户信息管理 page2 得到我发出的消息
        /// </summary>
        /// <returns></returns>
         [HttpPost]
        public JsonResult UserInfoAPI_GetSendMsg()
        {
                      
            string username = HttpContext.Session.GetString("xsdUserName");
            string sql = "select mid as f1,mdate as f2,bname as f3 ,contents as f4,status as f5 from message_pool where aname=@aname order by mid";
             Dictionary <string,dynamic> ps1 =new Dictionary<string,dynamic>();
             ps1.Add("@aname",username);  
            // SQLiteParameter[] ps1 = new SQLiteParameter[]
            // {    new SQLiteParameter("@aname",username)

            // };
            DataTable dt = Models.DataBase_SQLite.GetDataTable(sql, ps1);           
            return Json(new { data = dt });        
        }
        //  


          /// <summary>
        /// 用户信息
        /// </summary>
        /// <returns></returns>
        [TypeFilter(typeof(CheckUserLoginFilter))]
        [CheckUserPermissionFilter("磁盘设置")]
      public IActionResult setpath()
        {  
            //定义页面导航
            //   
            BreadCrumbNode[] bcnodes = new BreadCrumbNode[4];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");
            bcnodes[1] = new BreadCrumbNode("控制面板", "#");
            bcnodes[2] = new BreadCrumbNode("设备管理器", "#");
            bcnodes[3] = new BreadCrumbNode("磁盘设置", "/Urp/SetPath");
            ViewData["BreadCrumbNode_Data"] = bcnodes;
          
             //*********************************************
            //以下是业务逻辑 
                //以下处理页面数据------------------------ 
                string sql = "select urp_user.f0001 as f1,urp_user.f0002 as f2,urp_userinfo.f0003 as f3,urp_userinfo.f0004 as f4,urp_userinfo.f0005 as f5 from urp_user  left JOIN urp_userinfo on  urp_user.f0001=urp_userinfo.f0001 ";
                DataTable dt = Models.DataBase_SQLite.GetDataTable(sql);                        
                JArray jd = JArray.Parse(JsonConvert.SerializeObject(dt));
                return this.View(jd);           
        }//
       
       [TypeFilter(typeof(CheckUserLoginFilter))]
        [CheckUserPermissionFilter("系统命令")]
         public IActionResult eshell()
        {    //定义页面导航             
            BreadCrumbNode[] bcnodes = new BreadCrumbNode[4];
            bcnodes[0] = new BreadCrumbNode("首页", "/Home/Home");   
            bcnodes[1] = new BreadCrumbNode("控制面板", "#");
            bcnodes[2] = new BreadCrumbNode("设备管理器", "#");                  
            bcnodes[3] = new BreadCrumbNode("系统命令", "/Urp/eshell");
            ViewData["BreadCrumbNode_Data"] = bcnodes;
           
          
             //*********************************************
            return this.View();
        }//
        
        [HttpPost]
        public string  wexecshell([FromBody] dynamic data)
        {  
            string userid1 = HttpContext.Session.GetString("xsdUserID");
            string zxpwd1=HttpContext.Session.GetString("xsd_zxPwd");  
            string opt=data.wshell;           
            string msg="未执行!";
            switch(opt)
            {
                case "halt":
                 
                   msg=NetCoreFilesManger.Models.wshell.halt();//关机
                break;
                case "reboot":
                
                   msg=NetCoreFilesManger.Models.wshell.reboot();//重启
                break;
                case "CancelTheOrder":
                 
                   msg=NetCoreFilesManger.Models.wshell.CancelTheOrder();//取消
                break;
                // case "rebootnet":
                //  if(Models.common.testzxpwd(zxpwd1,userid1))
                //    { 
                //         try
                //         {
                //             applicationLifetime.StopApplication();
                //         }catch(Exception ex)
                //         {
                //                 msg=ex.Message; //重启asp
                //         }
                //         msg="重启已操作！请刷新页面";
                //      }else
                //      {
                //         msg="未开启管理员模式！"; 
                //      }
                // break;
                case "Orther": 
                                             
                        if(Models.common.testzxpwd(zxpwd1,userid1))
                        {                  
                           msg=NetCoreFilesManger.Models.wshell.Orther(_hostingEnvironment.ContentRootPath);//来自配置的命令
                        }else
                        {
                            msg="未开启管理员模式！";
                        }
                break;
                // case "rar":    
                                            
                //         if(Models.common.testzxpwd(zxpwd1,userid1))
                //         {             
                //             msg=NetCoreFilesManger.Models.wshell.rar(_hostingEnvironment.ContentRootPath);//来自配置的命令
                //         }else
                //         {
                //             msg="未开启管理员模式！";
                //         }
                // break;
            }           
            return msg;
        }//
        public IActionResult setpath_upload()
        {  
            return this.View();
        }
        public string setpath_uploadfile()
        {
            string userid1 = HttpContext.Session.GetString("xsdUserID");
            string zxpwd1=HttpContext.Session.GetString("xsd_zxPwd");                       
            if(Models.common.testzxpwd(zxpwd1,userid1))
            {  
            var files = Request.Form.Files;           
            try
            {
                DataTable dt=new DataTable ();
                int f1=0;
                int c1=0;
                
               using (var ms = new MemoryStream())
                  {         
                       
                        if (files[0] != null)
                        {  
                            
                            files[0].CopyTo(ms);
                            
                        }
                        else
                        {
                            Request.Body.CopyTo(ms);
                        } 
                        ms.Seek(0, SeekOrigin.Begin);
                        string [] ss=new string[]{"f1","f2","f3","f4","f5"};
                        dt=Models.ExcelOpt.ReadExcelToTable(ms,"userdisksets",ss,true);
                        string sql = "select urp_user.f0001 as UserId,urp_user.f0002 as UserName,urp_userinfo.f0003 as DiskPathSets,urp_userinfo.f0004 as TrashSets,urp_userinfo.f0005 as FavorSets from urp_user  left JOIN urp_userinfo on  urp_user.f0001=urp_userinfo.f0001 ";
                        DataTable dts = Models.DataBase_SQLite.GetDataTable(sql);    
                        List<string> ssqls = new List<string>();
                        List<Dictionary<string, dynamic>> slps = new List<Dictionary<string, dynamic>>();
                        foreach (DataRow item in dt.Rows)
                        {
                           
                            if( dts.Select( "UserId="+item[0].ToString().Trim()).Count()>0)
                            {
                                ssqls.Add("UPDATE urp_userinfo SET f0003=@f3,f0004=@f4,f0005=@f5 WHERE f0001=@f1");
                                Dictionary<string, dynamic> pst = new Dictionary<string, dynamic>();
                                pst.Add( "@f3", item[2].ToString().Trim() );
                                pst.Add( "@f4", item[3].ToString().Trim() );
                                pst.Add( "@f5", item[4].ToString().Trim() );
                                pst.Add( "@f1", int.Parse(item[0].ToString()) );
                                slps.Add(pst);
                                c1++;
                            }
                        } 
                       f1=Models.DataBase_SQLite.TranInsertDelEdit(ssqls.ToArray(),slps);                                                 
                
                  }
                  if(f1>0)
                  {
                      return "上传更新成功！<br/>共"+dt.Rows.Count.ToString()+"条，有"+c1.ToString()+"条匹配！";
                  }
                  else
                  {
                      return "上传更新失败！<br/>共"+dt.Rows.Count.ToString()+"条，检测有"+c1.ToString()+"条匹配！";
                  }
              
                 
            }
            catch (Exception ex)
            {
                 return "上传失败！"+ex.Message.ToString();
            }
            }else
            {
                 return "未开启管理员模式!";

            }
        } 
         public IActionResult  setpathdownexcel()
        {  
            string userid1 = HttpContext.Session.GetString("xsdUserID");
            string zxpwd1=HttpContext.Session.GetString("xsd_zxPwd");                       
            if(Models.common.testzxpwd(zxpwd1,userid1))
            {   
                string sql = "select urp_user.f0001 as UserId,urp_user.f0002 as UserName,urp_userinfo.f0003 as DiskPathSets,urp_userinfo.f0004 as TrashSets,urp_userinfo.f0005 as FavorSets from urp_user  left JOIN urp_userinfo on  urp_user.f0001=urp_userinfo.f0001 ";
                DataTable dt = Models.DataBase_SQLite.GetDataTable(sql);      
                byte[] buffer = Models.ExcelOpt.CreateExcelFromDataTable(dt,"userdisksets", true); 
                return File(buffer, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",DateTime.Now.ToShortDateString().Replace("/", "_")+"V2007.xlsx");//2007                   
            }else
            {   
                 DataTable dt1=new DataTable ();
                 dt1.Columns.Add("msg_error");
                 dt1.Rows.Add("未开启管理员模式");
                 byte[] buffer1 = Models.ExcelOpt.CreateExcelFromDataTable(dt1,"userdisksets", true); 
                 return File(buffer1, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",DateTime.Now.ToShortDateString().Replace("/", "_")+"V未开启管理员模式.xlsx");//2007                   
                        
            }
        }
          public string  SetFavor([FromBody] dynamic data)
        {  
             string uid=data.uid;  
             string msg="初始化失败！"; 
             string sql ="UPDATE urp_userinfo SET f0005=@f5 WHERE f0001=@f1";
             Dictionary<string, dynamic> pst = new Dictionary<string, dynamic>();
             pst.Add( "@f5","d:/doc1/,false;d:/img1/,false;d:/vedio1/,false" );
             pst.Add( "@f1", int.Parse(uid) );
             int f1=Models.DataBase_SQLite.InsertDelEdit(sql,pst);
             if (f1>0)
             {
                msg="初始化成功！";
             }            
            return msg;
        }
        public IActionResult setpath_Trash([FromBody] dynamic data)
        {  
            string uid=data.uid;
            string uname=data.uname;
            string tpath=data.tpath;
            ViewData["uid"]=uid;
            ViewData["uname"]=uname;
            ViewData["trashpath"]=tpath.Split(",")[0];
            ViewData["trashspace"]=tpath.Split(",")[1];
            return this.View();
        }
          public string  SetTrashPath([FromBody] dynamic data)
        {  
            string userid1 = HttpContext.Session.GetString("xsdUserID");
            string zxpwd1=HttpContext.Session.GetString("xsd_zxPwd");                       
            if(Models.common.testzxpwd(zxpwd1,userid1))
            {     
                string uid=data.uid;  
                string tpath=data.tpath;  
                string tspace=data.tspace;  
                string msg="设置失败！"; 
                string sql ="UPDATE urp_userinfo SET f0004=@f4 WHERE f0001=@f1";
                Dictionary<string, dynamic> pst = new Dictionary<string, dynamic>();
                pst.Add( "@f4",tpath+","+tspace );
                pst.Add( "@f1", int.Parse(uid) );
                int f1=Models.DataBase_SQLite.InsertDelEdit(sql,pst);
                if (f1>0)
                {
                    msg="设置回收站成功！";
                }            
                return msg;
            }else
            {
                 return "未开启管理员模式！";
            }
        }
         public IActionResult setpath_Disk([FromBody] dynamic data)
        {     
           
                string uid=data.uid;
                string uname=data.uname;
                string dpath=data.dpath;
                ViewData["uid"]=uid;
                ViewData["uname"]=uname;
                string[] dpaths= dpath.Split(";");
                List<setdiskinfo> lds=new List<setdiskinfo> ();
                foreach (var item in dpaths)
                {
                    if(item!="")
                    {
                        setdiskinfo dst=new setdiskinfo ();
                        dst.label=item.Split(",")[2];//卷标
                        dst.path=item.Split(",")[0];//路径
                        dst.tspace=item.Split(",")[1];//总共
                        dst.uspace=item.Split(",")[3];//已用
                        lds.Add(dst);
                    }
                    
                }
                
                return this.View(lds);
             
        }
        public IActionResult UserModeMsg()
        {        //未开启管理员模式     
            return this.View();
        }
           public string  SetDiskPath([FromBody] dynamic data)
        {  
              string userid1 = HttpContext.Session.GetString("xsdUserID");
            string zxpwd1=HttpContext.Session.GetString("xsd_zxPwd");                       
            if(Models.common.testzxpwd(zxpwd1,userid1))
            {       
                            string uid=data.uid;  
                            string dpath=data.dpath;  
                            dpath = dpath.Substring(0,dpath.Length - 1);//去除最后一个字符 ;        
                            string msg="error"; 
                            string sql ="UPDATE urp_userinfo SET f0003=@f3 WHERE f0001=@f1";
                            Dictionary<string, dynamic> pst = new Dictionary<string, dynamic>();
                            pst.Add( "@f3",dpath);
                            pst.Add( "@f1", int.Parse(uid) );
                            int f1=Models.DataBase_SQLite.InsertDelEdit(sql,pst);
                            if (f1>0)
                            {
                                msg="ok";
                            }            
                            return msg;
            }else
            {
                return "未开启管理员模式！";
            }
                   
        }
      
    }  
    
     public class setdiskinfo
    {
         public string label { get; set; }
         public string path { get; set; }
         public string tspace { get; set; }   
         public string uspace { get; set; }      
        
    }  
}
