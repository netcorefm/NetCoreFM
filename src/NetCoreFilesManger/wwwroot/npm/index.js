import {myclass} from './src/js/mymodels'
let my=new myclass();
window.msgbox=my.msgbox; //信息提示函数
window.msgboxEvent=my.msgboxEvent; //信息提示框，带有确定与取消事件
window.CallDialog=my.CallDialog; //对话框，带有确定与取消事件
window.pInsertSelectObject=my.pInsertSelectObject;//select 填值
window.IsNumber=my.IsNumber; //判断数据是否为数值型
window.IsIntNumber=my.IsIntNumber; //判断数据是否为整数
window.IsWinPath=my.IsWinPath;
window.IsLinuxPath=my.IsLinuxPath;
 //bootbox位置与可移动
$(document).on("shown.bs.modal", ".modal", function(){

    let dialog = $(this).find(".modal-dialog");
    dialog.draggable({           
        cancel:".modal-body",//除了Models的内容以外都可点击拖动
        cursor: 'move',
        scroll: false
    });
    $(this).css("overflow", "hidden"); // 防止出现滚动条，出现的话，你会把滚动条一起拖着走的 
    //$(".modal-dialog").css("margin-top", "100px");//默认加载位置   
});
