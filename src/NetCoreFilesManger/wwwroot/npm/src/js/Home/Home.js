﻿//加载表格分页
var indextable1 = $('#indext1').DataTable({
    "searching": false,
    "lengthChange": false,
    "pagingType": "full_numbers",
    "language": {
        "url": "../lib/datatables/zh_CN.txt"
    },
    "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
    ]
});
indextable1.on("click", "button", function (e) {
    // alert(e.target.innerText);//按钮名称
    var row = indextable1.row($(this).parents('tr')).data();
    var htm11 = "<table class='table table-bordered'><tr><td>来自：" + row[2] + "</td></tr> <tr><td>时间：" + row[1] + "</td></tr><tr><td>内容:</td></tr><tr><td>" + row[3] + "</td></tr></table>";

    //配置参数
    var ps1 = {
        optname: 'UserInfo,FlagMsg',
        mid: row[0]
    };
    //调用查询
    $.ajax({
        url: '/Urp/AddEditDelOpt',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(ps1),
        success: function (data) {

            bootbox.alert({
                size: "large",
                title: "提示",
                buttons: {
                    ok: {
                        label: '确定',
                        className: 'btn-default'
                    }
                },
                message: htm11 + "<br/>阅读标记 " + data + "条记录",
                callback: function () {
                    window.location.reload();
                }
            });

        }
    });
});
