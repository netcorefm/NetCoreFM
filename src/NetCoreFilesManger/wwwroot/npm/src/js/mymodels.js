export let myclass=class{

    constructor( )
    {
        
    } 
    //给<select></select>添加项
    //var sJsonData = [{ name: "白", value: '1' }, { name: "黑", value: '0' }];
    pInsertSelectObject(sObjID, sJsonData)
    {
      sJsonData.forEach(ev => {
          $(sObjID).append("<option value=" + ev.value + ">" + ev.name + "</option>");
      });
    }
    //验证字符串是否是数字
    IsNumber(theObj)
    {
      var reg = /^[0-9]+.?[0-9]*$/;
      if (reg.test(theObj)) {
        return true;
      }
      return false;
    }
    //验证字符串是否是整数
    IsIntNumber(theObj)
    {
        var reg = /^\d+$/;
        if (reg.test(theObj)) {
        return true;
        }
        return false;
    }
    IsWinPath(theObj)
    {
        var winpath = /^[a-zA-Z]:[\/(\w\W+\/?)+]*\/$/
        if (winpath.test(theObj)) {
        return true;
        }
        return false;
    }
    IsLinuxPath(theObj)
    {
        var lnxPath =/^\/[(\w\W+\/?)+]*\/$/ 
        if (lnxPath.test(theObj)) {
        return true;
        }
        return false;
    }
   
    
    //以下是封装bootbox使用----------------------------------------
//信息提示框，无回调事件
 msgbox(stitle, stext) {
  bootbox.alert({
      size: "large",
      title:stitle,
      buttons: {
         ok: {
             label: '确定',
             className: 'btn-primary'
         }
      },
      message: stext
  });  
}
//信息提示框，带有确定与取消事件
 msgboxEvent(stitle,stext,okexec,cancelexec)
{
  let wd1=bootbox.confirm({
      title:stitle,
      size: "large",
      message:stext,
      buttons: {
      confirm: {
          label: '确定',
          className: 'btn-primary'
      },
      cancel: {
          label: '取消',
          className: 'btn-primary'
      }
      },
      callback: function (result) {

          if (result)
          {
              if(okexec!==undefined)
              {
                okexec(); //确定    
              }         
          }else
          { 
               if(cancelexec!==undefined)
              {
                  cancelexec();//取消
              }
          }        
      }
  });//bootbox   
}
//录入对话框，带有确定与取消事件
 CallDialog(sps,surl,stitle,okexec,cancelexec)//sps为post参数可以为""做占位,cancelexec 可以为空不写,点取消时的事件
{      
  $.ajax({
      url: surl,
      type: 'POST',
      contentType: 'application/json',
      data:JSON.stringify(sps),
      success: function (data) {
      //      
      let wd2=bootbox.confirm({
          title:stitle,
          size: "large",
          message:data,
          buttons: {
          confirm: {
              label: '确定',
              className: 'btn-primary'
          },
          cancel: {
              label: '取消',
              className: 'btn-primary'
          }
          },
          callback: function (result) {

              if (result)
              {
                  if(okexec!==undefined)
                  {
                      okexec(); //确定    
                  }         
              }else
              { 
                   if(cancelexec!==undefined)
                  {
                      cancelexec();//取消
                  }
              }            
          }
      });//bootbox   
  }});
  
}
      
    
    //
}; //定义一个类，全局的window.car,不用window时在webpack封中后所有变量为内部变量,在这里ES2015我们用export导出用import导入功能进行变量暴漏



 