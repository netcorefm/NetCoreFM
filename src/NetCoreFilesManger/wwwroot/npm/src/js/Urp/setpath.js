var table =$('#t11').DataTable({
    "pagingType": "full_numbers",    
    "language": {
    "url":"../lib/datatables/zh_CN.txt"
    },
    "lengthMenu": [[10,25,50, -1], [10,25,50, "All"]]
});
//单击表格行事件
$('#t11 tbody').on('click', 'tr', function () {
    $("#userid").val(table.row(this).data()[0]);
    $("#username").val(table.row(this).data()[1]);   
    $("#userpath").val(table.row(this).data()[2]);   
    $("#trashpath").val(table.row(this).data()[3]); 
    $("#dirpath").val(table.row(this).data()[4]);  
});

//修改
$("#editbt").click(function () {

    var data = {
        optname: 'setpath,edit',
        userid: $("#userid").val(),
        username: $("#username").val(),
        userpath: $("#userpath").val(),
        trashpath: $("#trashpath").val(),
        dirpath: $("#dirpath").val()
       
    };     
   
    if (data.trashpath === ""   && data.userpath === ""&& data.dirpath === "")
     {
         
        msgbox("提示", "不能为空！");
    }
    else {
        $.ajax({
            url: '/Urp/AddEditDelOpt',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (data) {
                //
                msgbox("提示", data);
                prefresh('/Urp/SetPath');
            }
        });
    }
});
//统计磁盘空间并保存
$("#countbt").click(function () {
    var ps =
     {
        
        uid: $("#userid").val(),
    }
    if(ps.uid!=="")
    {

        $.ajax({
            url: '/FileManger/FilesManger/DiskSpaceUseCount',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(ps),
            success: function (data) {
                //
                msgbox("提示", data);
                prefresh('/Urp/SetPath');
            }
        });
    }else
    {
        msgbox("提示", "请选择一个用户！");
    }
  
});