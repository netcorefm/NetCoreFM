﻿//page1 修改密码
$("#editpwd").click(function () {
    //配置参数
    var data = {
        optname: 'UserInfo,EditPwd',       
        oldpwd: $("#oldpwd").val(),
        newpwd1: $("#newpwd1").val(),
        newpwd2: $("#newpwd2").val()
    };
    //api调用处理
    if (data.oldpwd !== "" && data.newpwd1 !== "" && data.newpwd2 !== "" & data.newpwd1 === data.newpwd2)
    {
        $.ajax({
            url: '/Urp/AddEditDelOpt',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (data) {
                msgbox("提示", data);
                prefresh("/Urp/UserInfo");//刷新
            }
        });
    } else
    {
        msgbox("提示", "不能为空！新密码校验要一致！");

    }

});
//page1 修改密码
$("#editzxpwd").click(function () {
    //配置参数
    var data = {
        optname: 'UserInfo,EditzxPwd',       
        zxoldpwd: $("#zxoldpwd").val(),
        zxnewpwd1: $("#zxnewpwd1").val(),
        zxnewpwd2: $("#zxnewpwd2").val()
    };
    //api调用处理
    if (data.zxoldpwd !== "" && data.zxnewpwd1 !== "" && data.zxnewpwd2 !== "" & data.zxnewpwd1 === data.zxnewpwd2)
    {
        $.ajax({
            url: '/Urp/AddEditDelOpt',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (data) {
                msgbox("提示", data);
                prefresh("/Urp/UserInfo");//刷新
            }
        });
    } else
    {
        msgbox("提示", "不能为空！新密码校验要一致！");

    }

});


//使用datatables t1 page2
    var table1= $('#t1').DataTable({         
        "ajax": {
            "url": '/Urp/UserInfoAPI_GetMyMsg',
            "type": "POST"
        },
        "columns": [
            { "data": "f1" },
            { "data": "f2" },
            { "data": "f3" },
            { "data": "f4" },
            { "data": "f5" },
            {
                "defaultContent":"0",
                    "targets":-1
            }  

                     
           
        ],
        "columnDefs": [

            {
                "targets": [5],//隐藏"f1'列ID               
                "render": function (data, type, row, meta) {
                    if (row.f5 == "未阅读") {
                        return "<button class='btn btn-primary'>阅读</button>&nbsp;&nbsp;&nbsp;&nbsp; <button class='btn btn-default'>删除</button>";

                    } else
                    {
                        return "<button class='btn btn-default'>阅读</button>&nbsp;&nbsp;&nbsp;&nbsp; <button class='btn btn-default'>删除</button>";

                    };
                  
                   
                }
            },
            {
                "targets": [0],//隐藏"f1'列ID
                "visible": false,
                "searchable": false
            },
            {
                "targets": [3],//隐藏"f4'列 内容
                "visible": false,
                "searchable": true
            } 
            
               
             
        ],
        "bAutoWidth": false,  
        "pagingType": "full_numbers",
        "language": {
            "url": "../lib/datatables/zh_CN.txt"
        },
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
});
//t1 的按钮事件 标记为已阅读
$("#t1 tbody").on("click", "button", function (e) {
    var row = table1.row($(this).parents('tr')).data();
    var htm1 = "<table class='table table-bordered'><tr><td>来自：" + row.f3 + "</td></tr> <tr><td>时间：" + row.f2 + "</td></tr><tr><td>内容:</td></tr><tr><td>" + row.f4 + "</td></tr></table>";

    switch (e.target.innerText) {
        case "阅读":
           
            if (row.f5 != "已阅读") {
                //配置参数
                var ps1 = {
                    optname: 'UserInfo,FlagMsg',
                    mid: row.f1
                };
                //api调用处理
                $.ajax({
                    url: '/Urp/AddEditDelOpt',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(ps1),
                    success: function (data) {
                        msgbox("提示", htm1 + "<br/>阅读标记 " + data + "条记录");//显示具体内容
                        //刷新表格
                        table1.ajax.reload();
                    }
                });

            } else
            {  
                msgbox("提示", htm1);//显示具体内容
            }
           
          
           
            break;
        case "删除":             
            bootbox.confirm({
                title: "提示",
                size: "large",
                message: "你确定删除吗？<br/><hr>" + htm1,   
                buttons: {
                    confirm: {
                        label: '确定',
                        className: 'btn-primary'
                    },
                    cancel: {
                        label: '取消',
                        className: 'btn-primary'
                    }
                 },            
                callback: function (result) {

                    if (result) {
                        if (row.f5 == "已阅读")
                        {
                            //配置参数
                            var data = {
                                optname: 'UserInfo,DelMsg',
                                mid: row.f1
                            };
                            //api调用处理                            
                                $.ajax({
                                    url: '/Urp/AddEditDelOpt',
                                    type: 'POST',
                                    contentType: 'application/json',
                                    data: JSON.stringify(data),
                                    success: function (data) {
                                        msgbox("提示", data);
                                        //刷新表格
                                        table1.ajax.reload();

                                    }
                                });

                        }
                        else
                        {
                            msgbox("提示", "只有阅读过的消息才可以删除！");
                        }
                       
                       
                    }
                    else {
                       // msgbox("提示", "删除失败！");
                    };

                }
            });
            break;
    }
   
    
    //*************

});
    
//使用datatables t2 page3
var table2= $('#t2').DataTable({         
    "ajax": {
        "url": '/Urp/UserInfoAPI_GetSendMsg',
        "type": "POST"
    },
    "columns": [
        { "data": "f1" },
        { "data": "f2" },
        { "data": "f3" },
        { "data": "f4" },
        { "data": "f5" },
    ],
    "columnDefs": [
        {
            "targets": [0],//隐藏"f1'列
            "visible": false,
            "searchable": false
        }
    ],
    "bAutoWidth": false,  
    "pagingType": "full_numbers",
    "language": {
        "url": "../lib/datatables/zh_CN.txt"
    },
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
});
//page3 发送消息
$("#sendmsg").click(function () {
    //配置参数
    var data = {
        optname: 'UserInfo,SendMsg',
        toaccount: $("#toaccount").val(),
        tomsg: $("#tomsg").val()
       
    };
    //api调用处理
    if (data.toaccount !== "" && data.tomsg!=="") {
        $.ajax({
            url: '/Urp/AddEditDelOpt',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (data) {
                msgbox("提示", data);
                //刷新表格
            table2.ajax.reload();

            }
        });
    } else {
        msgbox("提示", "不能为空！新密码校验要一致！");

    }

});
//标签导航进入事件
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    switch (e.target.innerText)
    {
        case "密码修改":
            break;
        case "接收消息":
            table1.ajax.reload();
            dispMsgCount();
            break;
        case "发送消息":
            table2.ajax.reload();
            break;
        case "网站主题":
              
            break;

          

    };
});
var skds = [
    { name: "绿色", value: 'site_green' },
    { name: "蓝色", value: 'site_blue' },
    { name: "黑色", value: 'site_blank' }   
];
pInsertSelectObject("#skinsname", skds);//方式
$("#skinsbt1").click(function(){
    //在此保存，在_layout.cshtml引用名称处理，以下做的是即时更新
    //$("#skinid").attr("href","/css/"+$("#skinsname").val()+".css");
    $.cookie("Bs_yy_skins", $("#skinsname").val(), { expires:90,path:'/'}); //存储一个带3天期限的cookie
     window.location.reload();  
     
});

