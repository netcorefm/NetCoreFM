//下载文件---------------------------------------------------------------------------------------
//显示刷新正在压缩
var tzipMdialog = bootbox.dialog({
    message: '<div   class="text-center"><i class="fa fa-spin fa-spinner"></i> 正在压缩下载，请等待...<span id="tsecondid">0</span>...</div>',
    closeButton: false,
    backdrop:false,
    show: false
 }); 
//显示计时 
 var tmysecond=0;
 function tmyTimer() {
    tmysecond=tmysecond+1;
    $("#tsecondid").text(tmysecond);
 } 
var tmyinterval = setInterval(tmyTimer, 2000); //初始化启动
 clearInterval(tmyinterval );//停止
 //

$("#filedown").click(function (e) { 
    
   var jarray = [];
   var objs1 = $("input:checkbox[name='cbox']:checked");
   objs1.each(function (index, element) {
       var data1 = {          
           name: $(objs1[index]).parent().nextAll().eq(0).text(),
           type: $(objs1[index]).parent().nextAll().eq(1).text()
       };
       jarray.push(data1);
      }); 
  if(jarray.length>0)
  {
  $.ajax({
     url: '/FileManger/FilesManger/trashdownload',
     type: 'POST',
     contentType: 'application/json',  
     data: JSON.stringify(jarray),    
     success: function (data) {
          
           //***
           bootbox.confirm({
              title: "<i class='fa fa-download' aria-hidden='true'></i>下载文档",
              size: "large",
              message:data,
              buttons: {
                  confirm: {
                      label: '打包zip下载',
                      className: 'btn-default'
                  },
                  cancel: {
                      label: '取消',
                      className: 'btn-default'
                  }
              },
              callback: function (result) {
                  if (result)
                  {
                        //打包下载调用  
                              
                          postdowndf('/FileManger/FilesManger/trashdownloadzipfile',JSON.stringify(jarray)); 
                          tzipMdialog.modal('show');//显示提示正在压缩
                          tmysecond=0;//从0秒开始显示
                          tmyinterval = setInterval(tmyTimer, 2000); //启动
                  }
                  else
                  {
                  };
              }
          });//**


     }});
  
  }else
  {
     msgbox("提示","请选择 文件或文件夹");
  }
});
tprogress1.max = 100;  // 数据的总大小
tprogress1.value =0;  // 当前已上传的大小
//JQuery的ajax函数的返回类型只有xml、text、json、html等类型，没有“流”类型，所以我们要实现ajax下载，不能够使用相应的ajax函数进行文件下载。
//但可以用js生成一个form，用这个form提交参数，并返回“流”类型的数据。在实现过程中，页面也没有进行刷新。
function postdowndf(url1,data1) 
{
                   
        var xhr = new XMLHttpRequest();
        xhr.addEventListener("progress", updateProgress, false);
        xhr.open('post', url1, true);//get请求，请求地址，是否异步
        xhr.responseType = "blob";    // 返回类型blob
        xhr.setRequestHeader('content-type', 'application/json');
        xhr.onload = function () {// 请求完成处理函数
        if (this.status === 200) 
        {
            var blob = this.response;// 获取返回值
            var a = document.createElement('a');
            var tday1 = new Date();          
            var tsdate = tday1.getFullYear()+"_" + (tday1.getMonth()+1) + "_" + tday1.getDate();
            a.download = 'trashall'+tsdate+'.zip';
            a.href=window.URL.createObjectURL(blob);
            a.click();
            tprogress1.max = 100;  // 数据的总大小
            tprogress1.value =0;  // 当前已下载的大小           
            $("#tproglable").html("");
            clearInterval(tmyinterval );//停止
            tzipMdialog.modal('hide');//隐藏提示正在压缩
        }
        };
        // 发送ajax请求
        xhr.send(data1);

}

function updateProgress(evt) {
    if (evt.lengthComputable)
     {
        let percentComplete =parseInt(evt.loaded*100 / evt.total);
        tprogress1.max = evt.total;  // 数据的总大小
        tprogress1.value =evt.loaded;  // 当前已下载的大小
        let flength = evt.total + " byte";
        let fint = evt.total.toString().length       
        if (fint <= 3)
        { //byte
 
        }
        else
        {
            if (fint <= 6)
            {  //kb
                flength = (evt.total / 1024).toFixed(2)  + " Kb";
            }
            else
            {  //Mb
                flength = (evt.total / 1024 / 1024 ).toFixed(2)+ " Mb";
            }
        }
       $("#tproglable").html(percentComplete+"%("+tprogress1.value+"/"+flength +")");       
    } 
    else
     {
       $("#tproglable").html("");
       
    }
  }

//全部选择
$("#sdfall").click(function () {
    var allcheckObj = $('[name=sdf]');
    for (var i = 0; i < allcheckObj.length; i++) {
        allcheckObj[i].checked = this.checked;
    }
}); 
//初始化显示文件列表 
// var xdata = {   
//     opt:"current",
//     cdir:""//$("#xpath").html()
//  };
//  if(xdata.cdir!="")
//  {
//  $.ajax({
//     url: '/FileManger/FilesManger/TrashDblist',
//     type: 'POST',
//     contentType: 'application/json',
//     data: JSON.stringify(xdata),
//     success: function (data) {
//         $("#optfd").html(data);
//      }
//  });
// }
//返回上一级
$("#uplevelbt").click(function (e) { 
    
    // //跳到上一级    
    var cdata = {  
       opt:"uplevel",  
       cdir:""
    };
      $.ajax({
          url: '/FileManger/FilesManger/TrashDblist',
          type: 'POST',
          contentType: 'application/json',
          data: JSON.stringify(cdata),
          success: function (data) {
             $("#optfd").html(data);
              
           }
       });     
   
 });
 $("#downlevelbt").click(function (e) {
   //下一级
    var cdata = {  
       opt:"downlevel",  
       cdir:""
    }; 
       var objs = $("input:checkbox[name='cbox']:checked");
       if (objs.length>0)
       {
             var objtype="文件";
             objtype= $(objs[0]).parent().nextAll().eq(1).text();
             if(objtype=="文件夹")
             {
                   cdata.cdir=$(objs[0]).parent().nextAll().eq(0).text();;
                   $.ajax({
                      url: '/FileManger/FilesManger/TrashDblist',
                      type: 'POST',
                      contentType: 'application/json',
                      data: JSON.stringify(cdata),
                      success: function (data) {
                         $("#optfd").html(data);
                         //alert(data);
                      }
                   });
             }
      }
    });
 //清除
 $("#cleartrash").click(function (e) { 
    var objs = $("input:checkbox[name='cbox']:checked");
    var jarray = [];  
    objs.each(function (index, element) {
        var data1 = {
            name: $(objs[index]).parent().nextAll().eq(0).text(),
            type: $(objs[index]).parent().nextAll().eq(1).text()
        };
        jarray.push(data1);
    });
   $.ajax({
      url: '/FileManger/FilesManger/DeleteTrashDirAllFile',
      type: 'POST',
      contentType: 'application/json',  
      data: JSON.stringify(jarray),    
      success: function (data) {  
               msgbox("提示",data);
               file_reload();
      }}); 
 });
 //下载

 //刷新
 function file_reload()
{
   var cdata = {  
      opt:"current",  
      cdir:""
   };     
    $.ajax({
         url: '/FileManger/FilesManger/TrashDblist',
         type: 'POST',
         contentType: 'application/json',
         data: JSON.stringify(cdata),
         success: function (data) {
            $("#optfd").html(data);
            //alert(data);
          }
      }); 
}
file_reload();