﻿//侧边栏隐藏与展开
$("#side_button").click(function ()
        {
        if ($(".sidebar").is(":hidden")) {
            $(".sidebar").show();
            if($("#page-wrapper").width()>768)
            {
              $("#page-wrapper").css("margin-left","200px");
            }
        }
        else {
            $(".sidebar").hide();  
            $("#page-wrapper").css("margin-left","0px");
        }
        

});
$('#page-wrapper').resize(function(){

    if ($(".sidebar").is(":hidden"))     {
        
          $("#page-wrapper").css("margin-left","0px");  
    }
    else 
    {
         
        if(!$("#right_button").is(":hidden"))
        {
          $("#page-wrapper").css("margin-left","0px"); 
        }else
        {
        $("#page-wrapper").css("margin-left","200px");/*侧边栏宽度 4个地方需修改一致 另2处在site.css */
        }
    }

});
//加载侧边栏菜单结点，并做单击事件——打开功能窗口
disptreeview();
function disptreeview() {
let fl=($("#leftsidebar").css("background-color")!="rgba(0, 0, 0, 0)");
if(fl)
{
var menut1=null;
$.ajax({
    type: 'POST',
    url: "/Home/GetTreeData",
    async: false,
    data:"",
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    success: function (data) {
        if (data) {
          menut1=$('#MenuTreeView1').treeview({
                data: data,                
                onhoverColor: $("#leftsidebar").css("background-color"),               
                selectedBackColor:$("#leftsidebar").css("background-color"),
                levels:1,
                expandIcon: "picon picon-folder",
                collapseIcon:"picon picon-folder-open",
                onNodeSelected: function (event, data) {                  
                    if (data.tags instanceof Array) {
                        
                        if (data.tags[0] != "#") {
                            DispRefresh("正在加载...",700);                            
                            $.ajax({
                                url: data.tags[0],
                                type: 'GET',                              
                                complete: function (response) {
                                    if (response.status == 404) {

                                        $("#page-wrapper").html("正在实现中...");
                                        
                                    } else {
                                        
                                        $("#page-wrapper").html(response.responseText);
                                        
                                       
                                    }
                                }
                            });
                        } else {
                            //做展开或收起处理
                            if (data.nodes != null) {
                                var select_node = $('#MenuTreeView1').treeview('getSelected');
                                
                                if (select_node[0].state.expanded) {
                                    $('#MenuTreeView1').treeview('collapseNode', select_node);
                                    select_node[0].state.selected = false;
                                }
                                else {
                                    $('#MenuTreeView1').treeview('expandNode', select_node);
                                    select_node[0].state.selected = false;
                                }
                            }
                        }
                    }
                }
            });
        }
    },
    error: function (error) {
        alert(error.responseText);
    }
});

}else
{  
    setTimeout("disptreeview()",20);
}
};
//更新框架主页面
function prefresh(myurl) {
    $.ajax({
        url: myurl,
        type: 'GET',       
        complete: function (response) {
            if (response.status === 404) {

                $("#page-wrapper").html("正在实现中...");
                
            } else {
               
                $("#page-wrapper").html(response.responseText);
                      

            }            
        }
    });
   
}
//显示提示内容ms秒
function DispRefresh(ctext,ms)
{
    var Mdialog = bootbox.dialog({
        message: '<div   class="text-center"><i class="fa fa-spin fa-spinner"></i>'+ctext+'...</div>',
        closeButton: false,
        backdrop: false
    }); 
    Mdialog.init(function () {
        $(".modal-dialog").css("margin-top", "150px");
        setTimeout(function () {
            Mdialog.modal('hide');
        },ms);         
    });
   
}
//我的设置
$("#mysets").click(function () {

    $.ajax({
        url: "/Urp/UserInfo",
        type: 'GET',       
        complete: function (response) {
            if (response.status === 404) {

                $("#page-wrapper").html("正在实现中...");
            } else {
                $("#page-wrapper").html(response.responseText);
                $('#myTab a[href="#changePWD"]').tab('show');
            }
        }
    });

});
$("#exeshell").click(function () {
    prefresh( "/Urp/eshell");
});
//未读消息提示处理
$("#msg_button").click(function () {

    $.ajax({
        url: "/Urp/UserInfo",
        type: 'GET',       
        complete: function (response) {
            if (response.status === 404) {

                $("#page-wrapper").html("正在实现中...");
            } else {
                $("#page-wrapper").html(response.responseText);
                $('#myTab a[href="#acceptMSG"]').tab('show');//显示我的个信息-接收信息窗体
            }
        }
    });

});
function dispMsgCount() {
    $.ajax({
        url: '/Urp/UserInfoAPI_GetMyMsg',
        type: 'Post',
        contentType: 'application/json',
        success: function (data) {
            //以下设置未读消息个数
            var data1 = data.data;//从对像格式为{"data":[{"name":"ping"},{"id":"1"}]} 中取[{"name":"ping"},{"id":"1"}]对像
            var urmsg = data1.filter(function (e) { return e.f5 === "未阅读"; });
            if (urmsg.length > 0) {
                $(".badge").html(urmsg.length);
                $(".badge").css("background-color", "#f00");
                $(".badge").css("color", "#fff");

            } else {
                $(".badge").html(0);
                $(".badge").css("background-color", "#fff");
                $(".badge").css("color", "#337AB7");
            }
        }


        //
    });
}
dispMsgCount();

function isIE() {
    if(!!window.ActiveXObject || "ActiveXObject" in window){
        return true;
    }else{
        return false;
    　　 }
}
if(isIE())
{
    window.location.href= "/Home/LoginOut";
}


var source = new EventSource('/Home/GetLoginStatus');
source.onmessage = function (event) {
     if(event.data!=="ok")
     { 
       
        window.location.href= "/Home/QLogout";
       
     }else
     {
         $("#ptimenow").text(event.data);
     }   
};

source.onopen = function(event) {
    console.log('onopen');
};

source.onerror = function(event) {
    console.log('onerror');
}