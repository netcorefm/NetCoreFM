//创建webpack.config.js
var webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');//html插件,npm install html-webpack-plugin --save-dev 
//const ExtractTextPlugin = require('extract-text-webpack-plugin');//分离插件, 1.npm install --save-dev extract-text-webpack-plugin 2. npm install --save-dev extract-text-webpack-plugin@next
const CopyPlugin = require('copy-webpack-plugin');     
module.exports = {
     entry:'./index.js', //入口文件
     output:{
          //node.js中__dirname变量获取当前模块文件所在目录的完整绝对路径 
          path:__dirname, //输出位置
          filename:'../fmjs/mymodels.js' //输出文件
     }, 
     plugins: [ 
         //  new  HtmlWebpackPlugin(),//生成默认的index.html ,以下是自定义，此插件支持生成1个及以上个html          
          new HtmlWebpackPlugin(
          {
           //title:"食动力",//产生的页面标题,由模版时此项失效
          filename:"../../Views/Shared/_Layout.cshtml",//产生html格式的页面文件,我们这里使用的是aps.net core mvc的view视图文件扩展名
          template: './src/_layout.cshtml',//使用的模版
          inject:'head',  //在模版head区产生，默认在</body>前
          hash:true //这项在更新生产系统是非常有用，有效的清除了客户端的缓存，使用升级更安全
          }),
          //new ExtractTextPlugin('./dist/css/mysite_style.css'),//从出口js中分离出css
		  new CopyPlugin([
                 { from: './node_modules/font-awesome', to: '../lib/font-awesome' },
                 { from: './node_modules/jquery', to: '../lib/jquery' },
                 { from: './orther_modules/jquery-ui-1.12.1', to: '../lib/jquery-ui' },    
                 { from: './node_modules/jquery.cookie/jquery.cookie.js', to: '../lib/jquery.cookie/jquery.cookie.js' },
                 { from: './node_modules/jquery-contextmenu', to: '../lib/jquery-contextmenu' },
                 { from: './node_modules/bootstrap', to: '../lib/bootstrap' },//插件原样复制到项目库
                 { from: './node_modules/bootbox', to: '../lib/bootbox' },
                 { from: './node_modules/bootstrap-treeview', to: '../lib/bootstrap-treeview' },   
                 { from: './node_modules/datatables.net', to: '../lib/datatables/net' },             
                 { from: './node_modules/datatables.net-bs', to: '../lib/datatables/net-bs' },
                 { from: './node_modules/datatables.net-fixedcolumns-bs', to: '../lib/datatables/fixedcolumns-bs' },
                 { from: './node_modules/datatables.net-select', to: '../lib/datatables/select' }, 
                 { from: './node_modules/datatables.net-select-bs', to: '../lib/datatables/select-bs' }, 
                              
               ]) 
    ],
     //module: {
     //  rules: [         
     //   {
      //     test: /\.css$/,  
      //     use: ExtractTextPlugin.extract({
      //       fallback: "style-loader",
      //       use: ["css-loader"] 
       //    })
      //   }//从出口js中分离出css
     //   ]
     // }
     mode: 'development'

    }