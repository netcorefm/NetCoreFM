 
 //上传文件---------------------------------
 $("#fileupload").click(function (e) {        
$.ajax({
         url: '/FileManger/FilesManger/MyComputer_upload',
         type: 'POST',
         contentType: 'application/json',       
         success: function (data) {
            // msgbox("<i class='fa fa-cloud-upload' aria-hidden='true'></i>上传文档",data);
             
            //***
               bootbox.confirm({
               title: "<i class='fa fa-cloud-upload' aria-hidden='true'></i>上传文档",
               size: "large",
               message:data,
               buttons: {
                   confirm: {
                       label: '上传',
                       className: 'btn-default'
                   },
                   cancel: {
                       label: '取消',
                       className: 'btn-default'
                   }
               },
               callback: function (result) {

                   if (result) {
                      　//确定                    
                      Upload();
                      
                   }
                   else
                   {
                   };
               }
           });//**


          }
      });
    
});
//下载文件---------------------------------------------------------------------------------------
   //显示刷新正在压缩
   var zipMdialog = bootbox.dialog({
      message: '<div   class="text-center"><i class="fa fa-spin fa-spinner"></i> 正在压缩下载，请等待...<span id="secondid">0</span>...</div>',
      closeButton: false,
      backdrop:false,
      show: false
   }); 
  //显示计时 
   var mysecond=0;
   function myTimer() {
      mysecond=mysecond+1;
      $("#secondid").text(mysecond);
   } 
 var myinterval = setInterval(myTimer, 2000); //初始化启动
   clearInterval(myinterval );//停止

//下载
$("#filedown").click(function (e) { 
    
   var Mdialogd = bootbox.dialog({
      message: '<div   class="text-center"><i class="fa fa-spin fa-spinner"></i>正在统计列表，根据文件多少需要的时长也不一样。。。请等待。。。</div>',
      closeButton: false,
      backdrop: false
  }); 
  Mdialogd.init(function () {
      $(".modal-dialog").css("margin-top", "150px"); 
  });
    var jarray = [];  //格式 名字：类型
     if($("#listtype").attr("title")=="详细列表")   
     {   
      var objs1 = $("input:checkbox[name='cbox']:checked");
    objs1.each(function (index, element) {
        var data1 = {           
            name: $(objs1[index]).parent().nextAll().eq(0).text(),
            type: $(objs1[index]).parent().nextAll().eq(1).text()
        };
        jarray.push(data1);
       });
   }else
   {  var objs2 = $("input:checkbox[name='cbox1']:checked");
      
       
      objs2.each(function (index, element) {
         
         var data2 = {
             name: $(objs2[index]).parent().parent().nextAll().eq(0).text().trim(),
             type: "文件"
         };
         if($(objs2[index]).parent().eq(0).find("i").attr("class").trim()=="picon picon-dir")
         {
            data2.type="文件夹";            
         }
         jarray.push(data2);
        });
   }
   //以上得到参数jarry [{name:n1,type:t1},.....] 
   if(jarray.length>0)
   {
      //显示下载页面
   $.ajax({
      url: '/FileManger/FilesManger/MyComputer_download',
      type: 'POST',
      contentType: 'application/json',  
      data: JSON.stringify(jarray),    
      success: function (data) {
            Mdialogd.modal('hide');   
            //***
            bootbox.confirm({
               title: "<i class='fa fa-download' aria-hidden='true'></i>下载文档",
               size: "large",
               message:data,
               buttons: {
                   confirm: {
                       label: '打包ZIP(不推荐使用改方式下载）',
                       className: 'btn-default'
                   },
                   cancel: {
                       label: '取消',
                       className: 'btn-default'
                   }
               },
               callback: function (result) {
                   if (result)
                   {
                           //打包下载调用，选择的当前目录的文件及文件夹集合：jarry [{name:n1,type:t1},.....]                   
                           postdowndf('/FileManger/FilesManger/downloadzipfile',JSON.stringify(jarray));
                           zipMdialog.modal('show');//显示提示正在压缩
                           mysecond=0;//从0秒开始显示
                           myinterval = setInterval(myTimer, 2000); //启动
                   }
                   else
                   {
                   };
               }
           });//**


      }});
   
   }else
   {
      msgbox("提示","请选择 文件或文件夹");
   }
 });
 
 progress1.max = 100;  // 数据的总大小
 progress1.value =0;  // 当前已下载的大小
//JQuery的ajax函数的返回类型只有xml、text、json、html等类型，没有“流”类型，所以我们要实现ajax下载，不能够使用相应的ajax函数进行文件下载。
//但可以用js生成一个form，用这个form提交参数，并返回“流”类型的数据。在实现过程中，页面也没有进行刷新。
 function postdowndf(url1,data1) 
 {             
      var xhr = new XMLHttpRequest();
      xhr.addEventListener("progress", updateProgress, false);
      xhr.open('post', url1, true);//get请求，请求地址，是否异步
      xhr.responseType = "blob";    // 返回类型blob
      xhr.setRequestHeader('content-type', 'application/json');
      xhr.onload = function () {// 请求完成处理函数
            if (this.status === 200) 
            {
               var blob = this.response;// 获取返回值
               var a = document.createElement('a');
               var day1 = new Date();              
               var sdate = day1.getFullYear()+"_" + (day1.getMonth()+1) + "_" + day1.getDate();
               a.download = 'all'+sdate+'.zip';
               a.href=window.URL.createObjectURL(blob);
               a.click();
               progress1.max = 100;  // 数据的总大小
               progress1.value =0;  // 当前已下载的大小
               $("#proglable").html("");
               clearInterval(myinterval );//停止
               zipMdialog.modal('hide');//隐藏提示正在压缩
              
            }
         };
         // 发送ajax请求
         xhr.send(data1);        

}
function updateProgress(evt) {
   if (evt.lengthComputable)
    {
       let percentComplete =parseInt(evt.loaded*100 / evt.total);
       progress1.max = evt.total;  // 数据的总大小
       progress1.value =evt.loaded;  // 当前已下载的大小
       let loadedlength =evt.loaded + " byte";
       let loadint = evt.loaded.toString().length       
       if (loadint <= 3)
       { //byte

       }
       else
       {
           if (loadint <= 6)
           {  //kb
               loadedlength = (evt.loaded / 1024).toFixed(2)  + " Kb";
           }
           else
           {  //Mb
               loadedlength = (evt.loaded / 1024 / 1024 ).toFixed(2)+ " Mb";
           }
       }
       let flength = evt.total + " byte";
       let fint = evt.total.toString().length       
       if (fint <= 3)
       { //byte

       }
       else
       {
           if (fint <= 6)
           {  //kb
               flength = (evt.total / 1024).toFixed(2)  + " Kb";
           }
           else
           {  //Mb
               flength = (evt.total / 1024 / 1024 ).toFixed(2)+ " Mb";
           }
       }
      $("#proglable").html(percentComplete+"%("+loadedlength+"/"+flength +")");       
   } 
   else
    {
      $("#proglable").html("");
      
   }
 }
 //删除文件------------------------------------------------------------------------
 $("#filedelete").click(function (e) { 

   var jarray = [];  
     if($("#listtype").attr("title")=="详细列表")   
     {   
      var objs1 = $("input:checkbox[name='cbox']:checked");
    objs1.each(function (index, element) {
        var data1 = {
            name: $(objs1[index]).parent().nextAll().eq(0).text(),
            type: $(objs1[index]).parent().nextAll().eq(1).text()
        };
        jarray.push(data1);
       });
   }else
   {  var objs2 = $("input:checkbox[name='cbox1']:checked");
      
       
      objs2.each(function (index, element) {
         
         var data2 = {
             name: $(objs2[index]).parent().parent().nextAll().eq(0).text().trim(),
             type: "文件"
         };
         if($(objs2[index]).parent().eq(0).find("i").attr("class").trim()=="picon picon-dir")
         {
            data2.type="文件夹";            
         }
         jarray.push(data2);
        });
   }
   if(jarray.length>0)
   {


         $.ajax({
            url: '/FileManger/FilesManger/DeleteDirAllFile',
            type: 'POST',
            contentType: 'application/json',  
            data: JSON.stringify(jarray),    
            success: function (data) {  
                     msgbox("提示",data);
                     file_reload();
            }}); 

   }else
   {
      msgbox("提示","请选择 文件或文件夹");
   }
});
 
//新建文件夹---------------------------------------------------------------------------------------
$("#createdir").click(function (e) { 
   
            //***
            bootbox.confirm({
               title: "<i class='fa fa-minus-square-o' aria-hidden='true'></i>新建文件夹",
               size: "large",
               message:"<table><tr><td style='width:150px'>请输入文件夹名称：</td><td><input id='dirname' type='text' size=25></td></tr></table>",
               buttons: {
                   confirm: {
                       label: '新建',
                       className: 'btn-default'
                   },
                   cancel: {
                       label: '取消',
                       className: 'btn-default'
                   }
               },
               callback: function (result) {

                   if (result)
                   {
                           var data = {
                              name: $("#dirname").val()
                        };   
                     $.ajax({
                        url: '/FileManger/FilesManger/createdir',
                        type: 'POST',
                        contentType: 'application/json',  
                        data: JSON.stringify(data),    
                        success: function (data) {  
                                 msgbox("提示",data);
                                 file_reload();
                        }}); 
                        }
                        else
                        {
                        };
               }
           });//** 
});
//重命名------------------------------------------------------------------------
$("#renameid").click(function (e) { 

   
   var objs; 
   if($("#listtype").attr("title")=="详细列表")   
   {
      objs = $("input:checkbox[name='cbox']:checked");
   }    else
   {
      objs = $("input:checkbox[name='cbox1']:checked");
   }
    
    
      var oname="";
      var ftype="文件";
      if($("#listtype").attr("title")=="详细列表")   
      {  
         oname=$(objs[0]).parent().nextAll().eq(0).text();
         ftype=$(objs[0]).parent().nextAll().eq(1).text();
      }else
      {
          
         if($(objs).length>0)
         {
         oname=$(objs[0]).parent().parent().nextAll().eq(0).text().trim();
         if($(objs[0]).parent().eq(0).find("i").attr("class").trim()=="picon picon-dir")
         {
           ftype="文件夹";            
         }}
      }   
      
     if(oname.trim()!=""){
   //***
   bootbox.confirm({
      title: "<i class='fa fa-minus-square-o' aria-hidden='true'></i>重命名",
      size: "large",
      message:"<table><tr><td>旧名称为：</td><td>"+oname+"</td></tr><tr><td style='width:150px'>请输入新的名称：</td><td><input id='newnameid' type='text' size=25></td></tr></table>",
      buttons: {
          confirm: {
              label: '确定修改',
              className: 'btn-default'
          },
          cancel: {
              label: '取消',
              className: 'btn-default'
          }
      },
      callback: function (result) {

          if (result)
          { 
                       var data = {      
                        oldname: oname,
                        newname: $("#newnameid").val(),
                        type: ftype
                        }; 
                        $.ajax({
                           url: '/FileManger/FilesManger/dirfilesrename',
                           type: 'POST',
                           contentType: 'application/json',  
                           data: JSON.stringify(data),    
                           success: function (data) {  
                                    msgbox("提示",data);
                                    file_reload();
                           }}); 
          }
          else
            { //取消
            };
      }
  });//** 

}else
{
   msgbox("提示","请选择1个文件或文件夹");
}  //
});

 
//复制------------------------------------------------------------------------
 
$("#copyid").click(function (e) { 

   var jarray = [];  
     if($("#listtype").attr("title")=="详细列表")   
     {   
      var objs1 = $("input:checkbox[name='cbox']:checked");
    objs1.each(function (index, element) {
        var data1 = {
            name: $(objs1[index]).parent().nextAll().eq(0).text(),
            type: $(objs1[index]).parent().nextAll().eq(1).text()
        };
        jarray.push(data1);
       });
   }else
   {  var objs2 = $("input:checkbox[name='cbox1']:checked");
      
       
      objs2.each(function (index, element) {
         
         var data2 = {
             name: $(objs2[index]).parent().parent().nextAll().eq(0).text().trim(),
             type: "文件"
         };
         if($(objs2[index]).parent().eq(0).find("i").attr("class").trim()=="picon picon-dir")
         {
            data2.type="文件夹";            
         }
         jarray.push(data2);
        });
   }
   if(jarray.length>0)
   {
         $.ajax({
            url: '/FileManger/FilesManger/CopySelectFile',
            type: 'POST',
            contentType: 'application/json',  
            data: JSON.stringify(jarray),    
            success: function (data) {  
                     msgbox("提示",data);                    
            }}); 

   }else
   {
      msgbox("提示","请选择 文件或文件夹");
   }
});
//粘贴------------------------------------------------------------------------
$("#pasteid").click(function (e) { 
   
   var Mdialogc = bootbox.dialog({
      message: '<div   class="text-center"><i class="fa fa-spin fa-spinner"></i>正在粘贴，根据文件大小需要的时长也不一样。。。请等待。。。</div>',
      closeButton: false,
      backdrop: false
  }); 
  Mdialogc.init(function () {
      $(".modal-dialog").css("margin-top", "150px"); 
  });
   $.ajax({
      url: '/FileManger/FilesManger/pasteSelectFile',
      type: 'POST',
      contentType: 'application/json', 
      success: function (data) {  
               msgbox("提示",data); 
               Mdialogc.modal('hide');    
               file_reload();                   
      }}); 

});
 
//页面加载后初始化显示
 var xdata = {   
   opt:"current",
   cdir:""//$("#xpath").html()
};
$.ajax({
   url: '/FileManger/FilesManger/displist',
   type: 'POST',
   contentType: 'application/json',
   data: JSON.stringify(xdata),
   success: function (data) {
       $("#optfd").html(data);
    }
});
//切换到列表视图
 $("#displistid").click(function (e) { 

         $("#listtype").attr("title","详细列表")
         var ldata = {   
            opt:"current",
            cdir:$("#xpath").html()
         };
         $.ajax({
            url: '/FileManger/FilesManger/displist',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(ldata),
            success: function (data) {
               $("#optfd").html(data);
            }
         });//
    
});

//切换到图标视图
$("#dispiconid").click(function (e) { 
      $("#listtype").attr("title","大图标");
      var idata = {   
         opt:"current",
         cdir:$("#xpath").html()
      };
      $.ajax({
         url: '/FileManger/FilesManger/dispicon',
         type: 'POST',
         contentType: 'application/json',
         data: JSON.stringify(idata),
         success: function (data) {
            $("#optfd").html(data);
         }
      });//    
});
//跳到上一级---------------------------------------------
$("#uplevelbt").click(function (e) {    
  
   
   var cdata = {  
      opt:"uplevel",  
      cdir:""
   };     
   if($("#listtype").attr("title")=="详细列表")
   {
      $.ajax({
         url: '/FileManger/FilesManger/displist',
         type: 'POST',
         contentType: 'application/json',
         data: JSON.stringify(cdata),
         success: function (data) {
            $("#optfd").html(data);
            //alert(data);
          }
      });

   }else
   {
      $.ajax({
         url: '/FileManger/FilesManger/dispicon',
         type: 'POST',
         contentType: 'application/json',
         data: JSON.stringify(cdata),
         success: function (data) {
            $("#optfd").html(data);
            //alert(data);
          }
      });
   }
  
}); 
$("#downlevelbt").click(function (e) {    
  
   
   var cdata = {  
      opt:"downlevel",  
      cdir:""
   };     
   if($("#listtype").attr("title")=="详细列表")
   {
      var objs = $("input:checkbox[name='cbox']:checked");
      if (objs.length>0)
      {
            var objtype="文件";
            objtype= $(objs[0]).parent().nextAll().eq(1).text();
            if(objtype=="文件夹")
            {
                  cdata.cdir=$(objs[0]).parent().nextAll().eq(0).text();;
                  $.ajax({
                     url: '/FileManger/FilesManger/displist',
                     type: 'POST',
                     contentType: 'application/json',
                     data: JSON.stringify(cdata),
                     success: function (data) {
                        $("#optfd").html(data);
                        //alert(data);
                     }
                  });
            }
     }

   }else
   {   var objs1 = $("input:checkbox[name='cbox1']:checked");
      if(objs1.length>0)
      {       
         if($(objs1[0]).parent().eq(0).find("i").attr("class").trim()=="picon picon-dir")
         {
            cdata.cdir=$(objs1[0]).parent().parent().nextAll().eq(0).text().trim();
            $.ajax({
               url: '/FileManger/FilesManger/dispicon',
               type: 'POST',
               contentType: 'application/json',
               data: JSON.stringify(cdata),
               success: function (data) {
                  $("#optfd").html(data);
                  //alert(data);
               }
            });
         }
     }
   }
  
}); 
//刷新----------------------------------------------------
function file_reload()
{
   var cdata = {  
      opt:"current",  
      cdir:""
   };     
   if($("#listtype").attr("title")=="详细列表")
   {
      $.ajax({
         url: '/FileManger/FilesManger/displist',
         type: 'POST',
         contentType: 'application/json',
         data: JSON.stringify(cdata),
         success: function (data) {
            $("#optfd").html(data);
            //alert(data);
          }
      });

   }else
   {
      $.ajax({
         url: '/FileManger/FilesManger/dispicon',
         type: 'POST',
         contentType: 'application/json',
         data: JSON.stringify(cdata),
         success: function (data) {
            $("#optfd").html(data);
            //alert(data);
          }
      });
   }
}

//点击预览开关事件
$('.switch-anim').click(function(){
   if($('.switch-anim').prop("checked")==false)
   {
      $("#prview").css("display","none");//关时把窗口也关了
   }
});
$('#closeprview').click(function(){
   $("#prview").css("display","none");//关时把窗口也关了
});
$('#enlargeview').click(function(){
   let height=$("#img1").height();
   $.get("/FileManger/FilesManger/preimg",{filename:$("#pviewfilename").text(),changkuan:height+50}, function(data){
      $("#img1").attr("src",data);                              
   });
});
$('#reduceview').click(function(){
   let height=$("#img1").height();
   height=height-50;
   if(height<240)    height=240;
   $.get("/FileManger/FilesManger/preimg",{filename:$("#pviewfilename").text(),changkuan:height}, function(data){
      $("#img1").attr("src",data);                              
   });
});
$("#img1").on('load', function() {
  dx();
});
function dx( )
{
   let height=$("#img1").height();
   let width=$("#img1").width();
   if(height<240) height=220;
   if(width<220) width=220;
   $("#prview").width(width+20);
   $("#prview").height(height+$("#prviewtitle").height()+20);
   $("#prviewimg").width(width+20);
   $("#prviewimg").height(height+20);   
   $("#prview").css("bottom","5px");
   $("#prview").css("right","5px");
   

}
$('#prview').draggable();

