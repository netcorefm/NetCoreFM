﻿//加载表格分页
var usertable1 =$('#t11').DataTable({
    "columnDefs": [
        {
            "targets": [2,3],//隐藏列
            "visible": false,
            "searchable": false
        }
    ], 
    "pagingType": "full_numbers",       
    "select": {
        "style": 'single'
    },   
    "language": {
    "url":"../lib/datatables/zh_CN.txt"
    },
    "lengthMenu": [[10,25,50, -1], [10,25,50, "All"]]
});
//单击表格行事件
// $('#t11 tbody').on('click', 'tr', function () {
//     $("#userid").val(table.row(this).data()[0]);
//     $("#username").val(table.row(this).data()[1]);
//     $("#userpwd").val(table.row(this).data()[1]);
//     $("#roleid").html(table.row(this).data()[3]);
//     $("#rolename").html(table.row(this).data()[4]);
// });
$("#adduser").click(function () {  
    
    CallDialog("","/Urp/EditUser_Add","添加用户",function(){
        
         //确定
        //得到数据
           var psdata = {
            optname:'user,add',
            userid: $("#uid").text(),
            username: $("#uname").text(),
            userpwd: $("#upwd").text()
            };
            //添加
            if (psdata.userid === "" || psdata.username === "" || psdata.userpwd === "" ) 
            {
               msgbox("提示", "不能为空！");
            }
            else
            {  
                //alert(JSON.stringify(psdata));                              
                $.ajax({
                    url: '/Urp/AddEditDelOpt',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(psdata),
                    success: function (data)
                        {
                        //
                        msgbox("提示", data);
                        prefresh('/Urp/EditUser');
                    }
                });
            }        
    });         
});

//修改
$("#edituser").click(function () {

    let rows=usertable1.rows({selected: true}).data(); //得到选中行数组数据   
    if(rows[0] !==undefined)
    {
        let ps={
            ubm:rows[0][0],
            uname:rows[0][1]
        }
        CallDialog(ps,"/Urp/EditUser_edit","修改用户",function(){
              //确定
               var psdata = {
                optname:'user,edit',
                userid: $("#uid").text(),
                username: $("#uname").text(),
                userpwd: $("#upwd").text()
                };
                if (psdata.userid === "" || psdata.username === ""  ) 
                {
                    msgbox("提示", "不能为空！");
                }
                else
                {
                    $.ajax({
                        url: '/Urp/AddEditDelOpt',
                        type: 'POST',
                        contentType: 'application/json',
                        data: JSON.stringify(psdata),
                        success: function (data)
                        {
                            //
                            msgbox("提示", data);
                            prefresh('/Urp/EditUser');
                        }
                    });
                }
              
        });


    }
    else
    {
        msgbox("提示","请选择一条用户记录！");

    }   
});
//删除
$("#deluser").click(function () {

    let rows=usertable1.rows({selected: true}).data(); //得到选中行数组数据   
    if(rows[0] !==undefined)
    {   
         msgboxEvent("删除提示","<span style='color:red'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i></span>你确定要删除吗?",function()
          {     
            let ps1={
                optname:'user,del',
                userid: rows[0][0],
                username: rows[0][1],
                userpwd: rows[0][2]
                 
                };
                // msgbox("",JSON.stringify(ps1));
            $.ajax({
                url: '/Urp/AddEditDelOpt',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(ps1),
                success: function (data) {
                    //
                    msgbox("提示", data);
                    prefresh('/Urp/EditUser');
                }
            });
       });
    }else
    {
        msgbox("提示","请选择一条用户记录！");

    }
});
//设置角色
function GetCheckBoxTableRowsJson()
{
    //得到选中行数据，形成json数组
    var myArray = new Array();
    var temps = $("[name=sqx]");    
    for (var i = 0; i < temps.length; i++)
    {
        if (temps[i].checked)
        {

            myArray.push({ roleid: $(temps[i].parentElement).nextAll().eq(0).html(), rolename: $(temps[i].parentElement).nextAll().eq(1).html(), rolememo: $(temps[i].parentElement).nextAll().eq(2).html() });
        }         
    }
    return myArray;
}
$("#edituserrole").click(function () {  
    let rows=usertable1.rows({selected: true}).data(); //得到选中行数组数据   
    if(rows[0] !==undefined)
    { 
        let ps={
            ubm:rows[0][0] //用户ID
        }
        CallDialog(ps,"/Urp/EditUser_EditRole","用户角色",function(){
            //确定
            //参数
            var psdata = {
                optname: 'user,edituserrole',
                userid: rows[0][0],
                rolerows: GetCheckBoxTableRowsJson()
            };                                  
            $.ajax({
                url: '/Urp/AddEditDelOpt',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(psdata),
                success: function (data) {
                    //
                    msgbox("提示", data);
                    prefresh('/Urp/EditUser');
                }
            });
            
      });

    }else
    {
        msgbox("提示","请选择一条用户记录！");

    }

});
