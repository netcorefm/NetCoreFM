var pathtable =$('#t11').DataTable({
    "pagingType": "full_numbers",
    "select": {
        "style": 'single'
    },       
    "language": {
    "url":"../lib/datatables/zh_CN.txt"
    },
    "lengthMenu": [[10,25,50, -1], [10,25,50, "All"]]
});
//单击表格行事件
// $('#t11 tbody').on('click', 'tr', function () {
//     $("#userid").val(table.row(this).data()[0]);
//     $("#username").val(table.row(this).data()[1]);   
//     $("#userpath").val(table.row(this).data()[2]);   
//     $("#trashpath").val(table.row(this).data()[3]); 
//     $("#dirpath").val(table.row(this).data()[4]);  
// });

//修改
// $("#editbt").click(function () {

//     var data = {
//         optname: 'setpath,edit',
//         userid: $("#userid").val(),
//         username: $("#username").val(),
//         userpath: $("#userpath").val(),
//         trashpath: $("#trashpath").val(),
//         dirpath: $("#dirpath").val()
       
//     };     
   
//     if (data.trashpath === ""   && data.userpath === ""&& data.dirpath === "")
//      {
         
//         msgbox("提示", "不能为空！");
//     }
//     else {
//         $.ajax({
//             url: '/Urp/AddEditDelOpt',
//             type: 'POST',
//             contentType: 'application/json',
//             data: JSON.stringify(data),
//             success: function (data) {
//                 //
//                 msgbox("提示", data);
//                 prefresh('/Urp/SetPath');
//             }
//         });
//     }
// });
//统计磁盘空间并保存
// $("#ptotal").click(function () {
//     var ps =
//      {
        
//         uid: $("#userid").val(),
//     }
//     if(ps.uid!=="")
//     {

//         $.ajax({
//             url: '/FileManger/FilesManger/DiskSpaceUseCount',
//             type: 'POST',
//             contentType: 'application/json',
//             data: JSON.stringify(ps),
//             success: function (data) {
//                 //
//                 msgbox("提示", data);
//                 prefresh('/Urp/SetPath');
//             }
//         });
//     }else
//     {
//         msgbox("提示", "请选择一个用户！");
//     }
  
// });
//设置磁盘目录
$("#psetdiskdir").click(function () {
    let rows=pathtable.rows({selected: true}).data(); //得到选中行数组数据   
    if(rows[0] !==undefined)
    {
         let ps={
            uid:rows[0][0],
            uname:rows[0][1],
            dpath:rows[0][2]
         }
         CallDialog(ps,"/Urp/setpath_Disk","设置磁盘目录",function(){  
            let ps1={
                uid:rows[0][0],
                uname:rows[0][1],
                dpath:""
             }
            ps1.dpath=getpath();
            if(ps1.dpath.trim()=="SetPath_Error_Msg")
            {
                msgbox("提示","格式不正确！");

            }else
            {
                $.ajax({
                    url: '/Urp/SetDiskPath',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(ps1),
                    success: function (data) {  
                           if(data=="ok")
                           {
                             tc(ps1.uid);
                           }else
                           {
                             msgbox("提示","设置失败！"+data);
                           }
                      }
                    });    
               
            }

         });
       
    }else
    {
        msgbox("提示","请选择一条记录！");

    }
});
function tc( uids)
{
    var ps =
         {
            
            uid:uids
        };
    $.ajax({
            url: '/FileManger/FilesManger/DiskSpaceUseCount',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(ps),
            success: function (data) {
                
                msgbox("提示","设置磁盘目录成功！<br>"+data);
                prefresh('/Urp/SetPath');
            }
        });
}
//设置回收站目录
$("#psettrashdir").click(function () {
   
    let rows=pathtable.rows({selected: true}).data(); //得到选中行数组数据   
    if(rows[0] !==undefined)
    {
        let ps={
           uid:rows[0][0],
           uname:rows[0][1],
           tpath:rows[0][3]
        }
        CallDialog(ps,"/Urp/setpath_Trash","设置回收站目录",function(){  
            
            let ps1={
                uid:rows[0][0],
                uname:rows[0][1],
                tpath:$("#ptpath").text(),
                tspace:$("#ptspace").text()
             };
             ps1.tpath= ps1.tpath.replace(/\\/g,"/");
             //alert(ps1.tpath);
             if(ps1.tpath!=="" && ps1.tspace!=="" && IsIntNumber(ps1.tspace) && (IsWinPath(ps1.tpath)||IsLinuxPath(ps1.tpath)))
             { 
                 $.ajax({
                url: '/Urp/SetTrashPath',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(ps1),
                success: function (data) {                        
                    msgbox("提示", data);
                    prefresh('/Urp/SetPath');
                  }
                });     
             }else
             {
                msgbox("提示","格式不正确！");

             }
      });
       
    }else
    {
        msgbox("提示","请选择一条记录！");

    }
});
//设置收藏夹
$("#psetstardir").click(function () {
    let rows=pathtable.rows({selected: true}).data(); //得到选中行数组数据   
    if(rows[0] !==undefined)
    {
        let ps={
           uid:rows[0][0]
        }
        $.ajax({
                    url: '/Urp/SetFavor',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(ps),
                    success: function (data) {                        
                        msgbox("提示", data);
                        prefresh('/Urp/SetPath');
                    }
                });         
    }else
    {
        msgbox("提示","请选择一条记录！");

    }
    
});
//上传更新配置
$("#puploadupdate").click(function () {
    CallDialog("","/Urp/setpath_upload","按用户ID更新配置",function(){       
        Upload();//上传        
  });

});
//下载保存配置
$("#pdownloadsave").click(function () {
    let url ='/Urp/setpathdownexcel'; 
    let a = document.createElement('a');
    a.download ='userdisksets.xlsx';
    a.href=url;
    a.click();
});