﻿//page1 修改密码
$("#editpwd").click(function () {
    //配置参数
    var data = {
        optname: 'UserInfo,EditPwd',       
        oldpwd: $("#oldpwd").val(),
        newpwd1: $("#newpwd1").val(),
        newpwd2: $("#newpwd2").val()
    };
    //api调用处理
    if (data.oldpwd !== "" && data.newpwd1 !== "" && data.newpwd2 !== "" & data.newpwd1 === data.newpwd2)
    {
        $.ajax({
            url: '/Urp/AddEditDelOpt',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (data) {
                msgbox("提示", data);
                prefresh("/Urp/UserInfo");//刷新
            }
        });
    } else
    {
        msgbox("提示", "不能为空！新密码校验要一致！");

    }

});
//标签导航进入事件
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    switch (e.target.innerText)
    {
        case "密码修改":
            break;
        case "接收消息":
            table1.ajax.reload();
            dispMsgCount();
            break;
    };
});
var skds = [
    { name: "黑色", value: 'site_blank' },
    { name: "绿色", value: 'site_green' },
    { name: "蓝色", value: 'site_blue' },
    { name: "灰色", value: 'site_gray' } 
];
pInsertSelectObject("#skinsname", skds);//方式
$("#skinsbt1").click(function(){
    //在此保存，在_layout.cshtml引用名称处理，以下做的是即时更新
    //$("#skinid").attr("href","/css/"+$("#skinsname").val()+".css");
    $.cookie("Bs_yy_skins", $("#skinsname").val(), { expires:90,path:'/'}); //存储一个带3天期限的cookie
     window.location.reload();  
     
});

