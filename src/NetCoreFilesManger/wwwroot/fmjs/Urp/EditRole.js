﻿//加载表格分页
var table1 =$('#t11').DataTable({
    "pagingType": "full_numbers",  
    "select": {
        "style": 'single'
    },
    "language": {
    "url":"../lib/datatables/zh_CN.txt"
    },
    "lengthMenu": [[10,25,50, -1], [10,25,50, "All"]]
});
//添加----------------------------------------------------------------------
//添加----------------------------------------------------------------------
$("#addrole").click(function () {       
    $.ajax({
        url: '/Urp/EditRole_Add',
        type: 'POST',
        contentType: 'application/json', 
        success: function (data) {                
                
                //*
                bootbox.confirm({
                    title: "添加角色",
                    size: "large",
                    message:data,
                    buttons: {
                       confirm: {
                           label: '确定',
                           className: 'btn-default'
                       },
                       cancel: {
                           label: '取消',
                           className: 'btn-default'
                       }
                    },
                    callback: function (result) {
    
                        if (result)
                         {
                            //确定
                            //得到数据
                            var psdata = {
                              optname:'role,add',
                               rid: $("#roleid").text(),
                               rname: $("#rolename").text(),
                               rmemo: $("#rolememo").text()                              
                           };
                            //添加
                           if (data.rid === "" || data.rname === "" || data.rmemo === "" ) 
                           {
                              msgbox("提示", "不能为空！");
                          }
                          else
                           {
                              $.ajax({
                                  url: '/Urp/AddEditDelOpt',
                                  type: 'POST',
                                  contentType: 'application/json',
                                  data: JSON.stringify(psdata),
                                  success: function (data)
                                   {
                                      //
                                      msgbox("提示", data);
                                      prefresh('/Urp/EditRole');
                                  }
                              });
                          }
                           
                        }//if
                      
                    }
                });
    
    
                //*
                $(".modal-dialog").css("margin-top", "150px");
            
        }});
});

//修改---------------------------------------------------------------------------------------------------
//修改---------------------------------------------------------------------------------------------------

$("#editrole").click(function () {  
    var d1=table1.rows({selected: true}).data(); //得到选中行数组数据   
     if(d1[0] !==undefined)
     {
        var ps={
            rid:d1[0][0],
            rname:d1[0][1],
            rmemo:d1[0][2]
          };
          $.ajax({
              url: '/Urp/EditRole_Edit',
              type: 'POST',
              contentType: 'application/json',  
              data:JSON.stringify(ps),         
              success: function (data) {
                    
                      
                      //*
                      bootbox.confirm({
                          title: "修改用户",
                          size: "large",
                          message:data,
                          buttons: {
                             confirm: {
                                 label: '确定',
                                 className: 'btn-default'
                             },
                             cancel: {
                                 label: '取消',
                                 className: 'btn-default'
                             }
                          },
                          callback: function (result) {
          
                              if (result) {
                                   //得到数据
                            var psdata = {
                                optname:'role,edit',
                                 rid: $("#roleid").text(),
                                 rname: $("#rolename").text(),
                                 rmemo: $("#rolememo").text()                              
                             };
                              //修改
                             if (data.rid === "" || data.rname === "" || data.rmemo === "" ) 
                             {
                                msgbox("提示", "不能为空！");
                            }
                            else
                             {
                                $.ajax({
                                    url: '/Urp/AddEditDelOpt',
                                    type: 'POST',
                                    contentType: 'application/json',
                                    data: JSON.stringify(psdata),
                                    success: function (data)
                                     {
                                        //
                                        msgbox("提示", data);
                                        prefresh('/Urp/EditRole');
                                    }
                                });
                            }
                              }
                             
                          }
                      });
          
          
                      //*
                      $(".modal-dialog").css("margin-top", "150px");
                  
              }});
        
     }else
     {
        msgbox("提示","请选中一行!");
     }
});

//删除------------------------------------------------------------------------------------------
//删除------------------------------------------------------------------------------------------
$("#delrole").click(function () {  
    var d1=table1.rows({selected: true}).data(); //得到选中行数组数据   
     if(d1[0] !==undefined)
     { 
        bootbox.confirm({
            title: "删除提示",
            size: "large",
            message: "<span style='color:red'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i></span>你确定要删除吗?",
            buttons: {
                confirm: {
                    label: '确定',
                    className: 'btn-default'
                },
                cancel: {
                    label: '取消',
                    className: 'btn-default'
                }
            },
            callback: function (result) {

                if (result) {
                    //确定
                     //参数
                        var psdata = {
                            optname: 'role,del',
                            rid: d1[0][0],  
                            rname: "占个位容错",
                            rmemo: "占个位容错"        
                        };
                        //删除

                    $.ajax({
                        url: '/Urp/AddEditDelOpt',
                        type: 'POST',
                        contentType: 'application/json',
                        data: JSON.stringify(psdata),
                        success: function (data) {
                            //
                             msgbox("提示", data);
                            prefresh('/Urp/EditRole');
                        }
                     });
                }
                
            }
        });
        $(".modal-dialog").css("margin-top", "150px");
         
     }else
     {
        msgbox("提示","请选中一行!");
     }
});

function checkselectitem()
 {      //组合选择项发给后台用
        var temps =$("[name=sqx]");// document.getElementsByName('sqx'); 
		var vs="00";
        for (var i =0; i<temps.length; i++) 
        { 
            if (temps[i].checked)
            {
			vs=vs+','+temps[i].value;
            }			
        }
		return vs;
 } 
$("#editrolepermission").click(function () {  

    var d1=table1.rows({selected: true}).data(); //得到选中行数组数据   
     if(d1[0] !==undefined)
     {
         
        var ps=
          {
          rid:d1[0][0],
          rname:d1[0][1],
          rmemo:d1[0][2]             
          };
          $.ajax({
              url: '/Urp/EditRole_EditPermission',
              type: 'POST',
              contentType: 'application/json',  
              data:JSON.stringify(ps),         
              success: function (data) {
                    
                    
                      //*
                      bootbox.confirm({
                          title: "角色权根管理",
                          size: "large",
                          message:data,
                          buttons: {
                             confirm: {
                                 label: '确定',
                                 className: 'btn-default'
                             },
                             cancel: {
                                 label: '取消',
                                 className: 'btn-default'
                             }
                          },
                          callback: function (result) {
                             
                              if (result) {
                                  //确定
                                  //参数
                                  var psdata = {
                                      optname: 'role,editrolepermission',
                                      rid:d1[0][0],
                                      rname:"占位容错",
                                      rmemo:"占位容错",  
                                      permissionids: checkselectitem()
                                  };                                  
                                  $.ajax({
                                      url: '/Urp/AddEditDelOpt',
                                      type: 'POST',
                                      contentType: 'application/json',
                                      data: JSON.stringify(psdata),
                                      success: function (data) {
                                          //
                                          msgbox("提示", data);
                                          prefresh('/Urp/EditRole');
                                      }
                                  });
                              }
                               
                          }
                      });         
          
                      //*
                   
                  
              }});
     }else
     {
        msgbox("提示","请选中一行!");
     }
});