//加载表格分页
var emtable =$('#t11').DataTable({
    "pagingType": "full_numbers",    
    "language": {
    "url":"../lib/datatables/zh_CN.txt"
    },
    "lengthMenu": [[10,25,50, -1], [10,25,50, "All"]]
});
emtable.on("click", "tr", function (e) {
    var row = emtable.row(this).data();
    $("#numa").val(row[0]);
    $("#numb").val(row[0]);

});

//删除事件
$("#delbt").click(function () {
    //配置参数
    var data = {
        optname: 'EditMsgPool,del',
        read_status: $("input:radio[name='status']:checked").val(),
        range_s: $("input:radio[name='range']:checked").val(),
        rangeA: $("#numa").val(),
        rangeB: $("#numb").val()
    };
    //api调用处理
    $.ajax({
        url: '/Urp/AddEditDelOpt',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function (data) {          
            msgbox("提示", data);
            dispMsgCount();
            prefresh("/Urp/EditMsgPool");//刷新
        }
    });


});