using System.Diagnostics;
using System.Runtime.InteropServices;

namespace NetCoreFilesManger.Models
{
    
    public class wshell
    {
         public static string halt()
         {  
            string msg="<div style='background-color:#000;color:#fff'>命令窗口反馈：<br>";  
            try{          
                ProcessStartInfo psi =new ProcessStartInfo(); //创建一个ProcessStartInfo对象 使用系统shell 指定命令和参数 设置标准输出
                //根据系统使用不同的shell文件
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                        
                        psi= new ProcessStartInfo("shutdown","-s -t 60") { RedirectStandardOutput = true };
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                        
                        psi= new ProcessStartInfo("shutdown","-h 1") { RedirectStandardOutput = true };

                }
                //启动
                var proc = Process.Start(psi);
                if (proc != null)
                {
                    
                    //开始读取
                    using (var sr = proc.StandardOutput)
                    {
                        while (!sr.EndOfStream)
                        {
                            msg=msg+ sr.ReadLine()+"<br>";
                        }
                        if (!proc.HasExited)
                        {
                            proc.Kill();
                        }
                    } 
                        
                }
             }catch(ExternalException ex)
            {msg=ex.Message;}
            msg=msg+"<br>命令已发出，1分钟后服务器将关闭...</div>";        
            return msg;     
        }//
        public static string reboot()
         {  
            string msg="<div style='background-color:#000;color:#fff'>命令窗口反馈：<br>"; 
             try{             
            ProcessStartInfo psi =new ProcessStartInfo(); //创建一个ProcessStartInfo对象 使用系统shell 指定命令和参数 设置标准输出
            //根据系统使用不同的shell文件
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                     
                     psi= new ProcessStartInfo("shutdown","-r -t 60") { RedirectStandardOutput = true };
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                     
                     psi= new ProcessStartInfo("shutdown","-r 1") { RedirectStandardOutput = true };

            }
            //启动
            var proc = Process.Start(psi);
            if (proc != null)
            {
                
                //开始读取
                using (var sr = proc.StandardOutput)
                {
                    while (!sr.EndOfStream)
                    {
                        msg=msg+ sr.ReadLine()+"<br>";
                    }
                    if (!proc.HasExited)
                    {
                        proc.Kill();
                    }
                 } 
                    
            } 
             }catch(ExternalException ex)
            {msg=ex.Message;}
            msg=msg+"<br>命令已发出，1分钟后服务器将关闭...</div>";        
            return msg;
        }//
        public static string CancelTheOrder()
         {  
             string msg="<div style='background-color:#000;color:#fff'>命令窗口反馈：<br>";  
             try{          
            ProcessStartInfo psi =new ProcessStartInfo(); //创建一个ProcessStartInfo对象 使用系统shell 指定命令和参数 设置标准输出
            //根据系统使用不同的shell文件
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {     
                     
                     psi= new ProcessStartInfo("shutdown","-a");
                     psi.CreateNoWindow=false;
                     psi.UseShellExecute = false;
                     psi.WindowStyle = ProcessWindowStyle.Hidden;
                     psi.RedirectStandardError = true;
                     psi.RedirectStandardInput = true;
                     psi.RedirectStandardOutput=true;
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                     
                     psi= new ProcessStartInfo("shutdown","-c") { RedirectStandardOutput = true };

            }
            //启动
            var proc = Process.Start(psi);
            if (proc != null)
            {
                
                //开始读取
                using (var sr = proc.StandardOutput)
                {
                    while (!sr.EndOfStream)
                    {
                        msg=msg+ sr.ReadLine()+"\r\n";
                    }
                    if (!proc.HasExited)
                    {
                        proc.Kill();
                    }
                 } 
                    
            } 
             }catch(ExternalException ex)
            {msg=ex.Message;}
            msg=msg+"<br>命令已发出，1分钟后服务器将关闭...</div>";        
            return msg;      
        }//
        public static string Orther( string epath)
         {  
            string msg="命令窗口反馈：";   
            try{         
            ProcessStartInfo psi =new ProcessStartInfo(); //创建一个ProcessStartInfo对象 使用系统shell 指定命令和参数 设置标准输出
            //根据系统使用不同的shell文件
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                     
                     psi= new ProcessStartInfo(epath+@"\shell\win.bat");
                     psi.CreateNoWindow=false;
                     psi.UseShellExecute = false;
                     psi.WindowStyle = ProcessWindowStyle.Hidden;
                     psi.RedirectStandardError = true;
                     psi.RedirectStandardInput = false;
                     psi.RedirectStandardOutput=true;
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                     
                     psi= new ProcessStartInfo("sh",epath+@"/shell/linux.sh") { RedirectStandardOutput = true };

            }
            //启动
            var proc = Process.Start(psi);
            if (proc != null)
            {
                
                //开始读取
                using (var sr = proc.StandardOutput)
                {
                    // while (!sr.EndOfStream)
                    // {
                    //     msg=msg+sr.ReadLine().Replace(@"<DIR>","[dir]")+"<br>";
                    // }
                    msg= proc.StandardOutput.ReadToEnd();
                    //msg=msg.Replace(@"<DIR>","[dir]");                 
                    if (!proc.HasExited)
                    {
                        proc.Kill();
                    }
                 } 
                    
            } 
             }catch(ExternalException ex)
            {msg=ex.Message;}
            msg=msg+"来自配置的命令已执行...";           
            return msg;
        }//
         public static string rar( string epath)
         {  
            string msg="命令窗口反馈：";   
            try{         
            ProcessStartInfo psi =new ProcessStartInfo(); //创建一个ProcessStartInfo对象 使用系统shell 指定命令和参数 设置标准输出
            //根据系统使用不同的shell文件
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                     
                     psi= new ProcessStartInfo(epath+@"\shell\winrar.bat");
                     psi.CreateNoWindow=false;
                     psi.UseShellExecute = false;
                     psi.WindowStyle = ProcessWindowStyle.Hidden;
                     psi.RedirectStandardError = true;
                     psi.RedirectStandardInput = false;
                     psi.RedirectStandardOutput=true;
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                     
                     psi= new ProcessStartInfo("sh",epath+@"/shell/linuxrar.sh") { RedirectStandardOutput = true };

            }
            //启动
            var proc = Process.Start(psi);
            if (proc != null)
            {
                
                //开始读取
                using (var sr = proc.StandardOutput)
                {
                    // while (!sr.EndOfStream)
                    // {
                    //     msg=msg+sr.ReadLine().Replace(@"<DIR>","[dir]")+"<br>";
                    // }
                    msg= proc.StandardOutput.ReadToEnd();
                    //msg=msg.Replace(@"<DIR>","[dir]");                 
                    if (!proc.HasExited)
                    {
                        proc.Kill();
                    }
                 } 
                    
            } 
             }catch(ExternalException ex)
            {msg=ex.Message;}
            msg=msg+"来自配置的命令已执行...";           
            return msg;
        }//

    }
    

}