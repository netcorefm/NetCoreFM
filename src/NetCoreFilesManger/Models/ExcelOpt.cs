using System.Data;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
namespace NetCoreFilesManger.Models
{
    public class ExcelOpt
    {   
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file_ms">文件流</param>
        /// <param name="sheetno">要读取的表单编号从0开始</param>
        /// <param name="Columnnames">生成datatable的列名</param>
        /// <param name="HaveFirstRowHeader">excel表单第一行是否为表头行</param>
        /// <returns></returns>
        public static DataTable ReadExcelToTable(MemoryStream file_ms,int sheetno,string[] Columnnames,bool HaveFirstRowHeader)
        {
            file_ms.Position = 0;
            XSSFWorkbook workbook = new XSSFWorkbook(file_ms);   
            ISheet sheet = workbook.GetSheetAt(sheetno);
            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
            DataTable dt = new DataTable();             
            for (int j = 0; j < Columnnames.Length; j++)
            {
            dt.Columns.Add(Columnnames[j]);
            }
            if(HaveFirstRowHeader)
            {
            rows.MoveNext();
            }
            while (rows.MoveNext())
            {
            XSSFRow row = (XSSFRow)rows.Current;
            DataRow dr = dt.NewRow();
            for (int i = 0; i < row.Cells.Count; i++)
            {
                ICell cell = row.GetCell(i);
                if (cell == null)
                {
                    dr[i] = null;
                }
                else
                { 
                    dr[i] = cell.ToString();
                }
            }
            dt.Rows.Add(dr);
            }
            workbook.Close();
            workbook = null;
            return dt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file_ms">文件流</param>
        /// <param name="sheetname">要读取的表单名称</param>
        /// <param name="Columnnames">生成datatable的列名</param>
        /// <param name="HaveFirstRowHeader">excel表单第一行是否为表头行</param>
        /// <returns></returns>
        public static DataTable ReadExcelToTable(MemoryStream file_ms,string sheetname,string[] Columnnames,bool HaveFirstRowHeader)
        {
            file_ms.Position = 0;
            XSSFWorkbook workbook = new XSSFWorkbook(file_ms);   
            ISheet sheet = workbook.GetSheet(sheetname);
            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
            DataTable dt = new DataTable();
             for (int j = 0; j < Columnnames.Length; j++)
                {
                    dt.Columns.Add(Columnnames[j]);
                }
            if(HaveFirstRowHeader)
            {                 
                rows.MoveNext();
            }         
            while (rows.MoveNext())
            {
                XSSFRow row = (XSSFRow)rows.Current;
                DataRow dr = dt.NewRow();
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    ICell cell = row.GetCell(i);
                    if (cell == null)
                    {
                        dr[i] = null;
                    }
                    else
                    { 
                        dr[i] = cell.ToString();
                    }
                }
                dt.Rows.Add(dr);
            }
            workbook.Close();
            workbook = null;
            return dt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path">全文件路径</param>
        /// <param name="sheetname">要读取的表单名称</param>
        /// <param name="Columnnames">生成datatable的列名</param>
        /// <param name="HaveFirstRowHeader">excel表单第一行是否为表头行</param>
        /// <returns></returns>
        public static DataTable ReadExcelToTable(string path, string sheetname, string[] Columnnames, bool HaveFirstRowHeader)//excel存放的路径
        {
            XSSFWorkbook workbook;


            using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                // 根据现有的Excel文档创建工作簿
                workbook = new XSSFWorkbook(file);
            }
            ISheet sheet = workbook.GetSheet(sheetname);
            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();

            DataTable dt = new DataTable();
            for (int j = 0; j < Columnnames.Length; j++)
            {
                dt.Columns.Add(Columnnames[j]);
            }
            if (HaveFirstRowHeader)
            {
                rows.MoveNext();
            }
            while (rows.MoveNext())
            {
                XSSFRow row = (XSSFRow)rows.Current;
                DataRow dr = dt.NewRow();

                for (int i = 0; i < row.Cells.Count; i++)
                {
                    ICell cell = row.GetCell(i);


                    if (cell == null)
                    {
                        dr[i] = null;
                    }
                    else
                    {
                        dr[i] = cell.ToString();
                    }
                }
                dt.Rows.Add(dr);
            }
            workbook.Close();
            workbook = null;
            return dt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path">全文件路径</param>
        /// <param name="dt">要保存的datatable</param>
        /// <param name="sname">生成的excel内的表单名</param>
        /// <param name="HaveFirstRowHeader">在excel表单中是否生成表头行</param>
        /// <returns></returns>
        public static byte[] CreateExcelFromDataTable ( DataTable dt, string sname, bool HaveFirstRowHeader)
        {   
             XSSFWorkbook workbook = new XSSFWorkbook();
             ISheet sheet = workbook.CreateSheet(sname);
             int start = 0;              
             if (HaveFirstRowHeader == true) //写入DataTable的列名
             {
                IRow row = sheet.CreateRow(0);                
                for (int j = 0; j < dt.Columns.Count; ++j)
                {
                    row.CreateCell(j).SetCellValue(dt.Columns[j].ColumnName);
                }
                start++;
            }            
            for (int i = 0; i < dt.Rows.Count; ++i)
            {
                IRow row = sheet.CreateRow(start);
                for (int j = 0; j < dt.Columns.Count; ++j)
                {
                    row.CreateCell(j).SetCellValue(dt.Rows[i][j].ToString());
                }
                ++start;
            }
            byte[] buffer = new byte[1024*5];
            using (MemoryStream ms = new MemoryStream())
            {
                workbook.Write(ms);
                buffer = ms.ToArray();
                ms.Close();
            }             
            workbook.Close();       
            return buffer;
        }
    }
}
