﻿using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
namespace NetCoreFilesManger.Models
{
     
    public class common
    {
        //全局静态变量，每个人都会使用,相当于appliction   
        public static string weburl = "http://127.0.0.1:5000";//定义网站端口
        public static string strcon= "Data Source=./NetCoreFM.db3";//定义连接字符串，在startup.cs已做覆盖
        public static List<Models.PEntity.OnLineUser> UserStatus=new List<PEntity.OnLineUser> ();
        //---------------------------------------------------------------
        //                  加密函数php下的md5()   
        //  xypzf lastdate 2018.2.3
        //---------------------------------------------------------------
        /// <summary>
        /// C#与PHP 一致性数据md5加密函数,[和php的md5(str,false)结果一样].
        /// </summary>
        /// <param name="input">要加密的字符串</param>
        /// <returns>返回加密后的字符串</returns>    
        public static string EncryptByMD5(string input)//md5加密，和php的md5(str,false)结果一样
        {
            MD5 md5o = MD5.Create();
            byte[] data = md5o.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2")); //将每个字节转为两位格式16进制,如01,1F不足两位以0占位
            }
            return sBuilder.ToString();
        }        
        public static bool IsNumeric(string value)
        {
            try
            {
                //return Regex.IsMatch(value, @"^([+-]?)/d*[.]?/d*$");
                return Regex.IsMatch(value, @"^[0-9]+(.[0-9]{1,3})?$");
            }
            catch
            {
                return false;
            }
        }      
        public static bool testzxpwd(string zxpwd,string userid)
        {
            try
            {
                //string zxpwdm=Models.StringSecurity.DESEncrypt(zxpwd);                
                string sqluser = "Select * From  urp_user where f0001=@uid and f0004=@zxpwd "; 
                Dictionary <string,dynamic> ps=new Dictionary<string,dynamic>();
                ps.Add("@uid",int.Parse(userid));
                ps.Add("@zxpwd",zxpwd);                                      
                DataTable dt_user= Models.DataBase_SQLite.GetDataTable(sqluser,ps);
                if ( dt_user.Rows.Count> 0)
                {

                  return true;
                }else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public static bool CheckPageQX(string uid,string qxname)
        {
            bool qx=false;
             int userid=int.Parse (uid);
            string sqlqx = "Select  distinct urp_permission.f0002 as name  From urp_user_role LEFT  JOIN urp_role on urp_user_role.f0003=urp_role.f0001 left join urp_role_permission on urp_role.f0001=urp_role_permission.f0002 left join urp_permission on urp_role_permission.f0003=urp_permission.f0001 where urp_user_role.f0002=@uid";            
            Dictionary <string,dynamic> ps=new Dictionary<string,dynamic>();
            ps.Add("@uid",userid);
            DataTable dt_qxs= Models.DataBase_SQLite.GetDataTable(sqlqx,ps);
            DataRow[] rows=dt_qxs.Select("name='"+qxname+"'");
           if(rows.Length>0)
           {
               qx=true;
           }
            return qx;
        }
        public static void WriterSysLog(string stext)
        {
           
            FileStream fileStream = new FileStream("netcorefm.log", FileMode.Append);
            using (StreamWriter writer = new StreamWriter(fileStream))
            {
                 
                writer.WriteLine(stext);
            } 
            
        }
        

    }//类************

    
    
}//空间
