﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
namespace NetCoreFilesManger.Models
{
    public class MsgPool
    {
        
        
        //得到消息池当前用户的数量
        public static string GetMessagePoolCount( string uname)
        {
            
            string sql = "SELECT count(*) FROM  message_pool where bname=@bname and status='未阅读'";            
            Dictionary <string,dynamic> ps=new Dictionary<string,dynamic>();
            ps.Add("@bname",uname);
            string cn =Models.DataBase_SQLite.GetOneValue(sql,ps);
            return cn;
        }
        //向消息池写消息
        public static bool SendMessagePool(string SendName, string ToName, string Contexts)
        {
            string sql = "SELECT Max(mid)+1 FROM  message_pool";
            string mid =DataBase_SQLite.GetOneValue(sql);
            if (mid.Trim() == "") mid = "1";
            string sqlcmd = "INSERT INTO  message_pool(mid, mdate, aname, contents, bname, status)VALUES (@mid,@mdate,@aname,@contents,@bname,@status)";
            Dictionary <string,dynamic> ps=new Dictionary<string,dynamic>();
            ps.Add("@mid", int.Parse(mid));
            ps.Add("@mdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm").Trim ());
            ps.Add("@aname", SendName.Trim ());
            ps.Add("@contents", Contexts.Trim ());
            ps.Add("@bname", ToName.Trim ());
            ps.Add("@status", "未阅读");         
            int f1 =DataBase_SQLite.InsertDelEdit(sqlcmd, ps);
            if (f1 == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }//


    }
}
