using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace NetCoreFilesManger
{

    #region check userlogin
    //检查用户登录
    public class CheckUserLoginFilter:Attribute, IAuthorizationFilter
    {
         private readonly  Models.PEntity.WebSets _websiteset;  
         private readonly IHttpContextAccessor _httpContextAccessor;
        public CheckUserLoginFilter(IOptionsSnapshot<Models.PEntity.WebSets> websets,IHttpContextAccessor httpContextAccessor )
        {
           _websiteset=websets.Value;  
           _httpContextAccessor = httpContextAccessor;
        } 
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            //运行控制器代码前
            //以下是否登录验证，如果登录异常，转到登录界面
            //-------------
            string Uname= _httpContextAccessor.HttpContext.Session.GetString("xsdUserName");           
            string Udate = _httpContextAccessor.HttpContext.Session.GetString("xsd_logindate");// 登录时间                
            var onlineobj = Models.common.UserStatus.Where(ojb => ojb.username == Uname).FirstOrDefault();
            if( !(onlineobj != null && Udate.Trim() == onlineobj.logindate.Trim()))
            {
                
                 context.Result=new RedirectResult("/Home/Index");
            }  
        } 
    }
    #endregion
    #region check premission
    //验证用户权限
    public class CheckUserPermissionFilter :Attribute, IActionFilter
    {
         private readonly Models.PEntity.WebSets _websiteset;  
         private readonly IHttpContextAccessor _httpContextAccessor;
         private readonly string _qxname;
        
 
        public CheckUserPermissionFilter(string name)
        {
            _qxname = name;           
        }
        public CheckUserPermissionFilter(IOptionsSnapshot<Models.PEntity.WebSets> websets,IHttpContextAccessor httpContextAccessor ,string qxname)
        {
           _websiteset=websets.Value;  
           _httpContextAccessor = httpContextAccessor;
            _qxname=qxname;
        } 
        public void OnActionExecuting(ActionExecutingContext context)
        {
            // do something before the action executes
            string qxuid=context.HttpContext.Session.GetString("xsdUserID");
             if(!Models.common.CheckPageQX(qxuid,_qxname))
             {
                 context.Result=new RedirectResult("/Home/Index");//跳到登录
             }
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
           // do something after the action executes
        }
    }
    #endregion
    #region  DisableIISrq
        [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
        public class DisableFormValueModelBindingAttribute : Attribute, IResourceFilter
        {
        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            var factories = context.ValueProviderFactories;
            factories.RemoveType<FormValueProviderFactory>();
            factories.RemoveType<JQueryFormValueProviderFactory>();
        }

        public void OnResourceExecuted(ResourceExecutedContext context)
        {
        }
        }
    #endregion 
}