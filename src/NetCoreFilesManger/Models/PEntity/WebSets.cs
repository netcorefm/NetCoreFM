using System.Diagnostics;
using System.Runtime.InteropServices;

namespace NetCoreFilesManger.Models.PEntity
{
    
        public class WebSets
        {
            //系统常量设置，与appsettings.json对应
            public string  WebTitle { get; set; } //网站标题
            public string  Version { get; set; } //版本

            public string  WebSubTitle { get; set; } //登录子标题
            public string  Trashswitch { get; set; } //系统回收站开关 on-自动  off手动
            
        } 
    
}