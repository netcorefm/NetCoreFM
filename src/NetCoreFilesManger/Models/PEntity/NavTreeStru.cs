﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace NetCoreFilesManger.Models.PEntity
{
    //侧边栏权限树,对应bootstrap-treeview的json数据,类属性结应其结点属性
    [Serializable]
    public class NavTreeNodeEntity
    {
        public string text { get; set; }  //结点名称
        public string icon { get; set; }  //结点图标
        public string[] tags { get; set; } //结点数据

        public List<NavTreeNodeEntity> nodes { get; set; }  //下级结点
    }  
    
}
