﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreFilesManger.Models.PEntity
{  
     //导航
    public class BreadCrumbNode
    {
        public BreadCrumbNode(string name,string url)
        {  //析构函数，对属性赋值
            nodename = name;
            nodeurl = url;
        }
        public string nodename { get; set; } 
        public string nodeurl  { get; set; }
    }
}
