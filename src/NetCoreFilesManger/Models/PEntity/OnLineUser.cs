namespace NetCoreFilesManger.Models.PEntity
{
    public class OnLineUser
    {  
          public OnLineUser(string name,string date,string ip)
        {  
           //析构函数，对属性赋值
           username= name;
           logindate =date;
           loginip=ip;
        }
        //临时类，服器终止，自动销毁，用于同名用户上线和服务器在线人数统计
        public string username { get; set;}  //用户名
        public string logindate { get; set;}  //登录时间
        public string loginip { get; set;}  //登录IP
        
    }
}