using System;
using System.IO;
using System.Text;
using System.Drawing;
namespace NetCoreFilesManger.Models
{
    public class ImageOpt
    {
         public static string MakeCode(int codeLen)
        {
            if (codeLen < 1)
            {
                return string.Empty;
            }
            int number;
            StringBuilder sbCheckCode = new StringBuilder();
            Random random = new Random();

            for (int index = 0; index < codeLen; index++)
            {
                number = random.Next();

                if (number % 2 == 0)
                {
                    sbCheckCode.Append((char)('0' + (char)(number % 10))); //生成数字
                  }
                else
                {
                    sbCheckCode.Append((char)('A' + (char)(number % 26))); //生成字母
                  }
            }
            return sbCheckCode.ToString();
        } 
        //生成预览图Base64,text文本,得到4位图片
        public static string   TextToBase64Img(string text)
        {

            Random r = new Random();
            string sBasecode = "";
            Bitmap image =new Bitmap(85,30);
            Graphics g = Graphics.FromImage(image);
            g.Clear(Color.WhiteSmoke);
            //g.DrawString(text, new Font("微软雅黑",15, FontStyle.Bold), new SolidBrush(Color.Red),new Point(2,0));
            string[] fonts = { "黑体", "隶书", "仿宋" };
            Color[] colors = { Color.Blue, Color.DeepPink,Color.Orange,Color.SteelBlue, Color.Red,Color.DarkMagenta,Color.Green,Color.DimGray,Color.Tomato };
            for (int i = 0; i < 4; i++)
            {
                Point p = new Point(2+i * 20,r.Next(0, 5));
              
                g.DrawString(text[i].ToString(), new Font(fonts[r.Next(0,3)], 15, FontStyle.Bold), new SolidBrush(colors[r.Next(0,9)]), p);
            }
            //加斜线
            for (int i = 0; i <2; i++)
            {
                Point p1 = new Point(r.Next(0,  image.Width), r.Next(0, image.Height));
                Point p2 = new Point(r.Next(0,  image.Width), r.Next(0,  image.Height));
                g.DrawLine(new Pen(colors[r.Next(0,9)]),p1,p2);
            }
            //加杂色点
            for (int i = 0; i < 100; i++)
            {    
               Point p = new Point(r.Next(0,image.Width), r.Next(0, image.Height));
               image.SetPixel(p.X,p.Y,colors[r.Next(0,9)]);
             }
            byte[] byteArray = ImageToBytes(image, System.Drawing.Imaging.ImageFormat.Jpeg);
            sBasecode = "data:image/jpeg;base64," + Convert.ToBase64String(byteArray);
            image.Dispose();           
            return sBasecode;
        }//
        //得到图像文件的Base64字符串
        public static string ToBase64(string pathfilename)
        {
             string sBasecode="";
            
            try
            { 
                if(File.Exists(pathfilename))
              { 
                using (FileStream fs = new FileStream(pathfilename, FileMode.Open, FileAccess.Read))
                {
                    byte[] byteArray = new byte[fs.Length];
                    fs.Read(byteArray, 0, byteArray.Length);
                    sBasecode ="data:image/jpeg;base64,"+Convert.ToBase64String(byteArray);
                }
              }else
              {
                  sBasecode ="";
              }
            }
            catch
            {
               sBasecode = "";
            }
            return sBasecode;
        }//

        //生成预览图Base64,200内大小
        public static string MakeImgPreBase64(string pathfilename)
        {
             string sBasecode="";
             Image image = Image.FromFile(pathfilename);
             int width = image.Width;
             int height = image.Height;
             int sz = height /150;
             width = width / sz;
             height = height / sz;        
             Image img1=image.GetThumbnailImage(width,height, new Image.GetThumbnailImageAbort(delegate { return false; }), IntPtr.Zero);
             byte[] byteArray = ImageToBytes(img1, System.Drawing.Imaging.ImageFormat.Jpeg);
             sBasecode = "data:image/jpeg;base64," + Convert.ToBase64String(byteArray);
            image.Dispose();
            img1.Dispose();
            return sBasecode;
        }//
        public static Image MakeImgTo2cun(string pathfilename)
        {
             
             Image image = Image.FromFile(pathfilename);
             int width = image.Width;
             int height = image.Height;
             if (height>600)
             {
                double sz = height /600;
                width =(int)(width / sz);
                height =(int)(height / sz);  
             }   
             Image img1=image.GetThumbnailImage(width,height, new Image.GetThumbnailImageAbort(delegate { return false; }), IntPtr.Zero);
             image.Dispose();            
             return img1;
        }//
        public static byte[] ImageToBytes(Image Image, System.Drawing.Imaging.ImageFormat imageFormat)
        {

            if (Image == null) { return null; }

            byte[] data =null;

    using (MemoryStream ms = new MemoryStream())
            {

                using (Bitmap Bitmap = new Bitmap(Image))
                {

                    Bitmap.Save(ms, imageFormat);

                    ms.Position = 0;

                    data =new byte[ms.Length];

            ms.Read(data, 0, Convert.ToInt32(ms.Length));

                    ms.Flush();

                }

            }

            return data;

        }
        //---
    }
}