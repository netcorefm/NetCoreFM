﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;

namespace NetCoreFilesManger
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var isService = !(Debugger.IsAttached || args.Contains("--console"));
            if (isService)
            {
                var pathToExe = Process.GetCurrentProcess().MainModule.FileName;
                var pathToContentRoot = Path.GetDirectoryName(pathToExe);
                Directory.SetCurrentDirectory(pathToContentRoot);
            }
            var builder = CreateWebHostBuilder(
                args.Where(arg => arg != "--console").ToArray());
            var host = builder.Build();
            if (isService)
            {
                // To run the app without the CustomWebHostService change the
                // next line to host.RunAsService();
                // host.RunAsCustomService();
                host.RunAsService();
            }
            else
            {
                host.Run();//调试用
            }

            // CreateWebHostBuilder(args).Build().Run(); //通用            
            //var host = hostBuilder.UseKestrel()  //kestrel

            //                      .UseStartup<Startup>()

            //                      .UseContentRoot(Directory.GetCurrentDirectory())

            //                      .UseUrls("http://localhost:7000")

            //                      .Build();
            //host.Run();


        }
        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var configuration = new ConfigurationBuilder().SetBasePath(Environment.CurrentDirectory)
                                          .AddJsonFile("host.json")
                                          .Build();

            var url = configuration["url"];

            return WebHost.CreateDefaultBuilder(args).UseUrls(configuration["url"])
                                                     .UseStartup<Startup>();
        }
        //启用 IISIntegration 组件
        //    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>           

        //    WebHost.CreateDefaultBuilder(args)

        //         .ConfigureLogging((hostingContext, logging) =>
        //         {
        //             logging.AddEventLog();

        //             //NetCoreFilesManger.Models.common.weburl = Configuration.GetSection("weburl").Value;
        //         })             
        //         .UseStartup<Startup>();

        //}
        //    public static class WebHostServiceExtensions
        //{
        //    public static void RunAsCustomService(this IWebHost host)
        //    {
        //        var webHostService = new CustomWebHostService(host);
        //        ServiceBase.Run(webHostService);
        //    }
        //}
        [DesignerCategory("Code")]
        internal class CustomWebHostService : WebHostService
        {
            private ILogger _logger;

            public CustomWebHostService(IWebHost host) : base(host)
            {
                _logger = host.Services
                    .GetRequiredService<ILogger<CustomWebHostService>>();
            }

            protected override void OnStarting(string[] args)
            {
                _logger.LogInformation("OnStarting method called.");
                base.OnStarting(args);
            }

            protected override void OnStarted()
            {
                _logger.LogInformation("OnStarted method called.");
                base.OnStarted();
            }

            protected override void OnStopping()
            {
                _logger.LogInformation("OnStopping method called.");
                base.OnStopping();
            }
        }


    }
}
